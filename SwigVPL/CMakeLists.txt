#==============================================================================
# This file is part of
#
# VPLswig - SWIG-based VPL bindings for Python
# Changes are Copyright 2015 3Dim Laboratory s.r.o.
# All rights reserved.
#
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.
#==============================================================================

SET(CMAKE_FIND_LIBRARY_SUFFIXES ".lib" ".dll")

find_library( VPL_PNG_DLL
                NAMES "png.dll" "libpng.dll" "libpng16.dll"
                PATHS ${VPL_3RDPARTY_DIR}/bin
                NO_DEFAULT_PATH 
            )
INCLUDE_DIRECTORIES(${PROJECT_SOURCE_DIR}/include)

if ( VPL_FOUND )
	
    link_directories( ${VPL_LIBRARIES_DIR} )
	include_directories( ${VPL_INCLUDE_DIR})
	set( VPL_LINKED_LIBS ${VPL_LIBRARIES} CACHE STRING "All the VPL libraries." )
    
    option(BUILD_WITH_BUILTIN "It makes exposed code faster, not working for including in c++ app." OFF )
    
    
    if( VPL_3RDPARTY_DIR STREQUAL "" )
        SET( VPL_3RDPARTY_DIR "" CACHE PATH "VPL 3rdparty directory for build." )
        message( WARNING "VPL 3rdparty was not found! Please, set the cache entry VPL_3RDPARTY_DIR!" )
        
    else( VPL_3RDPARTY_DIR STREQUAL "" )
        # Find the Eigen library
        if( NOT VPL_EIGEN_INCLUDE_DIR )
            find_package( Eigen3 REQUIRED )
            if( EIGEN3_FOUND )
            set( VPL_EIGEN_INCLUDE_DIR ${EIGEN3_INCLUDE_DIR} CACHE STRING "Full path to the Eigen library header files." FORCE )
            endif( EIGEN3_FOUND )
        endif()
        
        if( NOT VPL_ZLIB )
            find_library( VPL_ZLIB
                  NAMES zlib libzlib
                  PATHS ${VPL_3RDPARTY_DIR}/lib
                  NO_DEFAULT_PATH NO_CMAKE_ENVIRONMENT_PATH
                  NO_CMAKE_PATH NO_SYSTEM_ENVIRONMENT_PATH
                  NO_CMAKE_SYSTEM_PATH
                  )
            
            set( VPL_ZLIB_INCLUDE_DIR ${VPL_3RDPARTY_DIR}/include CACHE STRING "Full path to the ZLIB library header files." FORCE )
        endif( NOT VPL_ZLIB )
    
        if( NOT VPL_PNG )
            find_library( VPL_PNG
                NAMES png libpng libpng16
                PATHS ${VPL_3RDPARTY_DIR}/lib
                NO_DEFAULT_PATH NO_CMAKE_ENVIRONMENT_PATH
                NO_CMAKE_PATH NO_SYSTEM_ENVIRONMENT_PATH
                NO_CMAKE_SYSTEM_PATH
            )

            set( VPL_PNG_INCLUDE_DIR ${VPL_3RDPARTY_DIR}/include CACHE STRING "Full path to the PNG library header files." FORCE )
        endif( NOT VPL_PNG )
        
    endif()
    
	
    include_directories( ${VPL_PNG_INCLUDE_DIR} )
    include_directories( ${VPL_ZLIB_INCLUDE_DIR} )
    include_directories( ${VPL_EIGEN_INCLUDE_DIR} )

    mark_as_advanced( VPLSWIG_MAJOR_VERSION VPLSWIG_MINOR_VERSION VPLSWIG_PATCH_VERSION VPLSWIG_VERSION )
    
	find_package(SWIG REQUIRED)
	if (SWIG_FOUND)
	
        INCLUDE(${SWIG_USE_FILE})
		

		SET(CMAKE_SWIG_FLAGS "-I${CMAKE_CURRENT_SOURCE_DIR}")
						
		MESSAGE(STATUS "Now adding packages")
		add_subdirectory(interfaces)
        
        if(BUILD_DOCUMENTATION)

        INSTALL(DIRECTORY ${PROJECT_SOURCE_DIR}/doc/SwigVPLDoc/python/source DESTINATION ${CMAKE_INSTALL_PREFIX}/${TRIDIM_VPLSWIG_DOC_VER})
        INSTALL( FILES ${PROJECT_SOURCE_DIR}/doc/SwigVPLDoc/python/make.bat DESTINATION "${TRIDIM_VPLSWIG_DOC_VER}" )
        INSTALL( FILES ${PROJECT_SOURCE_DIR}/doc/SwigVPLDoc/python/makefile DESTINATION "${TRIDIM_VPLSWIG_DOC_VER}" )
        endif()

        
        if(BUILD_VPLSWIG_EXAMPLES)
        INSTALL_EXAMPLE()
        endif()
        
        

	else(SWIG_FOUND)
        message( FATAL_ERROR "SWIG was not found!" )
	endif( SWIG_FOUND )
endif()