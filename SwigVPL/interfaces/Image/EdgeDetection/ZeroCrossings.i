//==============================================================================
// This file is part of
//
// VPLswig - SWIG-based VPL bindings for Python
// Changes are Copyright 2016 3Dim Laboratory s.r.o.
// All rights reserved.
//
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.
//==============================================================================

%{
#include <VPL/Image/EdgeDetection/ZeroCrossings.h>
%}

%include "VPL/Image/EdgeDetection/ZeroCrossings.h"

%template(CZeroCrossDetector_Image8)vpl::img::CZeroCrossDetector<vpl::img::CImage<vpl::img::tPixel8, VPL_IMAGE_DATA_ALLOCATOR>>;
%template(CZeroCrossDetector_Image16)vpl::img::CZeroCrossDetector<vpl::img::CImage<vpl::img::tPixel16, VPL_IMAGE_DATA_ALLOCATOR>>;
%template(CZeroCrossDetector_Image32)vpl::img::CZeroCrossDetector<vpl::img::CImage<vpl::img::tPixel32, VPL_IMAGE_DATA_ALLOCATOR>>;
%template(CZeroCrossDetector_FImage)vpl::img::CZeroCrossDetector<vpl::img::CImage<vpl::img::tFloatPixel, VPL_IMAGE_DATA_ALLOCATOR>>;
%template(CZeroCrossDetector_DImage)vpl::img::CZeroCrossDetector<vpl::img::CImage<vpl::img::tDensityPixel, VPL_IMAGE_DATA_ALLOCATOR>>;
//%template(CZeroCrossDetector_RGBAImage)vpl::img::CZeroCrossDetector<vpl::img::CImage<vpl::img::tRGBPixel, VPL_IMAGE_DATA_ALLOCATOR>>;
//%template(CZeroCrossDetector_ComplexImage)vpl::img::CZeroCrossDetector<vpl::img::CImage<vpl::img::tComplexPixel, VPL_IMAGE_DATA_ALLOCATOR>>;