//==============================================================================
// This file is part of
//
// VPLswig - SWIG-based VPL bindings for Python
// Changes are Copyright 2016 3Dim Laboratory s.r.o.
// All rights reserved.
//
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.
//==============================================================================
// Module description 
%{
#include <VPL/Image/ImageEdgeDetector.h>
%}

%include "VPL/Image/ImageEdgeDetector.h"

%template(swig_base_CImage8EdgeDetector)vpl::img::CImageEdgeDetector<vpl::img::CImage<vpl::img::tPixel8, VPL_IMAGE_DATA_ALLOCATOR>>;
%template(swig_base_CImage16EdgeDetector)vpl::img::CImageEdgeDetector<vpl::img::CImage<vpl::img::tPixel16, VPL_IMAGE_DATA_ALLOCATOR>>;
%template(swig_base_CImage32EdgeDetector)vpl::img::CImageEdgeDetector<vpl::img::CImage<vpl::img::tPixel32, VPL_IMAGE_DATA_ALLOCATOR>>;
%template(swig_base_CFImageEdgeDetector)vpl::img::CImageEdgeDetector<vpl::img::CImage<vpl::img::tFloatPixel, VPL_IMAGE_DATA_ALLOCATOR>>;
%template(swig_base_CDImageEdgeDetector)vpl::img::CImageEdgeDetector<vpl::img::CImage<vpl::img::tDensityPixel, VPL_IMAGE_DATA_ALLOCATOR>>;

%include "Canny.i"
%include "ZeroCrossings.i"