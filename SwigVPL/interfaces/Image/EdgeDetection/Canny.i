//==============================================================================
// This file is part of
//
// VPLswig - SWIG-based VPL bindings for Python
// Changes are Copyright 2016 3Dim Laboratory s.r.o.
// All rights reserved.
//
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.
//==============================================================================

%{
#include <VPL/Image/EdgeDetection/Canny.h>

%}
%include "VPL/Image/EdgeDetection/Canny.h"

%template(CCanny_Image8)vpl::img::CCanny<vpl::img::CImage<vpl::img::tPixel8, VPL_IMAGE_DATA_ALLOCATOR>>;
%template(CCanny_Image16)vpl::img::CCanny<vpl::img::CImage<vpl::img::tPixel16, VPL_IMAGE_DATA_ALLOCATOR>>;
%template(CCanny_Image32)vpl::img::CCanny<vpl::img::CImage<vpl::img::tPixel32, VPL_IMAGE_DATA_ALLOCATOR>>;
%template(CCanny_FImage)vpl::img::CCanny<vpl::img::CImage<vpl::img::tFloatPixel, VPL_IMAGE_DATA_ALLOCATOR>>;
%template(CCanny_DImage)vpl::img::CCanny<vpl::img::CImage<vpl::img::tDensityPixel, VPL_IMAGE_DATA_ALLOCATOR>>;
//%template(CCanny_RGBAImage)vpl::img::CCanny<vpl::img::CImage<vpl::img::tRGBPixel, VPL_IMAGE_DATA_ALLOCATOR>>;
//%template(CCanny_ComplexImage)vpl::img::CCanny<vpl::img::CImage<vpl::img::tComplexPixel, VPL_IMAGE_DATA_ALLOCATOR>>;
