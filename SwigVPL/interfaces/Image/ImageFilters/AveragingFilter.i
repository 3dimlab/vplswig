//==============================================================================
// This file is part of
//
// VPLswig - SWIG-based VPL bindings for Python
// Changes are Copyright 2016 3Dim Laboratory s.r.o.
// All rights reserved.
//
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.
//==============================================================================

%{

#include <VPL/Image/Filters/Averaging.h>
%}
%ignore vpl::img::CAvg3Filter::KERNEL;
%ignore vpl::img::CAvg3Filter::~CAvg3Filter();
%ignore vpl::img::CAvg5Filter::KERNEL;
%ignore vpl::img::CAvg5Filter::~CAvg5Filter();

%ignore vpl::img::CAvg7Filter::KERNEL;
%ignore vpl::img::CAvg7Filter::~CAvg7Filter();


%include "VPL/Image/Filters/Averaging.h"
%template(CAvg3Filter_Image8)vpl::img::CAvg3Filter<vpl::img::CImage<vpl::img::tPixel8>>;
%template(CAvg3Filter_Image16)vpl::img::CAvg3Filter<vpl::img::CImage<vpl::img::tPixel16>>;
%template(CAvg3Filter_Image32)vpl::img::CAvg3Filter<vpl::img::CImage<vpl::img::tPixel32>>;
%template(CAvg3Filter_FImage)vpl::img::CAvg3Filter<vpl::img::CImage<vpl::img::tFloatPixel>>;
%template(CAvg3Filter_DImage)vpl::img::CAvg3Filter<vpl::img::CImage<vpl::img::tDensityPixel>>;
//%template(CAvg3Filter_RGBAImage)vpl::img::CAvg3Filter<vpl::img::CImage<vpl::img::tRGBAPixel>>;
//%template(CAvg3Filter_ComplexImage)vpl::img::CAvg3Filter<vpl::img::CImage<vpl::img::tComplexPixel>>;


%template(CAvg5Filter_Image8)vpl::img::CAvg5Filter<vpl::img::CImage<vpl::img::tPixel8>>;
%template(CAvg5Filter_Image16)vpl::img::CAvg5Filter<vpl::img::CImage<vpl::img::tPixel16>>;
%template(CAvg5Filter_Image32)vpl::img::CAvg5Filter<vpl::img::CImage<vpl::img::tPixel32>>;
%template(CAvg5Filter_FImage)vpl::img::CAvg5Filter<vpl::img::CImage<vpl::img::tFloatPixel>>;
%template(CAvg5Filter_DImage)vpl::img::CAvg5Filter<vpl::img::CImage<vpl::img::tDensityPixel>>;
//%template(CAvg5Filter_RGBAImage)vpl::img::CAvg5Filter<vpl::img::CImage<vpl::img::tRGBAPixel>>;
//%template(CAvg5Filter_ComplexImage)vpl::img::CAvg5Filter<vpl::img::CImage<vpl::img::tComplexPixel>>;



%template(CAvg7Filter_Image8)vpl::img::CAvg7Filter<vpl::img::CImage<vpl::img::tPixel8>>;
%template(CAvg7Filter_Image16)vpl::img::CAvg7Filter<vpl::img::CImage<vpl::img::tPixel16>>;
%template(CAvg7Filter_Image32)vpl::img::CAvg7Filter<vpl::img::CImage<vpl::img::tPixel32>>;
%template(CAvg7Filter_FImage)vpl::img::CAvg7Filter<vpl::img::CImage<vpl::img::tFloatPixel>>;
%template(CAvg7Filter_DImage)vpl::img::CAvg7Filter<vpl::img::CImage<vpl::img::tDensityPixel>>;
//%template(CAvg7Filter_RGBAImage)vpl::img::CAvg7Filter<vpl::img::CImage<vpl::img::tRGBAPixel>>;
//%template(CAvg7Filter_ComplexImage)vpl::img::CAvg7Filter<vpl::img::CImage<vpl::img::tComplexPixel>>;