//==============================================================================
// This file is part of
//
// VPLswig - SWIG-based VPL bindings for Python
// Changes are Copyright 2016 3Dim Laboratory s.r.o.
// All rights reserved.
//
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.
//==============================================================================

%{

#include <VPL/Image/Filters/Gaussian.h>
%}
%ignore vpl::img::gaussian;

%include "VPL/Image/Filters/Gaussian.h"
%template(CGaussFilter_Image8)vpl::img::CGaussFilter<vpl::img::CImage<vpl::img::tPixel8>>;
%template(CGaussFilter_Image16)vpl::img::CGaussFilter<vpl::img::CImage<vpl::img::tPixel16>>;
%template(CGaussFilter_Image32)vpl::img::CGaussFilter<vpl::img::CImage<vpl::img::tPixel32>>;
%template(CGaussFilter_FImage)vpl::img::CGaussFilter<vpl::img::CImage<vpl::img::tFloatPixel>>;
%template(CGaussFilter_DImage)vpl::img::CGaussFilter<vpl::img::CImage<vpl::img::tDensityPixel>>;
//%template(CGaussFilter_RGBAImage)vpl::img::CGaussFilter<vpl::img::CImage<vpl::img::tRGBAPixel>>;
//%template(CGaussFilter_ComplexImage)vpl::img::CGaussFilter<vpl::img::CImage<vpl::img::tComplexPixel>>;