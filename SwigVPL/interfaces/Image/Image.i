//==============================================================================
// This file is part of
//
// VPLswig - SWIG-based VPL bindings for Python
// Changes are Copyright 2016 3Dim Laboratory s.r.o.
// All rights reserved.
//
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.
//==============================================================================
// Module description 
%module (directors = "1", package = "VPLSwig.Image") Image
%include "../Core/Common.i"
%import "../Core/Core.i"
%import "../Core/Geometry.i"
%import "../Core/Color.i"
%feature("director");
%{
#include <VPL/Image/ImageView.h>
#include <VPL/Image/ImageBase.h>
#include <VPL/Base/RefData.h>
#include <VPL/Image/Image.h>
%}
%include "VPL/Image/ImageView.h"
%include "VPL/Image/ImageBase.h"
%include "VPL/Base/RefData.h"
%include "VPL/Module/Serializable.h"

%include "VPL/Image/Image.h"

%template(swig_base_Image8) vpl::img::CImageBase< vpl::img::CImage<vpl::img::tPixel8,  VPL_IMAGE_DATA_ALLOCATOR> >;
%template(swig_base_Image16) vpl::img::CImageBase< vpl::img::CImage<vpl::img::tPixel16,  VPL_IMAGE_DATA_ALLOCATOR> >;
%template(swig_base_Image32) vpl::img::CImageBase< vpl::img::CImage<vpl::img::tPixel32, VPL_IMAGE_DATA_ALLOCATOR> >;
%template(swig_base_FImage) vpl::img::CImageBase< vpl::img::CImage<vpl::img::tFloatPixel, VPL_IMAGE_DATA_ALLOCATOR> >;
%template(swig_base_DImage) vpl::img::CImageBase< vpl::img::CImage<vpl::img::tDensityPixel, VPL_IMAGE_DATA_ALLOCATOR> >;
%template(swig_base_RGBImage) vpl::img::CImageBase< vpl::img::CImage<vpl::img::tRGBPixel, VPL_IMAGE_DATA_ALLOCATOR> >;
%template(swig_base_ComplexImage) vpl::img::CImageBase< vpl::img::CImage<vpl::img::tComplexPixel, VPL_IMAGE_DATA_ALLOCATOR> >;


%template(convert_from_Image8) vpl::img::CImage::convert<vpl::img::CImage<vpl::img::tPixel8, VPL_IMAGE_DATA_ALLOCATOR> >;
%template(convert_from_Image16) vpl::img::CImage::convert< vpl::img::CImage<vpl::img::tPixel16, VPL_IMAGE_DATA_ALLOCATOR> >;
%template(convert_from_Image32) vpl::img::CImage::convert< vpl::img::CImage<vpl::img::tPixel32, VPL_IMAGE_DATA_ALLOCATOR> >;
%template(convert_from_FImage) vpl::img::CImage::convert< vpl::img::CImage<vpl::img::tFloatPixel, VPL_IMAGE_DATA_ALLOCATOR> >;
%template(convert_from_DImage) vpl::img::CImage::convert< vpl::img::CImage<vpl::img::tDensityPixel, VPL_IMAGE_DATA_ALLOCATOR> >;
%template(convert_from_RGBImage) vpl::img::CImage::convert< vpl::img::CImage<vpl::img::tRGBPixel, VPL_IMAGE_DATA_ALLOCATOR> >;
%template(convert_from_ComplexImage) vpl::img::CImage::convert<vpl::img::CImage<vpl::img::tComplexPixel, VPL_IMAGE_DATA_ALLOCATOR> >;


%template(CImage8) vpl::img::CImage<vpl::img::tPixel8, VPL_IMAGE_DATA_ALLOCATOR>;
%template(CImage16) vpl::img::CImage<vpl::img::tPixel16, VPL_IMAGE_DATA_ALLOCATOR>;
%template(CImage32) vpl::img::CImage<vpl::img::tPixel32, VPL_IMAGE_DATA_ALLOCATOR>;
%template(CFImage) vpl::img::CImage<vpl::img::tFloatPixel, VPL_IMAGE_DATA_ALLOCATOR>;
%template(CDImage) vpl::img::CImage<vpl::img::tDensityPixel, VPL_IMAGE_DATA_ALLOCATOR>;
%template(CRGBImage) vpl::img::CImage<vpl::img::tRGBPixel, VPL_IMAGE_DATA_ALLOCATOR>;
%template(CComplexImage) vpl::img::CImage<vpl::img::tComplexPixel, VPL_IMAGE_DATA_ALLOCATOR>;


//%extend vpl::img::CImage<vpl::img::tPixel8, VPL_IMAGE_DATA_ALLOCATOR> {
//    void add(vpl::img::CImage<vpl::img::tPixel8, VPL_IMAGE_DATA_ALLOCATOR> image) {
//        vpl::img::CImage8 res ;
//    }
//}

//%inline %{
//    vpl::img::CImage<vpl::img::tPixel8, VPL_IMAGE_DATA_ALLOCATOR> CImage_Add(vpl::img::CImage<vpl::img::tPixel8, VPL_IMAGE_DATA_ALLOCATOR>&l, vpl::img::CImage<vpl::img::tPixel8, VPL_IMAGE_DATA_ALLOCATOR>& r) {
//        vpl::img::CImage<vpl::img::tPixel8, VPL_IMAGE_DATA_ALLOCATOR> res(l);
//        return vpl::img::CImage<vpl::img::tPixel8, VPL_IMAGE_DATA_ALLOCATOR>(res += r);
//    }
//%}
%include "Slice.i"

//globals()[var_name] = image
//%pythoncode %{
//    def save_image(var_name) :
//        return "ano neco to udelalo"
//
//
//    def get_image(var_name) :
//        return var_name
//%}