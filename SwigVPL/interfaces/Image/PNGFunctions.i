//==============================================================================
// This file is part of
//
// VPLswig - SWIG-based VPL bindings for Python
// Changes are Copyright 2016 3Dim Laboratory s.r.o.
// All rights reserved.
//
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.
//==============================================================================

// Module description 
%module (package = "VPLSwig.Image") IO
%include "../Core/Common.i"
%import "../Core/Core.i"
%import "Image.i"

///* SWIG defines functuions and types*/
%include "stdint.i"
%include <std_string.i>
%include <std_except.i>

%{
#include <VPL\Image\Slice.h>
#include <VPL\ImageIO\DicomSlice.h>
#include <VPL\ImageIO\DicomSliceLoader.h>
#include <VPL\ImageIO\DicomSliceLoaderVPL.h>

#include <VPL\ImageIO\DicomDirLoader.h>
#include <VPL\ImageIO\PNGBaseFunctions.h>
%}

%include "VPL\ImageIO\PNGBaseFunctions.h"