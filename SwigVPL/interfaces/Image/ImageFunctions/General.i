//==============================================================================
// This file is part of
//
// VPLswig - SWIG-based VPL bindings for Python
// Changes are Copyright 2016 3Dim Laboratory s.r.o.
// All rights reserved.
//
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.
//==============================================================================

%{
#include <VPL/Image/ImageFunctions/General.h>
%}

%include "VPL/Image/ImageFunctions/General.h"


/*********************** Get Max *********************************/
%template(getMaxPixel8_Image8)vpl::img::getMax<vpl::img::tPixel8,vpl::img::CImage<vpl::img::tPixel8, VPL_IMAGE_DATA_ALLOCATOR>>;
%template(getMaxPixel16_Image16)vpl::img::getMax<vpl::img::tPixel16, vpl::img::CImage<vpl::img::tPixel16, VPL_IMAGE_DATA_ALLOCATOR>>;
%template(getMaxPixel32_Image32)vpl::img::getMax<vpl::img::tPixel32, vpl::img::CImage<vpl::img::tPixel32, VPL_IMAGE_DATA_ALLOCATOR>>;
%template(getMaxFloat_FImage)vpl::img::getMax<vpl::img::tFloatPixel, vpl::img::CImage<vpl::img::tFloatPixel, VPL_IMAGE_DATA_ALLOCATOR>>;
%template(getMaxDensity_DImage)vpl::img::getMax<vpl::img::tDensityPixel, vpl::img::CImage<vpl::img::tDensityPixel, VPL_IMAGE_DATA_ALLOCATOR>>;
%template(getMaxRGB_RGBAImage)vpl::img::getMax<vpl::img::tRGBPixel, vpl::img::CImage<vpl::img::tRGBPixel, VPL_IMAGE_DATA_ALLOCATOR>>;
%template(getMaxComplex_ComplexImage)vpl::img::getMax<vpl::img::tComplexPixel, vpl::img::CImage<vpl::img::tComplexPixel, VPL_IMAGE_DATA_ALLOCATOR>>;

%template(getMaxDouble_Image8)vpl::img::getMax<double, vpl::img::CImage<vpl::img::tPixel8, VPL_IMAGE_DATA_ALLOCATOR>>;
%template(getMaxDouble_Image16)vpl::img::getMax<double, vpl::img::CImage<vpl::img::tPixel16, VPL_IMAGE_DATA_ALLOCATOR>>;
%template(getMaxDouble_Image32)vpl::img::getMax<double, vpl::img::CImage<vpl::img::tPixel32, VPL_IMAGE_DATA_ALLOCATOR>>;
%template(getMaxDouble_FImage)vpl::img::getMax<double, vpl::img::CImage<vpl::img::tFloatPixel, VPL_IMAGE_DATA_ALLOCATOR>>;
%template(getMaxDouble_DImage)vpl::img::getMax<double, vpl::img::CImage<vpl::img::tDensityPixel, VPL_IMAGE_DATA_ALLOCATOR>>;
//%template(getMaxDouble_RGBAImage)vpl::img::getMax<double, vpl::img::CImage<vpl::img::tRGBPixel, VPL_IMAGE_DATA_ALLOCATOR>>;
//%template(getMaxDouble_ComplexImage)vpl::img::getMax<double, vpl::img::CImage<vpl::img::tComplexPixel, VPL_IMAGE_DATA_ALLOCATOR>>;

/*********************** Get Min *********************************/
%template(getMinPixel8_Image8)vpl::img::getMin<vpl::img::tPixel8, vpl::img::CImage<vpl::img::tPixel8, VPL_IMAGE_DATA_ALLOCATOR>>;
%template(getMinPixel16_Image16)vpl::img::getMin<vpl::img::tPixel16, vpl::img::CImage<vpl::img::tPixel16, VPL_IMAGE_DATA_ALLOCATOR>>;
%template(getMinPixel32_Image32)vpl::img::getMin<vpl::img::tPixel32, vpl::img::CImage<vpl::img::tPixel32, VPL_IMAGE_DATA_ALLOCATOR>>;
%template(getMinFloat_FImage)vpl::img::getMin<vpl::img::tFloatPixel, vpl::img::CImage<vpl::img::tFloatPixel, VPL_IMAGE_DATA_ALLOCATOR>>;
%template(getMinDensity_DImage)vpl::img::getMin<vpl::img::tDensityPixel, vpl::img::CImage<vpl::img::tDensityPixel, VPL_IMAGE_DATA_ALLOCATOR>>;
%template(getMinRGBA_RGBAImage)vpl::img::getMin<vpl::img::tRGBPixel, vpl::img::CImage<vpl::img::tRGBPixel, VPL_IMAGE_DATA_ALLOCATOR>>;
%template(getMinComplex_ComplexImage)vpl::img::getMin<vpl::img::tComplexPixel, vpl::img::CImage<vpl::img::tComplexPixel, VPL_IMAGE_DATA_ALLOCATOR>>;
          
%template(getMinDouble_Image8)vpl::img::getMin<double,vpl::img::CImage<vpl::img::tPixel8, VPL_IMAGE_DATA_ALLOCATOR>>;
%template(getMinDouble_Image16)vpl::img::getMin<double, vpl::img::CImage<vpl::img::tPixel16, VPL_IMAGE_DATA_ALLOCATOR>>;
%template(getMinDouble_Image32)vpl::img::getMin<double, vpl::img::CImage<vpl::img::tPixel32, VPL_IMAGE_DATA_ALLOCATOR>>;
%template(getMinDouble_FImage)vpl::img::getMin<double, vpl::img::CImage<vpl::img::tFloatPixel, VPL_IMAGE_DATA_ALLOCATOR>>;
%template(getMinDouble_DImage)vpl::img::getMin<double, vpl::img::CImage<vpl::img::tDensityPixel, VPL_IMAGE_DATA_ALLOCATOR>>;
//%template(getMinDouble_RGBAImage)vpl::img::getMin<double, vpl::img::CImage<vpl::img::tRGBPixel, VPL_IMAGE_DATA_ALLOCATOR>>;
//%template(getMinDouble_ComplexImage)vpl::img::getMin<double, vpl::img::CImage<vpl::img::tComplexPixel, VPL_IMAGE_DATA_ALLOCATOR>>;


/*********************** Get Variance *********************************/
%template(getVariancePixel8_Image8)vpl::img::getVariance<vpl::img::tPixel8, vpl::img::CImage<vpl::img::tPixel8, VPL_IMAGE_DATA_ALLOCATOR>>;
%template(getVariancePixel16_Image16)vpl::img::getVariance<vpl::img::tPixel16, vpl::img::CImage<vpl::img::tPixel16, VPL_IMAGE_DATA_ALLOCATOR>>;
%template(getVariancePixel32_Image32)vpl::img::getVariance<vpl::img::tPixel32, vpl::img::CImage<vpl::img::tPixel32, VPL_IMAGE_DATA_ALLOCATOR>>;
%template(getVarianceFloat_FImage)vpl::img::getVariance<vpl::img::tFloatPixel, vpl::img::CImage<vpl::img::tFloatPixel, VPL_IMAGE_DATA_ALLOCATOR>>;
%template(getVarianceDensity_DImage)vpl::img::getVariance<vpl::img::tDensityPixel, vpl::img::CImage<vpl::img::tDensityPixel, VPL_IMAGE_DATA_ALLOCATOR>>;
//%template(getVarianceRGBA_RGBAImage)vpl::img::getVariance<vpl::img::tRGBPixel, vpl::img::CImage<vpl::img::tRGBPixel, VPL_IMAGE_DATA_ALLOCATOR>>;
//%template(getVarianceComplex_ComplexImage)vpl::img::getVariance<vpl::img::tComplexPixel, vpl::img::CImage<vpl::img::tComplexPixel, VPL_IMAGE_DATA_ALLOCATOR>>;


%template(getVarianceDouble_Image8)vpl::img::getVariance<double, vpl::img::CImage<vpl::img::tPixel8, VPL_IMAGE_DATA_ALLOCATOR>>;
%template(getVarianceDouble_Image16)vpl::img::getVariance<double, vpl::img::CImage<vpl::img::tPixel16, VPL_IMAGE_DATA_ALLOCATOR>>;
%template(getVarianceDouble_Image32)vpl::img::getVariance<double, vpl::img::CImage<vpl::img::tPixel32, VPL_IMAGE_DATA_ALLOCATOR>>;
%template(getVarianceDouble_FImage)vpl::img::getVariance<double, vpl::img::CImage<vpl::img::tFloatPixel, VPL_IMAGE_DATA_ALLOCATOR>>;
%template(getVarianceDouble_DImage)vpl::img::getVariance<double, vpl::img::CImage<vpl::img::tDensityPixel, VPL_IMAGE_DATA_ALLOCATOR>>;
//%template(getVarianceDouble_RGBAImage)vpl::img::getVariance<vpl::img::tRGBPixel, vpl::img::CImage<vpl::img::tRGBPixel, VPL_IMAGE_DATA_ALLOCATOR>>;
//%template(getVarianceDouble_ComplexImage)vpl::img::getVariance<vpl::img::tComplexPixel, vpl::img::CImage<vpl::img::tComplexPixel, VPL_IMAGE_DATA_ALLOCATOR>>;

/*********************** Get Mean *********************************/
%template(getMeanPixel8_Image8)vpl::img::getMean<vpl::img::tPixel8, vpl::img::CImage<vpl::img::tPixel8, VPL_IMAGE_DATA_ALLOCATOR>>;
%template(getMeanPixel16_Image16)vpl::img::getMean<vpl::img::tPixel16, vpl::img::CImage<vpl::img::tPixel16, VPL_IMAGE_DATA_ALLOCATOR>>;
%template(getMeanPixel32_Image32)vpl::img::getMean<vpl::img::tPixel32, vpl::img::CImage<vpl::img::tPixel32, VPL_IMAGE_DATA_ALLOCATOR>>;
%template(getMeanFloat_FImage)vpl::img::getMean<vpl::img::tFloatPixel, vpl::img::CImage<vpl::img::tFloatPixel, VPL_IMAGE_DATA_ALLOCATOR>>;
%template(getMeanDensity_DImage)vpl::img::getMean<vpl::img::tDensityPixel, vpl::img::CImage<vpl::img::tDensityPixel, VPL_IMAGE_DATA_ALLOCATOR>>;
%template(getMeanRGBA_RGBAImage)vpl::img::getMean<vpl::img::tRGBPixel, vpl::img::CImage<vpl::img::tRGBPixel, VPL_IMAGE_DATA_ALLOCATOR>>;
//%template(getMeanComplex_ComplexImage)vpl::img::getMean<vpl::img::tComplexPixel, vpl::img::CImage<vpl::img::tComplexPixel, VPL_IMAGE_DATA_ALLOCATOR>>;


%template(getMeanDouble_Image8)vpl::img::getMean<double, vpl::img::CImage<vpl::img::tPixel8, VPL_IMAGE_DATA_ALLOCATOR>>;
%template(getMeanDouble_Image16)vpl::img::getMean<double, vpl::img::CImage<vpl::img::tPixel16, VPL_IMAGE_DATA_ALLOCATOR>>;
%template(getMeanDouble_Image32)vpl::img::getMean<double, vpl::img::CImage<vpl::img::tPixel32, VPL_IMAGE_DATA_ALLOCATOR>>;
%template(getMeanDouble_FImage)vpl::img::getMean<double, vpl::img::CImage<vpl::img::tFloatPixel, VPL_IMAGE_DATA_ALLOCATOR>>;
%template(getMeanDouble_DImage)vpl::img::getMean<double, vpl::img::CImage<vpl::img::tDensityPixel, VPL_IMAGE_DATA_ALLOCATOR>>;
//%template(getMeanDouble_RGBAImage)vpl::img::getMean<double, vpl::img::CImage<vpl::img::tRGBPixel, VPL_IMAGE_DATA_ALLOCATOR>>;
//%template(getMeanDouble_ComplexImage)vpl::img::getMean<double, vpl::img::CImage<vpl::img::tComplexPixel, VPL_IMAGE_DATA_ALLOCATOR>>;

/*********************** Get Sum Of Squares *********************************/
%template(getSumOfSquaresPixel8_Image8)vpl::img::getSumOfSquares<vpl::img::tPixel8, vpl::img::CImage<vpl::img::tPixel8, VPL_IMAGE_DATA_ALLOCATOR>>;
%template(getSumOfSquaresPixel16_Image16)vpl::img::getSumOfSquares<vpl::img::tPixel16, vpl::img::CImage<vpl::img::tPixel16, VPL_IMAGE_DATA_ALLOCATOR>>;
%template(getSumOfSquaresPixel32_Image32)vpl::img::getSumOfSquares<vpl::img::tPixel32, vpl::img::CImage<vpl::img::tPixel32, VPL_IMAGE_DATA_ALLOCATOR>>;
%template(getSumOfSquaresFloat_FImage)vpl::img::getSumOfSquares<vpl::img::tFloatPixel, vpl::img::CImage<vpl::img::tFloatPixel, VPL_IMAGE_DATA_ALLOCATOR>>;
%template(getSumOfSquaresDensity_DImage)vpl::img::getSumOfSquares<vpl::img::tDensityPixel, vpl::img::CImage<vpl::img::tDensityPixel, VPL_IMAGE_DATA_ALLOCATOR>>;
//%template(getSumOfSquaresRGBA_RGBAImage)vpl::img::getSumOfSquares<vpl::img::tRGBPixel, vpl::img::CImage<vpl::img::tRGBPixel, VPL_IMAGE_DATA_ALLOCATOR>>;
%template(getSumOfSquaresComplex_ComplexImage)vpl::img::getSumOfSquares<vpl::img::tComplexPixel, vpl::img::CImage<vpl::img::tComplexPixel, VPL_IMAGE_DATA_ALLOCATOR>>;


%template(getSumOfSquaresDouble_Image8)vpl::img::getSumOfSquares<double, vpl::img::CImage<vpl::img::tPixel8, VPL_IMAGE_DATA_ALLOCATOR>>;
%template(getSumOfSquaresDouble_Image16)vpl::img::getSumOfSquares<double, vpl::img::CImage<vpl::img::tPixel16, VPL_IMAGE_DATA_ALLOCATOR>>;
%template(getSumOfSquaresDouble_Image32)vpl::img::getSumOfSquares<double, vpl::img::CImage<vpl::img::tPixel32, VPL_IMAGE_DATA_ALLOCATOR>>;
%template(getSumOfSquaresDouble_FImage)vpl::img::getSumOfSquares<double, vpl::img::CImage<vpl::img::tFloatPixel, VPL_IMAGE_DATA_ALLOCATOR>>;
%template(getSumOfSquaresDouble_DImage)vpl::img::getSumOfSquares<double, vpl::img::CImage<vpl::img::tDensityPixel, VPL_IMAGE_DATA_ALLOCATOR>>;
//%template(getSumOfSquaresDouble_RGBAImage)vpl::img::getSumOfSquares<vpl::img::tRGBPixel, vpl::img::CImage<vpl::img::tRGBPixel, VPL_IMAGE_DATA_ALLOCATOR>>;
//%template(getSumOfSquaresDouble_ComplexImage)vpl::img::getSumOfSquares<vpl::img::tComplexPixel, vpl::img::CImage<vpl::img::tComplexPixel, VPL_IMAGE_DATA_ALLOCATOR>>;

/*********************** Get Sum *********************************/
%template(getSumPixel8_Image8)vpl::img::getSum<vpl::img::tPixel8, vpl::img::CImage<vpl::img::tPixel8, VPL_IMAGE_DATA_ALLOCATOR>>;
%template(getSumPixel16_Image16)vpl::img::getSum<vpl::img::tPixel16, vpl::img::CImage<vpl::img::tPixel16, VPL_IMAGE_DATA_ALLOCATOR>>;
%template(getSumPixel32_Image32)vpl::img::getSum<vpl::img::tPixel32, vpl::img::CImage<vpl::img::tPixel32, VPL_IMAGE_DATA_ALLOCATOR>>;
%template(getSumFloat_FImage)vpl::img::getSum<vpl::img::tFloatPixel, vpl::img::CImage<vpl::img::tFloatPixel, VPL_IMAGE_DATA_ALLOCATOR>>;
%template(getSumDensity_DImage)vpl::img::getSum<vpl::img::tDensityPixel, vpl::img::CImage<vpl::img::tDensityPixel, VPL_IMAGE_DATA_ALLOCATOR>>;
%template(getSumRGBA_RGBAImage)vpl::img::getSum<vpl::img::tRGBPixel, vpl::img::CImage<vpl::img::tRGBPixel, VPL_IMAGE_DATA_ALLOCATOR>>;
%template(getSumComplex_ComplexImage)vpl::img::getSum<vpl::img::tComplexPixel, vpl::img::CImage<vpl::img::tComplexPixel, VPL_IMAGE_DATA_ALLOCATOR>>;
          
%template(getSumDouble_Image8)vpl::img::getSum<double, vpl::img::CImage<vpl::img::tPixel8, VPL_IMAGE_DATA_ALLOCATOR>>;
%template(getSumDouble_Image16)vpl::img::getSum<double, vpl::img::CImage<vpl::img::tPixel16, VPL_IMAGE_DATA_ALLOCATOR>>;
%template(getSumDouble_Image32)vpl::img::getSum<double, vpl::img::CImage<vpl::img::tPixel32, VPL_IMAGE_DATA_ALLOCATOR>>;
%template(getSumDouble_FImage)vpl::img::getSum<double, vpl::img::CImage<vpl::img::tFloatPixel, VPL_IMAGE_DATA_ALLOCATOR>>;
%template(getSumDouble_DImage)vpl::img::getSum<double, vpl::img::CImage<vpl::img::tDensityPixel, VPL_IMAGE_DATA_ALLOCATOR>>;
//%template(getSumDouble_RGBAImage)vpl::img::getSum<double, vpl::img::CImage<vpl::img::tRGBPixel, VPL_IMAGE_DATA_ALLOCATOR>>;
//%template(getSumDouble_ComplexImage)vpl::img::getSum<double, vpl::img::CImage<vpl::img::tComplexPixel, VPL_IMAGE_DATA_ALLOCATOR>>;



/*********************** Get Mean Square Error *********************************/
%template(getMeanSquareErrorPixel8_Image8)vpl::img::getMeanSquareError<vpl::img::tPixel8, vpl::img::CImage<vpl::img::tPixel8, VPL_IMAGE_DATA_ALLOCATOR>, vpl::img::CImage<vpl::img::tPixel8, VPL_IMAGE_DATA_ALLOCATOR>>;
%template(getMeanSquareErrorPixel16_Image16)vpl::img::getMeanSquareError<vpl::img::tPixel16, vpl::img::CImage<vpl::img::tPixel16, VPL_IMAGE_DATA_ALLOCATOR>, vpl::img::CImage<vpl::img::tPixel16, VPL_IMAGE_DATA_ALLOCATOR>>;
%template(getMeanSquareErrorPixel32_Image32)vpl::img::getMeanSquareError<vpl::img::tPixel32, vpl::img::CImage<vpl::img::tPixel32, VPL_IMAGE_DATA_ALLOCATOR>, vpl::img::CImage<vpl::img::tPixel32, VPL_IMAGE_DATA_ALLOCATOR>>;
%template(getMeanSquareErrorFloat_FImage)vpl::img::getMeanSquareError<vpl::img::tFloatPixel, vpl::img::CImage<vpl::img::tFloatPixel, VPL_IMAGE_DATA_ALLOCATOR>, vpl::img::CImage<vpl::img::tFloatPixel, VPL_IMAGE_DATA_ALLOCATOR>>;
%template(getMeanSquareErrorDensity_DImage)vpl::img::getMeanSquareError<vpl::img::tDensityPixel, vpl::img::CImage<vpl::img::tDensityPixel, VPL_IMAGE_DATA_ALLOCATOR>, vpl::img::CImage<vpl::img::tDensityPixel, VPL_IMAGE_DATA_ALLOCATOR>>;
//%template(getMeanSquareErrorRGBA_RGBAImage)vpl::img::getMeanSquareError<vpl::img::tRGBPixel, vpl::img::CImage<vpl::img::tRGBPixel, VPL_IMAGE_DATA_ALLOCATOR>, vpl::img::CImage<vpl::img::tRGBPixel, VPL_IMAGE_DATA_ALLOCATOR>>;
//%template(getMeanSquareErrorComplex_ComplexImage)vpl::img::getMeanSquareError<vpl::img::tComplexPixel, vpl::img::CImage<vpl::img::tComplexPixel, VPL_IMAGE_DATA_ALLOCATOR>, vpl::img::CImage<vpl::img::tComplexPixel, VPL_IMAGE_DATA_ALLOCATOR>>;


%template(getMeanSquareErrorDouble_Image8)vpl::img::getMeanSquareError<double, vpl::img::CImage<vpl::img::tPixel8, VPL_IMAGE_DATA_ALLOCATOR>, vpl::img::CImage<vpl::img::tPixel8, VPL_IMAGE_DATA_ALLOCATOR>>;
%template(getMeanSquareErrorDouble_Image16)vpl::img::getMeanSquareError<double, vpl::img::CImage<vpl::img::tPixel16, VPL_IMAGE_DATA_ALLOCATOR>, vpl::img::CImage<vpl::img::tPixel16, VPL_IMAGE_DATA_ALLOCATOR>>;
%template(getMeanSquareErrorDouble_Image32)vpl::img::getMeanSquareError<double, vpl::img::CImage<vpl::img::tPixel32, VPL_IMAGE_DATA_ALLOCATOR>, vpl::img::CImage<vpl::img::tPixel32, VPL_IMAGE_DATA_ALLOCATOR>>;
%template(getMeanSquareErrorDouble_FImage)vpl::img::getMeanSquareError<double, vpl::img::CImage<vpl::img::tFloatPixel, VPL_IMAGE_DATA_ALLOCATOR>, vpl::img::CImage<vpl::img::tFloatPixel, VPL_IMAGE_DATA_ALLOCATOR>>;
%template(getMeanSquareErrorDouble_DImage)vpl::img::getMeanSquareError<double, vpl::img::CImage<vpl::img::tDensityPixel, VPL_IMAGE_DATA_ALLOCATOR>, vpl::img::CImage<vpl::img::tDensityPixel, VPL_IMAGE_DATA_ALLOCATOR>>;
//%template(getMeanSquareErrorDouble_RGBAImage)vpl::img::getMeanSquareError<double, vpl::img::CImage<vpl::img::tRGBPixel, VPL_IMAGE_DATA_ALLOCATOR>, vpl::img::CImage<vpl::img::tRGBPixel, VPL_IMAGE_DATA_ALLOCATOR>>;
//%template(getMeanSquareErrorDouble_ComplexImage)vpl::img::getMeanSquareError<double, vpl::img::CImage<vpl::img::tComplexPixel, VPL_IMAGE_DATA_ALLOCATOR>, vpl::img::CImage<vpl::img::tComplexPixel, VPL_IMAGE_DATA_ALLOCATOR>>;


/*********************** Get Product *********************************/
%template(getProductPixel8_Image8)vpl::img::getProduct<vpl::img::tPixel8, vpl::img::CImage<vpl::img::tPixel8, VPL_IMAGE_DATA_ALLOCATOR>, vpl::img::CImage<vpl::img::tPixel8, VPL_IMAGE_DATA_ALLOCATOR>>;
%template(getProductPixel16_Image16)vpl::img::getProduct<vpl::img::tPixel16, vpl::img::CImage<vpl::img::tPixel16, VPL_IMAGE_DATA_ALLOCATOR>, vpl::img::CImage<vpl::img::tPixel16, VPL_IMAGE_DATA_ALLOCATOR>>;
%template(getProductPixel32_Image32)vpl::img::getProduct<vpl::img::tPixel32, vpl::img::CImage<vpl::img::tPixel32, VPL_IMAGE_DATA_ALLOCATOR>, vpl::img::CImage<vpl::img::tPixel32, VPL_IMAGE_DATA_ALLOCATOR>>;
%template(getProductFloat_FImage)vpl::img::getProduct<vpl::img::tFloatPixel, vpl::img::CImage<vpl::img::tFloatPixel, VPL_IMAGE_DATA_ALLOCATOR>, vpl::img::CImage<vpl::img::tFloatPixel, VPL_IMAGE_DATA_ALLOCATOR>>;
%template(getProductDensity_DImage)vpl::img::getProduct<vpl::img::tDensityPixel, vpl::img::CImage<vpl::img::tDensityPixel, VPL_IMAGE_DATA_ALLOCATOR>, vpl::img::CImage<vpl::img::tDensityPixel, VPL_IMAGE_DATA_ALLOCATOR>>;
%template(getProductRGBA_RGBAImage)vpl::img::getProduct<vpl::img::tRGBPixel, vpl::img::CImage<vpl::img::tRGBPixel, VPL_IMAGE_DATA_ALLOCATOR>, vpl::img::CImage<vpl::img::tRGBPixel, VPL_IMAGE_DATA_ALLOCATOR>>;
%template(getProductComplex_ComplexImage)vpl::img::getProduct<vpl::img::tComplexPixel, vpl::img::CImage<vpl::img::tComplexPixel, VPL_IMAGE_DATA_ALLOCATOR>, vpl::img::CImage<vpl::img::tComplexPixel, VPL_IMAGE_DATA_ALLOCATOR>>;


%template(getProductDouble_Image8)vpl::img::getProduct<double, vpl::img::CImage<vpl::img::tPixel8, VPL_IMAGE_DATA_ALLOCATOR>, vpl::img::CImage<vpl::img::tPixel8, VPL_IMAGE_DATA_ALLOCATOR>>;
%template(getProductDouble_Image16)vpl::img::getProduct<double, vpl::img::CImage<vpl::img::tPixel16, VPL_IMAGE_DATA_ALLOCATOR>, vpl::img::CImage<vpl::img::tPixel16, VPL_IMAGE_DATA_ALLOCATOR>>;
%template(getProductDouble_Image32)vpl::img::getProduct<double, vpl::img::CImage<vpl::img::tPixel32, VPL_IMAGE_DATA_ALLOCATOR>, vpl::img::CImage<vpl::img::tPixel32, VPL_IMAGE_DATA_ALLOCATOR>>;
%template(getProductDouble_FImage)vpl::img::getProduct<double, vpl::img::CImage<vpl::img::tFloatPixel, VPL_IMAGE_DATA_ALLOCATOR>, vpl::img::CImage<vpl::img::tFloatPixel, VPL_IMAGE_DATA_ALLOCATOR>>;
%template(getProductDouble_DImage)vpl::img::getProduct<double, vpl::img::CImage<vpl::img::tDensityPixel, VPL_IMAGE_DATA_ALLOCATOR>, vpl::img::CImage<vpl::img::tDensityPixel, VPL_IMAGE_DATA_ALLOCATOR>>;
//%template(getProductDouble_RGBAImage)vpl::img::getProduct<double, vpl::img::CImage<vpl::img::tRGBPixel, VPL_IMAGE_DATA_ALLOCATOR>, vpl::img::CImage<vpl::img::tRGBPixel, VPL_IMAGE_DATA_ALLOCATOR>>;
//%template(getProductDouble_ComplexImage)vpl::img::getProduct<double, vpl::img::CImage<vpl::img::tComplexPixel, VPL_IMAGE_DATA_ALLOCATOR>, vpl::img::CImage<vpl::img::tComplexPixel, VPL_IMAGE_DATA_ALLOCATOR>>;



