//==============================================================================
// This file is part of
//
// VPLswig - SWIG-based VPL bindings for Python
// Changes are Copyright 2016 3Dim Laboratory s.r.o.
// All rights reserved.
//
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.
//==============================================================================



#ifdef VPL_FFTW_ENABLED

%{
#include <VPL/Image/ImageFunctions/FFT.h>
%}

%include "VPL/Image/ImageFunctions/FFT.h"

/*********************** FFT *********************************/
%template(fft_Image8)vpl::img::fft<vpl::img::CImage<vpl::img::tPixel8, VPL_IMAGE_DATA_ALLOCATOR>>;
%template(fft_Image16)vpl::img::fft<vpl::img::CImage<vpl::img::tPixel16, VPL_IMAGE_DATA_ALLOCATOR>>;
%template(fft_Image32)vpl::img::fft<vpl::img::CImage<vpl::img::tPixel32, VPL_IMAGE_DATA_ALLOCATOR>>;
%template(fft_FImage)vpl::img::fft<vpl::img::CImage<vpl::img::tFloatPixel, VPL_IMAGE_DATA_ALLOCATOR>>;
%template(fft_DImage)vpl::img::fft<vpl::img::CImage<vpl::img::tDensityPixel, VPL_IMAGE_DATA_ALLOCATOR>>;
%template(fft_RGBAImage)vpl::img::fft<vpl::img::CImage<vpl::img::tRGBPixel, VPL_IMAGE_DATA_ALLOCATOR>>;
%template(fft_ComplexImage)vpl::img::fft<vpl::img::CImage<vpl::img::tComplexPixel, VPL_IMAGE_DATA_ALLOCATOR>>;

/*********************** IFFT *********************************/
%template(ifft_Image8)vpl::img::ifft<vpl::img::CImage<vpl::img::tPixel8, VPL_IMAGE_DATA_ALLOCATOR>>;
%template(ifft_Image16)vpl::img::ifft<vpl::img::CImage<vpl::img::tPixel16, VPL_IMAGE_DATA_ALLOCATOR>>;
%template(ifft_Image32)vpl::img::ifft<vpl::img::CImage<vpl::img::tPixel32, VPL_IMAGE_DATA_ALLOCATOR>>;
%template(ifft_FImage)vpl::img::ifft<vpl::img::CImage<vpl::img::tFloatPixel, VPL_IMAGE_DATA_ALLOCATOR>>;
%template(ifft_DImage)vpl::img::ifft<vpl::img::CImage<vpl::img::tDensityPixel, VPL_IMAGE_DATA_ALLOCATOR>>;
%template(ifft_RGBAImage)vpl::img::ifft<vpl::img::CImage<vpl::img::tRGBPixel, VPL_IMAGE_DATA_ALLOCATOR>>;
%template(ifft_ComplexImage)vpl::img::ifft<vpl::img::CImage<vpl::img::tComplexPixel, VPL_IMAGE_DATA_ALLOCATOR>>;


/*********************** FFT Shift *********************************/
%template(fftShift_Image8)vpl::img::fftShift<vpl::img::CImage<vpl::img::tPixel8, VPL_IMAGE_DATA_ALLOCATOR>>;
%template(fftShift_Image16)vpl::img::fftShift<vpl::img::CImage<vpl::img::tPixel16, VPL_IMAGE_DATA_ALLOCATOR>>;
%template(fftShift_Image32)vpl::img::fftShift<vpl::img::CImage<vpl::img::tPixel32, VPL_IMAGE_DATA_ALLOCATOR>>;
%template(fftShift_FImage)vpl::img::fftShift<vpl::img::CImage<vpl::img::tFloatPixel, VPL_IMAGE_DATA_ALLOCATOR>>;
%template(fftShift_DImage)vpl::img::fftShift<vpl::img::CImage<vpl::img::tDensityPixel, VPL_IMAGE_DATA_ALLOCATOR>>;
%template(fftShift_RGBAImage)vpl::img::fftShift<vpl::img::CImage<vpl::img::tRGBPixel, VPL_IMAGE_DATA_ALLOCATOR>>;
%template(fftShift_ComplexImage)vpl::img::fftShift<vpl::img::CImage<vpl::img::tComplexPixel, VPL_IMAGE_DATA_ALLOCATOR>>;

#endif // VPL_FFTW_ENABLED

