//==============================================================================
// This file is part of
//
// VPLswig - SWIG-based VPL bindings for Python
// Changes are Copyright 2016 3Dim Laboratory s.r.o.
// All rights reserved.
//
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.
//==============================================================================
// Module description 
%module (package = "VPLSwig.Image") ImageFunctions
%include "../Core/Common.i"
%import "Image.i"

%include "Histogram.i"
%include "General.i"
%include "FFT.i"
%include "Conversion.i"