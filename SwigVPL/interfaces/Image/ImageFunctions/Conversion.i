//==============================================================================
// This file is part of
//
// VPLswig - SWIG-based VPL bindings for Python
// Changes are Copyright 2016 3Dim Laboratory s.r.o.
// All rights reserved.
//
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.
//==============================================================================

%{
#include <VPL/Image/ImageFunctions/Conversion.h>
%}

%include "VPL/Image/ImageFunctions/Conversion.h"


/*********************** Log *********************************/
%template(log_Image8)vpl::img::log<vpl::img::CImage<vpl::img::tPixel8, VPL_IMAGE_DATA_ALLOCATOR>>;
%template(log_Image16)vpl::img::log<vpl::img::CImage<vpl::img::tPixel16, VPL_IMAGE_DATA_ALLOCATOR>>;
%template(log_Image32)vpl::img::log<vpl::img::CImage<vpl::img::tPixel32, VPL_IMAGE_DATA_ALLOCATOR>>;
%template(log_FImage)vpl::img::log<vpl::img::CImage<vpl::img::tFloatPixel, VPL_IMAGE_DATA_ALLOCATOR>>;
%template(log_DImage)vpl::img::log<vpl::img::CImage<vpl::img::tDensityPixel, VPL_IMAGE_DATA_ALLOCATOR>>;
//%template(log_RGBAImage)vpl::img::log<vpl::img::CImage<vpl::img::tRGBPixel, VPL_IMAGE_DATA_ALLOCATOR>>;
//%template(log_ComplexImage)vpl::img::log<vpl::img::CImage<vpl::img::tComplexPixel, VPL_IMAGE_DATA_ALLOCATOR>>;

/*********************** Real *********************************/
//%template(real_Image8)vpl::img::real<vpl::img::CImage<vpl::img::tPixel8, VPL_IMAGE_DATA_ALLOCATOR>>;
//%template(real_Image16)vpl::img::real<vpl::img::CImage<vpl::img::tPixel16, VPL_IMAGE_DATA_ALLOCATOR>>;
//%template(real_Image32)vpl::img::real<vpl::img::CImage<vpl::img::tPixel32, VPL_IMAGE_DATA_ALLOCATOR>>;
//%template(real_FImage)vpl::img::real<vpl::img::CImage<vpl::img::tFloatPixel, VPL_IMAGE_DATA_ALLOCATOR>>;
//%template(real_DImage)vpl::img::real<vpl::img::CImage<vpl::img::tDensityPixel, VPL_IMAGE_DATA_ALLOCATOR>>;
//%template(real_RGBAImage)vpl::img::real<vpl::img::CImage<vpl::img::tRGBPixel, VPL_IMAGE_DATA_ALLOCATOR>>;
//%template(real_ComplexImage)vpl::img::real<vpl::img::CImage<vpl::img::tComplexPixel, VPL_IMAGE_DATA_ALLOCATOR>>;

/*********************** Imag *********************************/
//%template(imag_Image8)vpl::img::imag<vpl::img::CImage<vpl::img::tPixel8, VPL_IMAGE_DATA_ALLOCATOR>>;
//%template(imag_Image16)vpl::img::imag<vpl::img::CImage<vpl::img::tPixel16, VPL_IMAGE_DATA_ALLOCATOR>>;
//%template(imag_Image32)vpl::img::imag<vpl::img::CImage<vpl::img::tPixel32, VPL_IMAGE_DATA_ALLOCATOR>>;
//%template(imag_FImage)vpl::img::imag<vpl::img::CImage<vpl::img::tFloatPixel, VPL_IMAGE_DATA_ALLOCATOR>>;
//%template(imag_DImage)vpl::img::imag<vpl::img::CImage<vpl::img::tDensityPixel, VPL_IMAGE_DATA_ALLOCATOR>>;
//%template(imag_RGBAImage)vpl::img::imag<vpl::img::CImage<vpl::img::tRGBPixel, VPL_IMAGE_DATA_ALLOCATOR>>;
//%template(imag_ComplexImage)vpl::img::imag<vpl::img::CImage<vpl::img::tComplexPixel, VPL_IMAGE_DATA_ALLOCATOR>>;

/*********************** Abs *********************************/
//%template(abs_Image8)vpl::img::abs<vpl::img::CImage<vpl::img::tPixel8, VPL_IMAGE_DATA_ALLOCATOR>>;
//%template(abs_Image16)vpl::img::abs<vpl::img::CImage<vpl::img::tPixel16, VPL_IMAGE_DATA_ALLOCATOR>>;
//%template(abs_Image32)vpl::img::abs<vpl::img::CImage<vpl::img::tPixel32, VPL_IMAGE_DATA_ALLOCATOR>>;
//%template(abs_FImage)vpl::img::abs<vpl::img::CImage<vpl::img::tFloatPixel, VPL_IMAGE_DATA_ALLOCATOR>>;
//%template(abs_DImage)vpl::img::abs<vpl::img::CImage<vpl::img::tDensityPixel, VPL_IMAGE_DATA_ALLOCATOR>>;
//%template(abs_RGBAImage)vpl::img::abs<vpl::img::CImage<vpl::img::tRGBPixel, VPL_IMAGE_DATA_ALLOCATOR>>;
//%template(abs_ComplexImage)vpl::img::abs<vpl::img::CImage<vpl::img::tComplexPixel, VPL_IMAGE_DATA_ALLOCATOR>>;

/*********************** Arg *********************************/
//%template(arg_Image8)vpl::img::arg<vpl::img::CImage<vpl::img::tPixel8, VPL_IMAGE_DATA_ALLOCATOR>>;
//%template(arg_Image16)vpl::img::arg<vpl::img::CImage<vpl::img::tPixel16, VPL_IMAGE_DATA_ALLOCATOR>>;
//%template(arg_Image32)vpl::img::arg<vpl::img::CImage<vpl::img::tPixel32, VPL_IMAGE_DATA_ALLOCATOR>>;
//%template(arg_FImage)vpl::img::arg<vpl::img::CImage<vpl::img::tFloatPixel, VPL_IMAGE_DATA_ALLOCATOR>>;
//%template(arg_DImage)vpl::img::arg<vpl::img::CImage<vpl::img::tDensityPixel, VPL_IMAGE_DATA_ALLOCATOR>>;
//%template(arg_RGBAImage)vpl::img::arg<vpl::img::CImage<vpl::img::tRGBPixel, VPL_IMAGE_DATA_ALLOCATOR>>;
//%template(arg_ComplexImage)vpl::img::arg<vpl::img::CImage<vpl::img::tComplexPixel, VPL_IMAGE_DATA_ALLOCATOR>>;