//==============================================================================
// This file is part of
//
// VPLswig - SWIG-based VPL bindings for Python
// Changes are Copyright 2016 3Dim Laboratory s.r.o.
// All rights reserved.
//
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.
//==============================================================================

%{
#include <VPL/Image/ImageView.h>
#include <VPL/Image/ImageBase.h>
#include <VPL/Base/RefData.h>
#include <VPL/Image/Image.h>

#include <VPL\Image\Slice.h>
#include <VPL\ImageIO\DicomSlice.h>

#include <VPL/Image/HistogramBase.h>
#include <VPL/Image/Histogram.h>
#include <VPL/Base/TypeTraits.h>

#include <VPL/Image/ImageFunctions/Histogram.h>
%}

%import "VPL/Base/TypeTraits.h"
%include "VPL/Image/HistogramBase.h"
%include "VPL/Image/Histogram.h"
%include "VPL/Image/ImageFunctions/Histogram.h"


%template(CHistogram_Image8)vpl::img::CHistogram< vpl::img::CImage<vpl::img::tPixel8, VPL_IMAGE_DATA_ALLOCATOR> >;
%template(CHistogram_Image16)vpl::img::CHistogram< vpl::img::CImage<vpl::img::tPixel16, VPL_IMAGE_DATA_ALLOCATOR> >;
%template(CHistogram_Image32)vpl::img::CHistogram< vpl::img::CImage<vpl::img::tPixel32, VPL_IMAGE_DATA_ALLOCATOR> >;
%template(CHistogram_FImage)vpl::img::CHistogram< vpl::img::CImage<vpl::img::tFloatPixel, VPL_IMAGE_DATA_ALLOCATOR> >;
%template(CHistogram_DImage)vpl::img::CHistogram< vpl::img::CImage<vpl::img::tDensityPixel, VPL_IMAGE_DATA_ALLOCATOR> >;
//%template(CHistogram_RGBImage) vpl::img::CHistogram< vpl::img::CImage<vpl::img::tRGBPixel, VPL_IMAGE_DATA_ALLOCATOR> >;
//%template(CHistogram_ComplexImage) vpl::img::CHistogram< vpl::img::CImage<vpl::img::tComplexPixel, VPL_IMAGE_DATA_ALLOCATOR> >;


%template(CFullHistogram_Image8)vpl::img::CFullHistogram< vpl::img::CImage<vpl::img::tPixel8, VPL_IMAGE_DATA_ALLOCATOR> >;
%template(CFullHistogram_Image16)vpl::img::CFullHistogram< vpl::img::CImage<vpl::img::tPixel16, VPL_IMAGE_DATA_ALLOCATOR> >;
%template(CFullHistogram_Image32)vpl::img::CFullHistogram< vpl::img::CImage<vpl::img::tPixel32, VPL_IMAGE_DATA_ALLOCATOR> >;
//%template(CFullHistogram_FImage)vpl::img::CFullHistogram< vpl::img::CImage<vpl::img::tFloatPixel, VPL_IMAGE_DATA_ALLOCATOR> >;
%template(CFullHistogram_DImage)vpl::img::CFullHistogram< vpl::img::CImage<vpl::img::tDensityPixel, VPL_IMAGE_DATA_ALLOCATOR> >;
//%template(CFullHistogram_RGBImage) vpl::img::CFullHistogram< vpl::img::CImage<vpl::img::tRGBPixel, VPL_IMAGE_DATA_ALLOCATOR> >;
//%template(CFullHistogram_ComplexImage) vpl::img::CFullHistogram< vpl::img::CImage<vpl::img::tComplexPixel, VPL_IMAGE_DATA_ALLOCATOR> >;



/******************************* Histogram functions **********************************************/
%template(histogramEqualization_Image8)vpl::img::histogramEqualization<vpl::img::CImage<vpl::img::tPixel8, VPL_IMAGE_DATA_ALLOCATOR>>;
%template(histogramEqualization_Image16)vpl::img::histogramEqualization<vpl::img::CImage<vpl::img::tPixel16, VPL_IMAGE_DATA_ALLOCATOR>>;
%template(histogramEqualization_Image32)vpl::img::histogramEqualization<vpl::img::CImage<vpl::img::tPixel32, VPL_IMAGE_DATA_ALLOCATOR>>;
%template(histogramEqualization_DImage)vpl::img::histogramEqualization<vpl::img::CImage<vpl::img::tDensityPixel, VPL_IMAGE_DATA_ALLOCATOR>>;


%template(singleThresholding_Image8)vpl::img::singleThresholding<vpl::img::CImage<vpl::img::tPixel8, VPL_IMAGE_DATA_ALLOCATOR>>;
%template(singleThresholding_Image16)vpl::img::singleThresholding<vpl::img::CImage<vpl::img::tPixel16, VPL_IMAGE_DATA_ALLOCATOR>>;
%template(singleThresholding_Image32)vpl::img::singleThresholding<vpl::img::CImage<vpl::img::tPixel32, VPL_IMAGE_DATA_ALLOCATOR>>;
%template(singleThresholding_DImage)vpl::img::singleThresholding<vpl::img::CImage<vpl::img::tDensityPixel, VPL_IMAGE_DATA_ALLOCATOR>>;

%template(multiIterativeThresholding_Image8)vpl::img::multiIterativeThresholding<vpl::img::CImage<vpl::img::tPixel8, VPL_IMAGE_DATA_ALLOCATOR>>;
%template(multiIterativeThresholding_Image16)vpl::img::multiIterativeThresholding<vpl::img::CImage<vpl::img::tPixel16, VPL_IMAGE_DATA_ALLOCATOR>>;
%template(multiIterativeThresholding_Image32)vpl::img::multiIterativeThresholding<vpl::img::CImage<vpl::img::tPixel32, VPL_IMAGE_DATA_ALLOCATOR>>;
%template(multiIterativeThresholding_DImage)vpl::img::multiIterativeThresholding<vpl::img::CImage<vpl::img::tDensityPixel, VPL_IMAGE_DATA_ALLOCATOR>>;


%template(singleOtsuThresholding_Image8)vpl::img::singleOtsuThresholding<vpl::img::CImage<vpl::img::tPixel8, VPL_IMAGE_DATA_ALLOCATOR>>;
%template(singleOtsuThresholding_Image16)vpl::img::singleOtsuThresholding<vpl::img::CImage<vpl::img::tPixel16, VPL_IMAGE_DATA_ALLOCATOR>>;
%template(singleOtsuThresholding_Image32)vpl::img::singleOtsuThresholding<vpl::img::CImage<vpl::img::tPixel32, VPL_IMAGE_DATA_ALLOCATOR>>;
%template(singleOtsuThresholding_DImage)vpl::img::singleOtsuThresholding<vpl::img::CImage<vpl::img::tDensityPixel, VPL_IMAGE_DATA_ALLOCATOR>>;



