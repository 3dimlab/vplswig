//==============================================================================
// This file is part of
//
// VPLswig - SWIG-based VPL bindings for Python
// Changes are Copyright 2016 3Dim Laboratory s.r.o.
// All rights reserved.
//
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.
//==============================================================================

%{
#include <VPL\Image\Slice.h>

#include <VPL\ImageIO\DicomSlice.h>
#include <VPL\ImageIO\DicomSliceLoader.h>
#include <VPL\ImageIO\DicomSliceLoaderVPL.h>

#include <VPL\ImageIO\DicomDirLoader.h>
%}


%include "VPL\Image\Slice.h"
%include "VPL\ImageIO\DicomSlice.h"

%include "VPL\ImageIO\DicomSliceLoader.h"
%include "VPL\ImageIO\DicomSliceLoaderVPL.h"

%include "VPL\ImageIO\DicomDirLoader.h"

%template(sp_dicomSlice)vpl::base::CSharedPtr<vpl::img::CDicomSlice>;
