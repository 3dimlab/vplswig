//==============================================================================
// This file is part of
//
// VPLswig - SWIG-based VPL bindings for Python
// Changes are Copyright 2016 3Dim Laboratory s.r.o.
// All rights reserved.
//
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.
//==============================================================================
// Module description 


%{
#include <VPL/Image/ImageView.h>
#include <VPL/Image/ImageBase.h>
#include <VPL/Base/RefData.h>
#include <VPL/Image/Image.h>

#include <VPL/Image/VolumeView.h>
#include <VPL/Image/VolumeBase.h>
#include <VPL/Image/Volume.h>
#include <VPL/Image/DensityVolume.h>

#include <VPL\Image\Slice.h>
#include <VPL\ImageIO\DicomSlice.h>

#include <VPL/Image/HistogramBase.h>
#include <VPL/Image/Histogram.h>
#include <VPL/Base/TypeTraits.h>

%}
%import "VPL/Base/TypeTraits.h"
%include "VPL/Image/HistogramBase.h"
%include "VPL/Image/Histogram.h"




/************ Volume *********************/
%template(CHistogram_Volume8)vpl::img::CHistogram< vpl::img::CVolume<vpl::img::tPixel8, VPL_VOLUME_DATA_ALLOCATOR> >;
%template(CHistogram_Volume16)vpl::img::CHistogram< vpl::img::CVolume<vpl::img::tPixel16, VPL_VOLUME_DATA_ALLOCATOR> >;
%template(CHistogram_Volume32)vpl::img::CHistogram< vpl::img::CVolume<vpl::img::tPixel32, VPL_VOLUME_DATA_ALLOCATOR> >;
%template(CHistogram_FVolume)vpl::img::CHistogram< vpl::img::CVolume<vpl::img::tFloatPixel, VPL_VOLUME_DATA_ALLOCATOR> >;
%template(CHistogram_DVolume)vpl::img::CHistogram< vpl::img::CVolume<vpl::img::tDensityPixel, VPL_VOLUME_DATA_ALLOCATOR> >;
//%template(CHistogram_RGBVolume)vpl::img::CHistogram< vpl::img::CVolume<vpl::img::tRGBPixel, VPL_VOLUME_DATA_ALLOCATOR> >;
//%template(CHistogram_ComplexVolume)vpl::img::CHistogram< vpl::img::CVolume<vpl::img::tComplexPixel, VPL_VOLUME_DATA_ALLOCATOR> >;


//%template(CFullHistogram_Volume8)vpl::img::CFullHistogram< vpl::img::CVolume<vpl::img::tPixel8, VPL_VOLUME_DATA_ALLOCATOR> >;
//%template(CFullHistogram_Volume16)vpl::img::CFullHistogram< vpl::img::CVolume<vpl::img::tPixel16, VPL_VOLUME_DATA_ALLOCATOR> >;
//%template(CFullHistogram_Volume32)vpl::img::CFullHistogram< vpl::img::CVolume<vpl::img::tPixel32, VPL_VOLUME_DATA_ALLOCATOR> >;
////%template(CFullHistogram_FVolume)vpl::img::CFullHistogram< vpl::img::CVolume<vpl::img::tFloatPixel, VPL_VOLUME_DATA_ALLOCATOR> >;
//%template(CFullHistogram_DVolume)vpl::img::CFullHistogram< vpl::img::CVolume<vpl::img::tDensityPixel, VPL_VOLUME_DATA_ALLOCATOR> >;
////%template(CFullHistogram_RGBVolume)vpl::img::CFullHistogram< vpl::img::CVolume<vpl::img::tRGBPixel, VPL_VOLUME_DATA_ALLOCATOR> >;
////%template(CFullHistogram_ComplexVolume)vpl::img::CFullHistogram< vpl::img::CVolume<vpl::img::tComplexPixel, VPL_VOLUME_DATA_ALLOCATOR> >;
