//==============================================================================
// This file is part of
//
// VPLswig - SWIG-based VPL bindings for Python
// Changes are Copyright 2016 3Dim Laboratory s.r.o.
// All rights reserved.
//
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.
//==============================================================================

// Module description 
%module (package = "VPLSwig.Image") ImageFilters
%include "../Core/Common.i"
%import "Image.i"


%{
#include <VPL\Image\Slice.h>
#include <VPL\ImageIO\DicomSlice.h>
#include <VPL/Image/ImageFilter.h>
#include <VPL\Image\SeparableImageFilter.h>
%}

%include "VPL/Image/ImageFilter.h"
%include "VPL\Image\SeparableImageFilter.h"

%template(swig_imageFilter_Image8)vpl::img::CImageFilter<vpl::img::CImage<vpl::img::tPixel8>>;
%template(swig_imageFilter_Image16)vpl::img::CImageFilter<vpl::img::CImage<vpl::img::tPixel16>>;
%template(swig_imageFilter_Image32)vpl::img::CImageFilter<vpl::img::CImage<vpl::img::tPixel32>>;
%template(swig_imageFilter_FImage)vpl::img::CImageFilter<vpl::img::CImage<vpl::img::tFloatPixel>>;
%template(swig_imageFilter_DImage)vpl::img::CImageFilter<vpl::img::CImage<vpl::img::tDensityPixel>>;
%template(swig_imageFilter_RGBAImage)vpl::img::CImageFilter<vpl::img::CImage<vpl::img::tRGBAPixel>>;
%template(swig_imageFilter_ComplexImage)vpl::img::CImageFilter<vpl::img::CImage<vpl::img::tComplexPixel>>;

%template(swig_separableImageFilter_Image8)vpl::img::CSeparableImageFilter<vpl::img::CImage<vpl::img::tPixel8>>;
%template(swig_separableImageFilter_Image16)vpl::img::CSeparableImageFilter<vpl::img::CImage<vpl::img::tPixel16>>;
%template(swig_separableImageFilter_Image32)vpl::img::CSeparableImageFilter<vpl::img::CImage<vpl::img::tPixel32>>;
%template(swig_separableImageFilter_FImage)vpl::img::CSeparableImageFilter<vpl::img::CImage<vpl::img::tFloatPixel>>;
%template(swig_separableImageFilter_DImage)vpl::img::CSeparableImageFilter<vpl::img::CImage<vpl::img::tDensityPixel>>;
%template(swig_separableImageFilter_RGBAImage)vpl::img::CSeparableImageFilter<vpl::img::CImage<vpl::img::tRGBAPixel>>;
%template(swig_separableImageFilter_ComplexImage)vpl::img::CSeparableImageFilter<vpl::img::CImage<vpl::img::tComplexPixel>>;

%include "ImageFilters/AveragingFilter.i"
%include "ImageFilters/GaussianFilter.i"

%include "EdgeDetection/EdgeDetection.i"