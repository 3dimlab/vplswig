//==============================================================================
// This file is part of
//
// VPLswig - SWIG-based VPL bindings for Python
// Changes are Copyright 2016 im Laboratory s.r.o.
// All rights reserved.
//
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.
//==============================================================================

%{
#include <VPL/Image/VolumeFunctions/General.h>
%}

%include "VPL/Image/VolumeFunctions/General.h"


/*********************** Get Max *********************************/
%template(getMax_pixel8_Volume8)vpl::img::getMax<vpl::img::tPixel8,vpl::img::CVolume<vpl::img::tPixel8, VPL_VOLUME_DATA_ALLOCATOR>>;
%template(getMax_pixel16_Volume16)vpl::img::getMax<vpl::img::tPixel16, vpl::img::CVolume<vpl::img::tPixel16, VPL_VOLUME_DATA_ALLOCATOR>>;
%template(getMax_pixel32_Volume32)vpl::img::getMax<vpl::img::tPixel32, vpl::img::CVolume<vpl::img::tPixel32, VPL_VOLUME_DATA_ALLOCATOR>>;
%template(getMax_float_FVolume)vpl::img::getMax<vpl::img::tFloatPixel, vpl::img::CVolume<vpl::img::tFloatPixel, VPL_VOLUME_DATA_ALLOCATOR>>;
%template(getMax_Density_DVolume)vpl::img::getMax<vpl::img::tDensityPixel, vpl::img::CVolume<vpl::img::tDensityPixel, VPL_VOLUME_DATA_ALLOCATOR>>;
%template(getMaxRGB_RGBAVolume)vpl::img::getMax<vpl::img::tRGBPixel, vpl::img::CVolume<vpl::img::tRGBPixel, VPL_VOLUME_DATA_ALLOCATOR>>;
%template(getMax_Complex_ComplexVolume)vpl::img::getMax<vpl::img::tComplexPixel, vpl::img::CVolume<vpl::img::tComplexPixel, VPL_VOLUME_DATA_ALLOCATOR>>;

%template(getMax_double_Volume8)vpl::img::getMax<double, vpl::img::CVolume<vpl::img::tPixel8, VPL_VOLUME_DATA_ALLOCATOR>>;
%template(getMax_double_Volume16)vpl::img::getMax<double, vpl::img::CVolume<vpl::img::tPixel16, VPL_VOLUME_DATA_ALLOCATOR>>;
%template(getMax_double_Volume32)vpl::img::getMax<double, vpl::img::CVolume<vpl::img::tPixel32, VPL_VOLUME_DATA_ALLOCATOR>>;
%template(getMax_double_FVolume)vpl::img::getMax<double, vpl::img::CVolume<vpl::img::tFloatPixel, VPL_VOLUME_DATA_ALLOCATOR>>;
%template(getMax_double_DVolume)vpl::img::getMax<double, vpl::img::CVolume<vpl::img::tDensityPixel, VPL_VOLUME_DATA_ALLOCATOR>>;
//%template(getMax_double_RGBAVolume)vpl::img::getMax<double, vpl::img::CVolume<vpl::img::tRGBPixel, VPL_VOLUME_DATA_ALLOCATOR>>;
//%template(getMax_double_ComplexVolume)vpl::img::getMax<double, vpl::img::CVolume<vpl::img::tComplexPixel, VPL_VOLUME_DATA_ALLOCATOR>>;

/*********************** Get Min *********************************/
%template(getMin_pixel8_Volume8)vpl::img::getMin<vpl::img::tPixel8, vpl::img::CVolume<vpl::img::tPixel8, VPL_VOLUME_DATA_ALLOCATOR>>;
%template(getMin_pixel16_Volume16)vpl::img::getMin<vpl::img::tPixel16, vpl::img::CVolume<vpl::img::tPixel16, VPL_VOLUME_DATA_ALLOCATOR>>;
%template(getMin_pixel32_Volume32)vpl::img::getMin<vpl::img::tPixel32, vpl::img::CVolume<vpl::img::tPixel32, VPL_VOLUME_DATA_ALLOCATOR>>;
%template(getMin_float_FVolume)vpl::img::getMin<vpl::img::tFloatPixel, vpl::img::CVolume<vpl::img::tFloatPixel, VPL_VOLUME_DATA_ALLOCATOR>>;
%template(getMin_Density_DVolume)vpl::img::getMin<vpl::img::tDensityPixel, vpl::img::CVolume<vpl::img::tDensityPixel, VPL_VOLUME_DATA_ALLOCATOR>>;
%template(getMin_RGBA_RGBAVolume)vpl::img::getMin<vpl::img::tRGBPixel, vpl::img::CVolume<vpl::img::tRGBPixel, VPL_VOLUME_DATA_ALLOCATOR>>;
%template(getMin_Complex_ComplexVolume)vpl::img::getMin<vpl::img::tComplexPixel, vpl::img::CVolume<vpl::img::tComplexPixel, VPL_VOLUME_DATA_ALLOCATOR>>;


%template(getMin_double_Volume8)vpl::img::getMin<double,vpl::img::CVolume<vpl::img::tPixel8, VPL_VOLUME_DATA_ALLOCATOR>>;
%template(getMin_double_Volume16)vpl::img::getMin<double, vpl::img::CVolume<vpl::img::tPixel16, VPL_VOLUME_DATA_ALLOCATOR>>;
%template(getMin_double_Volume32)vpl::img::getMin<double, vpl::img::CVolume<vpl::img::tPixel32, VPL_VOLUME_DATA_ALLOCATOR>>;
%template(getMin_double_FVolume)vpl::img::getMin<double, vpl::img::CVolume<vpl::img::tFloatPixel, VPL_VOLUME_DATA_ALLOCATOR>>;
%template(getMin_double_DVolume)vpl::img::getMin<double, vpl::img::CVolume<vpl::img::tDensityPixel, VPL_VOLUME_DATA_ALLOCATOR>>;
//%template(getMin_double_RGBAVolume)vpl::img::getMin<double, vpl::img::CVolume<vpl::img::tRGBPixel, VPL_VOLUME_DATA_ALLOCATOR>>;
//%template(getMin_double_ComplexVolume)vpl::img::getMin<double, vpl::img::CVolume<vpl::img::tComplexPixel, VPL_VOLUME_DATA_ALLOCATOR>>;


/*********************** Get Variance *********************************/
%template(getVariance_pixel8_Volume8)vpl::img::getVariance<vpl::img::tPixel8, vpl::img::CVolume<vpl::img::tPixel8, VPL_VOLUME_DATA_ALLOCATOR>>;
%template(getVariance_pixel16_Volume16)vpl::img::getVariance<vpl::img::tPixel16, vpl::img::CVolume<vpl::img::tPixel16, VPL_VOLUME_DATA_ALLOCATOR>>;
%template(getVariance_pixel32_Volume32)vpl::img::getVariance<vpl::img::tPixel32, vpl::img::CVolume<vpl::img::tPixel32, VPL_VOLUME_DATA_ALLOCATOR>>;
%template(getVariance_float_FVolume)vpl::img::getVariance<vpl::img::tFloatPixel, vpl::img::CVolume<vpl::img::tFloatPixel, VPL_VOLUME_DATA_ALLOCATOR>>;
%template(getVariance_Density_DVolume)vpl::img::getVariance<vpl::img::tDensityPixel, vpl::img::CVolume<vpl::img::tDensityPixel, VPL_VOLUME_DATA_ALLOCATOR>>;
//%template(getVariance_RGBA_RGBAVolume)vpl::img::getVariance<vpl::img::tRGBPixel, vpl::img::CVolume<vpl::img::tRGBPixel, VPL_VOLUME_DATA_ALLOCATOR>>;
//%template(getVariance_Complex_ComplexVolume)vpl::img::getVariance<vpl::img::tComplexPixel, vpl::img::CVolume<vpl::img::tComplexPixel, VPL_VOLUME_DATA_ALLOCATOR>>;


%template(getVariance_double_Volume8)vpl::img::getVariance<double, vpl::img::CVolume<vpl::img::tPixel8, VPL_VOLUME_DATA_ALLOCATOR>>;
%template(getVariance_double_Volume16)vpl::img::getVariance<double, vpl::img::CVolume<vpl::img::tPixel16, VPL_VOLUME_DATA_ALLOCATOR>>;
%template(getVariance_double_Volume32)vpl::img::getVariance<double, vpl::img::CVolume<vpl::img::tPixel32, VPL_VOLUME_DATA_ALLOCATOR>>;
%template(getVariance_double_FVolume)vpl::img::getVariance<double, vpl::img::CVolume<vpl::img::tFloatPixel, VPL_VOLUME_DATA_ALLOCATOR>>;
%template(getVariance_double_DVolume)vpl::img::getVariance<double, vpl::img::CVolume<vpl::img::tDensityPixel, VPL_VOLUME_DATA_ALLOCATOR>>;
//%template(getVariance_double_RGBAVolume)vpl::img::getVariance<vpl::img::tRGBPixel, vpl::img::CVolume<vpl::img::tRGBPixel, VPL_VOLUME_DATA_ALLOCATOR>>;
//%template(getVariance_double_ComplexVolume)vpl::img::getVariance<vpl::img::tComplexPixel, vpl::img::CVolume<vpl::img::tComplexPixel, VPL_VOLUME_DATA_ALLOCATOR>>;

/*********************** Get Mean *********************************/
%template(getMean_pixel8_Volume8)vpl::img::getMean<vpl::img::tPixel8, vpl::img::CVolume<vpl::img::tPixel8, VPL_VOLUME_DATA_ALLOCATOR>>;
%template(getMean_pixel16_Volume16)vpl::img::getMean<vpl::img::tPixel16, vpl::img::CVolume<vpl::img::tPixel16, VPL_VOLUME_DATA_ALLOCATOR>>;
%template(getMean_pixel32_Volume32)vpl::img::getMean<vpl::img::tPixel32, vpl::img::CVolume<vpl::img::tPixel32, VPL_VOLUME_DATA_ALLOCATOR>>;
%template(getMean_float_FVolume)vpl::img::getMean<vpl::img::tFloatPixel, vpl::img::CVolume<vpl::img::tFloatPixel, VPL_VOLUME_DATA_ALLOCATOR>>;
%template(getMean_Density_DVolume)vpl::img::getMean<vpl::img::tDensityPixel, vpl::img::CVolume<vpl::img::tDensityPixel, VPL_VOLUME_DATA_ALLOCATOR>>;
%template(getMean_RGBA_RGBAVolume)vpl::img::getMean<vpl::img::tRGBPixel, vpl::img::CVolume<vpl::img::tRGBPixel, VPL_VOLUME_DATA_ALLOCATOR>>;
//%template(getMean_Complex_ComplexVolume)vpl::img::getMean<vpl::img::tComplexPixel, vpl::img::CVolume<vpl::img::tComplexPixel, VPL_VOLUME_DATA_ALLOCATOR>>;


%template(getMean_double_Volume8)vpl::img::getMean<double, vpl::img::CVolume<vpl::img::tPixel8, VPL_VOLUME_DATA_ALLOCATOR>>;
%template(getMean_double_Volume16)vpl::img::getMean<double, vpl::img::CVolume<vpl::img::tPixel16, VPL_VOLUME_DATA_ALLOCATOR>>;
%template(getMean_double_Volume32)vpl::img::getMean<double, vpl::img::CVolume<vpl::img::tPixel32, VPL_VOLUME_DATA_ALLOCATOR>>;
%template(getMean_double_FVolume)vpl::img::getMean<double, vpl::img::CVolume<vpl::img::tFloatPixel, VPL_VOLUME_DATA_ALLOCATOR>>;
%template(getMean_double_DVolume)vpl::img::getMean<double, vpl::img::CVolume<vpl::img::tDensityPixel, VPL_VOLUME_DATA_ALLOCATOR>>;
//%template(getMean_double_RGBAVolume)vpl::img::getMean<double, vpl::img::CVolume<vpl::img::tRGBPixel, VPL_VOLUME_DATA_ALLOCATOR>>;
//%template(getMean_double_ComplexVolume)vpl::img::getMean<double, vpl::img::CVolume<vpl::img::tComplexPixel, VPL_VOLUME_DATA_ALLOCATOR>>;

/*********************** Get Sum Of Squares *********************************/
%template(getSumOfSquares_pixel8_Volume8)vpl::img::getSumOfSquares<vpl::img::tPixel8, vpl::img::CVolume<vpl::img::tPixel8, VPL_VOLUME_DATA_ALLOCATOR>>;
%template(getSumOfSquares_pixel16_Volume16)vpl::img::getSumOfSquares<vpl::img::tPixel16, vpl::img::CVolume<vpl::img::tPixel16, VPL_VOLUME_DATA_ALLOCATOR>>;
%template(getSumOfSquares_pixel32_Volume32)vpl::img::getSumOfSquares<vpl::img::tPixel32, vpl::img::CVolume<vpl::img::tPixel32, VPL_VOLUME_DATA_ALLOCATOR>>;
%template(getSumOfSquares_float_FVolume)vpl::img::getSumOfSquares<vpl::img::tFloatPixel, vpl::img::CVolume<vpl::img::tFloatPixel, VPL_VOLUME_DATA_ALLOCATOR>>;
%template(getSumOfSquares_Density_DVolume)vpl::img::getSumOfSquares<vpl::img::tDensityPixel, vpl::img::CVolume<vpl::img::tDensityPixel, VPL_VOLUME_DATA_ALLOCATOR>>;
//%template(getSumOfSquares_RGBA_RGBAVolume)vpl::img::getSumOfSquares<vpl::img::tRGBPixel, vpl::img::CVolume<vpl::img::tRGBPixel, VPL_VOLUME_DATA_ALLOCATOR>>;
%template(getSumOfSquares_Complex_ComplexVolume)vpl::img::getSumOfSquares<vpl::img::tComplexPixel, vpl::img::CVolume<vpl::img::tComplexPixel, VPL_VOLUME_DATA_ALLOCATOR>>;


%template(getSumOfSquares_double_Volume8)vpl::img::getSumOfSquares<double, vpl::img::CVolume<vpl::img::tPixel8, VPL_VOLUME_DATA_ALLOCATOR>>;
%template(getSumOfSquares_double_Volume16)vpl::img::getSumOfSquares<double, vpl::img::CVolume<vpl::img::tPixel16, VPL_VOLUME_DATA_ALLOCATOR>>;
%template(getSumOfSquares_double_Volume32)vpl::img::getSumOfSquares<double, vpl::img::CVolume<vpl::img::tPixel32, VPL_VOLUME_DATA_ALLOCATOR>>;
%template(getSumOfSquares_double_FVolume)vpl::img::getSumOfSquares<double, vpl::img::CVolume<vpl::img::tFloatPixel, VPL_VOLUME_DATA_ALLOCATOR>>;
%template(getSumOfSquares_double_DVolume)vpl::img::getSumOfSquares<double, vpl::img::CVolume<vpl::img::tDensityPixel, VPL_VOLUME_DATA_ALLOCATOR>>;
//%template(getSumOfSquares_double_RGBAVolume)vpl::img::getSumOfSquares<vpl::img::tRGBPixel, vpl::img::CVolume<vpl::img::tRGBPixel, VPL_VOLUME_DATA_ALLOCATOR>>;
//%template(getSumOfSquares_double_ComplexVolume)vpl::img::getSumOfSquares<vpl::img::tComplexPixel, vpl::img::CVolume<vpl::img::tComplexPixel, VPL_VOLUME_DATA_ALLOCATOR>>;

/*********************** Get Sum *********************************/
%template(getSum_pixel8_Volume8)vpl::img::getSum<vpl::img::tPixel8, vpl::img::CVolume<vpl::img::tPixel8, VPL_VOLUME_DATA_ALLOCATOR>>;
%template(getSum_pixel16_Volume16)vpl::img::getSum<vpl::img::tPixel16, vpl::img::CVolume<vpl::img::tPixel16, VPL_VOLUME_DATA_ALLOCATOR>>;
%template(getSum_pixel32_Volume32)vpl::img::getSum<vpl::img::tPixel32, vpl::img::CVolume<vpl::img::tPixel32, VPL_VOLUME_DATA_ALLOCATOR>>;
%template(getSum_float_FVolume)vpl::img::getSum<vpl::img::tFloatPixel, vpl::img::CVolume<vpl::img::tFloatPixel, VPL_VOLUME_DATA_ALLOCATOR>>;
%template(getSum_Density_DVolume)vpl::img::getSum<vpl::img::tDensityPixel, vpl::img::CVolume<vpl::img::tDensityPixel, VPL_VOLUME_DATA_ALLOCATOR>>;
%template(getSum_RGBA_RGBAVolume)vpl::img::getSum<vpl::img::tRGBPixel, vpl::img::CVolume<vpl::img::tRGBPixel, VPL_VOLUME_DATA_ALLOCATOR>>;
%template(getSum_Complex_ComplexVolume)vpl::img::getSum<vpl::img::tComplexPixel, vpl::img::CVolume<vpl::img::tComplexPixel, VPL_VOLUME_DATA_ALLOCATOR>>;


%template(getSum_double_Volume8)vpl::img::getSum<double, vpl::img::CVolume<vpl::img::tPixel8, VPL_VOLUME_DATA_ALLOCATOR>>;
%template(getSum_double_Volume16)vpl::img::getSum<double, vpl::img::CVolume<vpl::img::tPixel16, VPL_VOLUME_DATA_ALLOCATOR>>;
%template(getSum_double_Volume32)vpl::img::getSum<double, vpl::img::CVolume<vpl::img::tPixel32, VPL_VOLUME_DATA_ALLOCATOR>>;
%template(getSum_double_FVolume)vpl::img::getSum<double, vpl::img::CVolume<vpl::img::tFloatPixel, VPL_VOLUME_DATA_ALLOCATOR>>;
%template(getSum_double_DVolume)vpl::img::getSum<double, vpl::img::CVolume<vpl::img::tDensityPixel, VPL_VOLUME_DATA_ALLOCATOR>>;
//%template(getSum_double_RGBAVolume)vpl::img::getSum<double, vpl::img::CVolume<vpl::img::tRGBPixel, VPL_VOLUME_DATA_ALLOCATOR>>;
//%template(getSum_double_ComplexVolume)vpl::img::getSum<double, vpl::img::CVolume<vpl::img::tComplexPixel, VPL_VOLUME_DATA_ALLOCATOR>>;



/*********************** Get Mean Square Error *********************************/
%template(getMeanSquareError_pixel8_Volume8)vpl::img::getMeanSquareError<vpl::img::tPixel8, vpl::img::CVolume<vpl::img::tPixel8, VPL_VOLUME_DATA_ALLOCATOR>, vpl::img::CVolume<vpl::img::tPixel8, VPL_VOLUME_DATA_ALLOCATOR>>;
%template(getMeanSquareError_pixel16_Volume16)vpl::img::getMeanSquareError<vpl::img::tPixel16, vpl::img::CVolume<vpl::img::tPixel16, VPL_VOLUME_DATA_ALLOCATOR>, vpl::img::CVolume<vpl::img::tPixel16, VPL_VOLUME_DATA_ALLOCATOR>>;
%template(getMeanSquareError_pixel32_Volume32)vpl::img::getMeanSquareError<vpl::img::tPixel32, vpl::img::CVolume<vpl::img::tPixel32, VPL_VOLUME_DATA_ALLOCATOR>, vpl::img::CVolume<vpl::img::tPixel32, VPL_VOLUME_DATA_ALLOCATOR>>;
%template(getMeanSquareError_float_FVolume)vpl::img::getMeanSquareError<vpl::img::tFloatPixel, vpl::img::CVolume<vpl::img::tFloatPixel, VPL_VOLUME_DATA_ALLOCATOR>, vpl::img::CVolume<vpl::img::tFloatPixel, VPL_VOLUME_DATA_ALLOCATOR>>;
%template(getMeanSquareError_Density_DVolume)vpl::img::getMeanSquareError<vpl::img::tDensityPixel, vpl::img::CVolume<vpl::img::tDensityPixel, VPL_VOLUME_DATA_ALLOCATOR>, vpl::img::CVolume<vpl::img::tDensityPixel, VPL_VOLUME_DATA_ALLOCATOR>>;
%template(getMeanSquareError_RGBA_RGBAVolume)vpl::img::getMeanSquareError<vpl::img::tRGBPixel, vpl::img::CVolume<vpl::img::tRGBPixel, VPL_VOLUME_DATA_ALLOCATOR>, vpl::img::CVolume<vpl::img::tRGBPixel, VPL_VOLUME_DATA_ALLOCATOR>>;
%template(getMeanSquareError_Complex_ComplexVolume)vpl::img::getMeanSquareError<vpl::img::tComplexPixel, vpl::img::CVolume<vpl::img::tComplexPixel, VPL_VOLUME_DATA_ALLOCATOR>, vpl::img::CVolume<vpl::img::tComplexPixel, VPL_VOLUME_DATA_ALLOCATOR>>;


%template(getMeanSquareError_double_Volume8)vpl::img::getMeanSquareError<double, vpl::img::CVolume<vpl::img::tPixel8, VPL_VOLUME_DATA_ALLOCATOR>, vpl::img::CVolume<vpl::img::tPixel8, VPL_VOLUME_DATA_ALLOCATOR>>;
%template(getMeanSquareError_double_Volume16)vpl::img::getMeanSquareError<double, vpl::img::CVolume<vpl::img::tPixel16, VPL_VOLUME_DATA_ALLOCATOR>, vpl::img::CVolume<vpl::img::tPixel16, VPL_VOLUME_DATA_ALLOCATOR>>;
%template(getMeanSquareError_double_Volume32)vpl::img::getMeanSquareError<double, vpl::img::CVolume<vpl::img::tPixel32, VPL_VOLUME_DATA_ALLOCATOR>, vpl::img::CVolume<vpl::img::tPixel32, VPL_VOLUME_DATA_ALLOCATOR>>;
%template(getMeanSquareError_double_FVolume)vpl::img::getMeanSquareError<double, vpl::img::CVolume<vpl::img::tFloatPixel, VPL_VOLUME_DATA_ALLOCATOR>, vpl::img::CVolume<vpl::img::tFloatPixel, VPL_VOLUME_DATA_ALLOCATOR>>;
%template(getMeanSquareError_double_DVolume)vpl::img::getMeanSquareError<double, vpl::img::CVolume<vpl::img::tDensityPixel, VPL_VOLUME_DATA_ALLOCATOR>, vpl::img::CVolume<vpl::img::tDensityPixel, VPL_VOLUME_DATA_ALLOCATOR>>;
//%template(getMeanSquareError_double_RGBAVolume)vpl::img::getMeanSquareError<double, vpl::img::CVolume<vpl::img::tRGBPixel, VPL_VOLUME_DATA_ALLOCATOR>, vpl::img::CVolume<vpl::img::tRGBPixel, VPL_VOLUME_DATA_ALLOCATOR>>;
//%template(getMeanSquareError_double_ComplexVolume)vpl::img::getMeanSquareError<double, vpl::img::CVolume<vpl::img::tComplexPixel, VPL_VOLUME_DATA_ALLOCATOR>, vpl::img::CVolume<vpl::img::tComplexPixel, VPL_VOLUME_DATA_ALLOCATOR>>;


/*********************** Get Product *********************************/
%template(getProduct_pixel8_Volume8)vpl::img::getProduct<vpl::img::tPixel8, vpl::img::CVolume<vpl::img::tPixel8, VPL_VOLUME_DATA_ALLOCATOR>, vpl::img::CVolume<vpl::img::tPixel8, VPL_VOLUME_DATA_ALLOCATOR>>;
%template(getProduct_pixel16_Volume16)vpl::img::getProduct<vpl::img::tPixel16, vpl::img::CVolume<vpl::img::tPixel16, VPL_VOLUME_DATA_ALLOCATOR>, vpl::img::CVolume<vpl::img::tPixel16, VPL_VOLUME_DATA_ALLOCATOR>>;
%template(getProduct_pixel32_Volume32)vpl::img::getProduct<vpl::img::tPixel32, vpl::img::CVolume<vpl::img::tPixel32, VPL_VOLUME_DATA_ALLOCATOR>, vpl::img::CVolume<vpl::img::tPixel32, VPL_VOLUME_DATA_ALLOCATOR>>;
%template(getProduct_float_FVolume)vpl::img::getProduct<vpl::img::tFloatPixel, vpl::img::CVolume<vpl::img::tFloatPixel, VPL_VOLUME_DATA_ALLOCATOR>, vpl::img::CVolume<vpl::img::tFloatPixel, VPL_VOLUME_DATA_ALLOCATOR>>;
%template(getProduct_Density_DVolume)vpl::img::getProduct<vpl::img::tDensityPixel, vpl::img::CVolume<vpl::img::tDensityPixel, VPL_VOLUME_DATA_ALLOCATOR>, vpl::img::CVolume<vpl::img::tDensityPixel, VPL_VOLUME_DATA_ALLOCATOR>>;
%template(getProduct_RGBA_RGBAVolume)vpl::img::getProduct<vpl::img::tRGBPixel, vpl::img::CVolume<vpl::img::tRGBPixel, VPL_VOLUME_DATA_ALLOCATOR>, vpl::img::CVolume<vpl::img::tRGBPixel, VPL_VOLUME_DATA_ALLOCATOR>>;
%template(getProduct_Complex_ComplexVolume)vpl::img::getProduct<vpl::img::tComplexPixel, vpl::img::CVolume<vpl::img::tComplexPixel, VPL_VOLUME_DATA_ALLOCATOR>, vpl::img::CVolume<vpl::img::tComplexPixel, VPL_VOLUME_DATA_ALLOCATOR>>;


%template(getProduct_double_Volume8)vpl::img::getProduct<double, vpl::img::CVolume<vpl::img::tPixel8, VPL_VOLUME_DATA_ALLOCATOR>, vpl::img::CVolume<vpl::img::tPixel8, VPL_VOLUME_DATA_ALLOCATOR>>;
%template(getProduct_double_Volume16)vpl::img::getProduct<double, vpl::img::CVolume<vpl::img::tPixel16, VPL_VOLUME_DATA_ALLOCATOR>, vpl::img::CVolume<vpl::img::tPixel16, VPL_VOLUME_DATA_ALLOCATOR>>;
%template(getProduct_double_Volume32)vpl::img::getProduct<double, vpl::img::CVolume<vpl::img::tPixel32, VPL_VOLUME_DATA_ALLOCATOR>, vpl::img::CVolume<vpl::img::tPixel32, VPL_VOLUME_DATA_ALLOCATOR>>;
%template(getProduct_double_FVolume)vpl::img::getProduct<double, vpl::img::CVolume<vpl::img::tFloatPixel, VPL_VOLUME_DATA_ALLOCATOR>, vpl::img::CVolume<vpl::img::tFloatPixel, VPL_VOLUME_DATA_ALLOCATOR>>;
%template(getProduct_double_DVolume)vpl::img::getProduct<double, vpl::img::CVolume<vpl::img::tDensityPixel, VPL_VOLUME_DATA_ALLOCATOR>, vpl::img::CVolume<vpl::img::tDensityPixel, VPL_VOLUME_DATA_ALLOCATOR>>;
//%template(getProduct_double_RGBAVolume)vpl::img::getProduct<double, vpl::img::CVolume<vpl::img::tRGBPixel, VPL_VOLUME_DATA_ALLOCATOR>, vpl::img::CVolume<vpl::img::tRGBPixel, VPL_VOLUME_DATA_ALLOCATOR>>;
//%template(getProduct_double_ComplexVolume)vpl::img::getProduct<double, vpl::img::CVolume<vpl::img::tComplexPixel, VPL_VOLUME_DATA_ALLOCATOR>, vpl::img::CVolume<vpl::img::tComplexPixel, VPL_VOLUME_DATA_ALLOCATOR>>;