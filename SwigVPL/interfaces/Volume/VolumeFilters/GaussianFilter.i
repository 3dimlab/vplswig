//==============================================================================
// This file is part of
//
// VPLswig - SWIG-based VPL bindings for Python
// Changes are Copyright 2016 3Dim Laboratory s.r.o.
// All rights reserved.
//
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.
//==============================================================================

%{

#include <VPL/Image/VolumeFilters/Gaussian.h>
%}
%ignore vpl::img::gaussian;


%include "VPL/Image/VolumeFilters/Gaussian.h"
%template(CVolumeGaussFilter_Volume8)vpl::img::CVolumeGaussFilter<vpl::img::CVolume<vpl::img::tPixel8,VPL_VOLUME_DATA_ALLOCATOR>>;
%template(CVolumeGaussFilter_Volume16)vpl::img::CVolumeGaussFilter<vpl::img::CVolume<vpl::img::tPixel16,VPL_VOLUME_DATA_ALLOCATOR>>;
%template(CVolumeGaussFilter_Volume32)vpl::img::CVolumeGaussFilter<vpl::img::CVolume<vpl::img::tPixel32,VPL_VOLUME_DATA_ALLOCATOR>>;
%template(CVolumeGaussFilter_FVolume)vpl::img::CVolumeGaussFilter<vpl::img::CVolume<vpl::img::tFloatPixel,VPL_VOLUME_DATA_ALLOCATOR>>;
%template(CVolumeGaussFilter_DVolume)vpl::img::CVolumeGaussFilter<vpl::img::CVolume<vpl::img::tDensityPixel,VPL_VOLUME_DATA_ALLOCATOR>>;
//%template(CVolumeGaussFilter_RGBAVolume)vpl::img::CVolumeGaussFilter<vpl::img::CVolume<vpl::img::tRGBAPixel,VPL_VOLUME_DATA_ALLOCATOR>>;
//%template(CVolumeGaussFilter_ComplexVolume)vpl::img::CVolumeGaussFilter<vpl::img::CVolume<vpl::img::tComplexPixel,VPL_VOLUME_DATA_ALLOCATOR>>;