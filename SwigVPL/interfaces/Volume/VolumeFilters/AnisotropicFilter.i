//==============================================================================
// This file is part of
//
// VPLswig - SWIG-based VPL bindings for Python
// Changes are Copyright 2016 3Dim Laboratory s.r.o.
// All rights reserved.
//
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.
//==============================================================================

%{

#include <VPL/Image/VolumeFilters/Anisotropic.h>
%}

%include "VPL/Image/VolumeFilters/Anisotropic.h"
%template(CVolumeAnisotropicFilter_Volume8)vpl::img::CVolumeAnisotropicFilter<vpl::img::CVolume<vpl::img::tPixel8,VPL_VOLUME_DATA_ALLOCATOR>>;
%template(CVolumeAnisotropicFilter_Volume16)vpl::img::CVolumeAnisotropicFilter<vpl::img::CVolume<vpl::img::tPixel16,VPL_VOLUME_DATA_ALLOCATOR>>;
%template(CVolumeAnisotropicFilter_Volume32)vpl::img::CVolumeAnisotropicFilter<vpl::img::CVolume<vpl::img::tPixel32,VPL_VOLUME_DATA_ALLOCATOR>>;
%template(CVolumeAnisotropicFilter_FVolume)vpl::img::CVolumeAnisotropicFilter<vpl::img::CVolume<vpl::img::tFloatPixel,VPL_VOLUME_DATA_ALLOCATOR>>;
%template(CVolumeAnisotropicFilter_DVolume)vpl::img::CVolumeAnisotropicFilter<vpl::img::CVolume<vpl::img::tDensityPixel,VPL_VOLUME_DATA_ALLOCATOR>>;
//%template(CVolumeAnisotropicFilter_RGBAVolume)vpl::img::CVolumeAnisotropicFilter<vpl::img::CVolume<vpl::img::tRGBAPixel,VPL_VOLUME_DATA_ALLOCATOR>>;
//%template(CVolumeAnisotropicFilter_ComplexVolume)vpl::img::CVolumeAnisotropicFilter<vpl::img::CVolume<vpl::img::tComplexPixel,VPL_VOLUME_DATA_ALLOCATOR>>;