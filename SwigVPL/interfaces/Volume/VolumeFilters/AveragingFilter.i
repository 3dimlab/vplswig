//==============================================================================
// This file is part of
//
// VPLswig - SWIG-based VPL bindings for Python
// Changes are Copyright 2016 3Dim Laboratory s.r.o.
// All rights reserved.
//
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.
//==============================================================================

%{

#include <VPL/Image/VolumeFilters/Averaging.h>
%}
%ignore vpl::img::CVolumeAvg3Filter::KERNEL;
%ignore vpl::img::CVolumeAvg3Filter::DENOM;
%ignore vpl::img::CVolumeAvg3Filter::~CVolumeAvg3Filter();


%include "VPL/Image/VolumeFilters/Averaging.h"
%template(CVolumeAvg3Filter_Volume8)vpl::img::CVolumeAvg3Filter<vpl::img::CVolume<vpl::img::tPixel8, VPL_VOLUME_DATA_ALLOCATOR>>;
%template(CVolumeAvg3Filter_Volume16)vpl::img::CVolumeAvg3Filter<vpl::img::CVolume<vpl::img::tPixel16, VPL_VOLUME_DATA_ALLOCATOR>>;
%template(CVolumeAvg3Filter_Volume32)vpl::img::CVolumeAvg3Filter<vpl::img::CVolume<vpl::img::tPixel32, VPL_VOLUME_DATA_ALLOCATOR>>;
%template(CVolumeAvg3Filter_FVolume)vpl::img::CVolumeAvg3Filter<vpl::img::CVolume<vpl::img::tFloatPixel, VPL_VOLUME_DATA_ALLOCATOR>>;
%template(CVolumeAvg3Filter_DVolume)vpl::img::CVolumeAvg3Filter<vpl::img::CVolume<vpl::img::tDensityPixel, VPL_VOLUME_DATA_ALLOCATOR>>;
//%template(CVolumeAvg3Filter_RGBAVolume)vpl::img::CVolumeAvg3Filter<vpl::img::CVolume<vpl::img::tRGBAPixel>>;
