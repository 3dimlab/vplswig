//==============================================================================
// This file is part of
//
// VPLswig - SWIG-based VPL bindings for Python
// Changes are Copyright 2016 3Dim Laboratory s.r.o.
// All rights reserved.
//
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.
//==============================================================================
// Module description 
%module (package = "VPLSwig.Volume") VolumeFilters
%include "../Core/Common.i"
%import "Volume.i"
%include "../Image/Image.i"

%{
#include <VPL/Image/VolumeFilter.h>
#include "VPL\Image\SeparableVolumeFilter.h"
#include <VPL\Image\Slice.h>
#include <VPL\ImageIO\DicomSlice.h>
#include <VPL\Image\DensityVolume.h>
%}


%include "VPL/Image/VolumeFilter.h"
%include "VPL\Image\SeparableVolumeFilter.h"

%template(swig_volumeFilter_Volume8)vpl::img::CVolumeFilter<vpl::img::CVolume<vpl::img::tPixel8, VPL_VOLUME_DATA_ALLOCATOR>>;
%template(swig_volumeFilter_Volume16)vpl::img::CVolumeFilter<vpl::img::CVolume<vpl::img::tPixel16, VPL_VOLUME_DATA_ALLOCATOR>>;
%template(swig_volumeFilter_Volume32)vpl::img::CVolumeFilter<vpl::img::CVolume<vpl::img::tPixel32, VPL_VOLUME_DATA_ALLOCATOR>>;
%template(swig_volumeFilter_FVolume)vpl::img::CVolumeFilter<vpl::img::CVolume<vpl::img::tFloatPixel, VPL_VOLUME_DATA_ALLOCATOR>>;
%template(swig_volumeFilter_DensityVolume)vpl::img::CVolumeFilter<vpl::img::CVolume<vpl::img::tDensityPixel, VPL_VOLUME_DATA_ALLOCATOR>>;
%template(swig_volumeFilter_RGBAVolume)vpl::img::CVolumeFilter<vpl::img::CVolume<vpl::img::tRGBAPixel, VPL_VOLUME_DATA_ALLOCATOR>>;
              
%template(swig_separableVolumeFilterCVolume8)vpl::img::CSeparableVolumeFilter<vpl::img::CVolume<vpl::img::tPixel8, VPL_VOLUME_DATA_ALLOCATOR>>;
%template(swig_separableVolumeFilterCVolume16)vpl::img::CSeparableVolumeFilter<vpl::img::CVolume<vpl::img::tPixel16, VPL_VOLUME_DATA_ALLOCATOR>>;
%template(swig_separableVolumeFilterCVolume32)vpl::img::CSeparableVolumeFilter<vpl::img::CVolume<vpl::img::tPixel32, VPL_VOLUME_DATA_ALLOCATOR>>;
%template(swig_separableVolumeFilterCFVolume)vpl::img::CSeparableVolumeFilter<vpl::img::CVolume<vpl::img::tFloatPixel, VPL_VOLUME_DATA_ALLOCATOR>>;
%template(swig_separableVolumeFilterCDVolume)vpl::img::CSeparableVolumeFilter<vpl::img::CVolume<vpl::img::tDensityPixel, VPL_VOLUME_DATA_ALLOCATOR>>;
%template(swig_separableVolumeFilterCRGBAVolume)vpl::img::CSeparableVolumeFilter<vpl::img::CVolume<vpl::img::tRGBAPixel, VPL_VOLUME_DATA_ALLOCATOR>>;

%include "VolumeFilters/AveragingFilter.i"
%include "VolumeFilters/GaussianFilter.i"
%include "VolumeFilters/AnisotropicFilter.i"


%include "VolumeEdgeDetection/VolumeEdgeDetection.i"