//==============================================================================
// This file is part of
//
// VPLswig - SWIG-based VPL bindings for Python
// Changes are Copyright 2016 3Dim Laboratory s.r.o.
// All rights reserved.
//
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.
//==============================================================================
// Module description 
%module (package = "VPLSwig") vplHistogram
%import "vplCore.i"
%import "Image.i"
%import "Volume.i"

#define WINDOWS

/***** Allow debug build even if no python_d.lib founded. ****/
%begin
%{
#ifdef _MSC_VER
#define SWIG_PYTHON_INTERPRETER_NO_DEBUG
#endif
%}


    // Generate docstring in output 
%feature("autodoc", "3");

// Disable some unused warnings
#pragma SWIG nowarn=362,312,325,350/*,325,312,401/*503*/
#pragma warning(suppress: 4244)


//System define
#ifdef WINDOWS

%{
#define NOMINMAX
#include <Windows.h>
%}
%include "Windows.i"

#define _WIN32
#endif

#undef VPL_FFTW_ENABLED
#define VPL_VOLUME_DATA_ALLOCATION_IN_BLOCKS


%{
#include <VPL/Image/ImageView.h>
#include <VPL/Image/ImageBase.h>
#include <VPL/Base/RefData.h>
#include <VPL/Image/Image.h>

#include <VPL/Image/VolumeView.h>
#include <VPL/Image/VolumeBase.h>
#include <VPL/Image/Volume.h>
#include <VPL/Image/DensityVolume.h>

#include <VPL\Image\Slice.h>
#include <VPL\ImageIO\DicomSlice.h>

#include <VPL/Image/HistogramBase.h>
#include <VPL/Image/Histogram.h>
#include <VPL/Base/TypeTraits.h>

%}
%import "VPL/Base/TypeTraits.h"
%include "VPL/Image/HistogramBase.h"
%include "VPL/Image/Histogram.h"

%template(CHistogram_Image8) vpl::img::CHistogram< vpl::img::CImage<vpl::img::tPixel8,  VPL_IMAGE_DATA_ALLOCATOR> >;
%template(CHistogram_Image16) vpl::img::CHistogram< vpl::img::CImage<vpl::img::tPixel16,  VPL_IMAGE_DATA_ALLOCATOR> >;
%template(CHistogram_Image32) vpl::img::CHistogram< vpl::img::CImage<vpl::img::tPixel32, VPL_IMAGE_DATA_ALLOCATOR> >;
%template(CHistogram_FImage) vpl::img::CHistogram< vpl::img::CImage<vpl::img::tFloatPixel, VPL_IMAGE_DATA_ALLOCATOR> >;
%template(CHistogram_DImage) vpl::img::CHistogram< vpl::img::CImage<vpl::img::tDensityPixel, VPL_IMAGE_DATA_ALLOCATOR> >;
//%template(CHistogram_RGBImage) vpl::img::CHistogram< vpl::img::CImage<vpl::img::tRGBPixel, VPL_IMAGE_DATA_ALLOCATOR> >;
//%template(CHistogram_ComplexImage) vpl::img::CHistogram< vpl::img::CImage<vpl::img::tComplexPixel, VPL_IMAGE_DATA_ALLOCATOR> >;


%template(CFullHistogram_Image8)vpl::img::CFullHistogram< vpl::img::CImage<vpl::img::tPixel8, VPL_IMAGE_DATA_ALLOCATOR> >;
%template(CFullHistogram_Image16)vpl::img::CFullHistogram< vpl::img::CImage<vpl::img::tPixel16, VPL_IMAGE_DATA_ALLOCATOR> >;
%template(CFullHistogram_Image32)vpl::img::CFullHistogram< vpl::img::CImage<vpl::img::tPixel32, VPL_IMAGE_DATA_ALLOCATOR> >;
//%template(CFullHistogram_FImage)vpl::img::CFullHistogram< vpl::img::CImage<vpl::img::tFloatPixel, VPL_IMAGE_DATA_ALLOCATOR> >;
%template(CFullHistogram_DImage)vpl::img::CFullHistogram< vpl::img::CImage<vpl::img::tDensityPixel, VPL_IMAGE_DATA_ALLOCATOR> >;
//%template(CFullHistogram_RGBImage) vpl::img::CFullHistogram< vpl::img::CImage<vpl::img::tRGBPixel, VPL_IMAGE_DATA_ALLOCATOR> >;
//%template(CFullHistogram_ComplexImage) vpl::img::CFullHistogram< vpl::img::CImage<vpl::img::tComplexPixel, VPL_IMAGE_DATA_ALLOCATOR> >;


/************ Volume *********************/
%template(CHistogram_Volume8)vpl::img::CHistogram< vpl::img::CVolume<vpl::img::tPixel8, VPL_VOLUME_DATA_ALLOCATOR> >;
%template(CHistogram_Volume16)vpl::img::CHistogram< vpl::img::CVolume<vpl::img::tPixel16, VPL_VOLUME_DATA_ALLOCATOR> >;
%template(CHistogram_Volume32)vpl::img::CHistogram< vpl::img::CVolume<vpl::img::tPixel32, VPL_VOLUME_DATA_ALLOCATOR> >;
%template(CHistogram_FVolume)vpl::img::CHistogram< vpl::img::CVolume<vpl::img::tFloatPixel, VPL_VOLUME_DATA_ALLOCATOR> >;
%template(CHistogram_DVolume)vpl::img::CHistogram< vpl::img::CVolume<vpl::img::tDensityPixel, VPL_VOLUME_DATA_ALLOCATOR> >;
//%template(CHistogram_RGBVolume)vpl::img::CHistogram< vpl::img::CVolume<vpl::img::tRGBPixel, VPL_VOLUME_DATA_ALLOCATOR> >;
//%template(CHistogram_ComplexVolume)vpl::img::CHistogram< vpl::img::CVolume<vpl::img::tComplexPixel, VPL_VOLUME_DATA_ALLOCATOR> >;


//%template(CFullHistogram_Volume8)vpl::img::CFullHistogram< vpl::img::CVolume<vpl::img::tPixel8, VPL_VOLUME_DATA_ALLOCATOR> >;
//%template(CFullHistogram_Volume16)vpl::img::CFullHistogram< vpl::img::CVolume<vpl::img::tPixel16, VPL_VOLUME_DATA_ALLOCATOR> >;
//%template(CFullHistogram_Volume32)vpl::img::CFullHistogram< vpl::img::CVolume<vpl::img::tPixel32, VPL_VOLUME_DATA_ALLOCATOR> >;
////%template(CFullHistogram_FVolume)vpl::img::CFullHistogram< vpl::img::CVolume<vpl::img::tFloatPixel, VPL_VOLUME_DATA_ALLOCATOR> >;
//%template(CFullHistogram_DVolume)vpl::img::CFullHistogram< vpl::img::CVolume<vpl::img::tDensityPixel, VPL_VOLUME_DATA_ALLOCATOR> >;
////%template(CFullHistogram_RGBVolume)vpl::img::CFullHistogram< vpl::img::CVolume<vpl::img::tRGBPixel, VPL_VOLUME_DATA_ALLOCATOR> >;
////%template(CFullHistogram_ComplexVolume)vpl::img::CFullHistogram< vpl::img::CVolume<vpl::img::tComplexPixel, VPL_VOLUME_DATA_ALLOCATOR> >;
