//==============================================================================
// This file is part of
//
// VPLswig - SWIG-based VPL bindings for Python
// Changes are Copyright 2016 3Dim Laboratory s.r.o.
// All rights reserved.
//
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.
//==============================================================================

// Module description 
%module (package = "VPLSwig.Volume") vplVolumeEdgeDetection
%import "Volume.i"

#define WINDOWS

/***** Allow debug build even if no python_d.lib founded. ****/
%begin
%{
#ifdef _MSC_VER
#define SWIG_PYTHON_INTERPRETER_NO_DEBUG
#endif
%}


    // Generate docstring in output 
%feature("autodoc", "3");

// Disable some unused warnings
#pragma SWIG nowarn=362,312,325,350/*,325,312,401/*503*/
#pragma warning(suppress: 4244)


//System define
#ifdef WINDOWS

%{
#define NOMINMAX
#include <Windows.h>
%}
%include "Windows.i"

#define _WIN32
#endif

#undef VPL_FFTW_ENABLED
#define VPL_VOLUME_DATA_ALLOCATION_IN_BLOCKS

%{
#include <VPL/Image/VolumeEdgeDetector.h>
#include <VPL\Image\Slice.h>
#include <VPL\ImageIO\DicomSlice.h>
#include <VPL\Image\DensityVolume.h>
%}



%include "VPL/Image/VolumeEdgeDetector.h"

%template(swig_baseEdgeDetector_Volume8)vpl::img::CVolumeEdgeDetector<vpl::img::CVolume<vpl::img::tPixel8, VPL_VOLUME_DATA_ALLOCATOR>>;
%template(swig_baseEdgeDetector_Volume16)vpl::img::CVolumeEdgeDetector<vpl::img::CVolume<vpl::img::tPixel16, VPL_VOLUME_DATA_ALLOCATOR>>;
%template(swig_baseEdgeDetector_Volume32)vpl::img::CVolumeEdgeDetector<vpl::img::CVolume<vpl::img::tPixel32, VPL_VOLUME_DATA_ALLOCATOR>>;
%template(swig_baseEdgeDetector_FVolume)vpl::img::CVolumeEdgeDetector<vpl::img::CVolume<vpl::img::tFloatPixel, VPL_VOLUME_DATA_ALLOCATOR>>;
%template(swig_baseEdgeDetector_DVolume)vpl::img::CVolumeEdgeDetector<vpl::img::CVolume<vpl::img::tDensityPixel, VPL_VOLUME_DATA_ALLOCATOR>>;

%include "Canny.i"