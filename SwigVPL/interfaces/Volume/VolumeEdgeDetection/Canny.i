//==============================================================================
// This file is part of
//
// VPLswig - SWIG-based VPL bindings for Python
// Changes are Copyright 2016 3Dim Laboratory s.r.o.
// All rights reserved.
//
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.
//==============================================================================

%{
#include <VPL/Image/VolumeEdgeDetection/Canny.h>
%}
%include "VPL/Image/VolumeEdgeDetection/Canny.h"

%template(CCanny_Volume8)vpl::img::CVolumeCanny<vpl::img::CVolume<vpl::img::tPixel8, VPL_VOLUME_DATA_ALLOCATOR>>;
%template(CCanny_Volume16)vpl::img::CVolumeCanny<vpl::img::CVolume<vpl::img::tPixel16, VPL_VOLUME_DATA_ALLOCATOR>>;
%template(CCanny_Volume32)vpl::img::CVolumeCanny<vpl::img::CVolume<vpl::img::tPixel32, VPL_VOLUME_DATA_ALLOCATOR>>;
%template(CCanny_FVolume)vpl::img::CVolumeCanny<vpl::img::CVolume<vpl::img::tFloatPixel, VPL_VOLUME_DATA_ALLOCATOR>>;
%template(CCanny_DVolume)vpl::img::CVolumeCanny<vpl::img::CVolume<vpl::img::tDensityPixel, VPL_VOLUME_DATA_ALLOCATOR>>;
//%template(CCanny_RGBAVolume)vpl::img::CVolumeCanny<vpl::img::CVolume<vpl::img::tRGBPixel, VPL_VOLUME_DATA_ALLOCATOR>>;
//%template(CCanny_ComplexVolume)vpl::img::CVolumeCanny<vpl::img::CVolume<vpl::img::tComplexPixel, VPL_VOLUME_DATA_ALLOCATOR>>;