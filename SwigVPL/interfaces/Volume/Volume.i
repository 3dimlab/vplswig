//==============================================================================
// This file is part of
//
// VPLswig - SWIG-based VPL bindings for Python
// Changes are Copyright 2016 3Dim Laboratory s.r.o.
// All rights reserved.
//
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.
//==============================================================================
// Module description 
%module (directors = "1", package = "VPLSwig.Volume") Volume
%include "../Core/Common.i"
%import "../Core/Core.i"
%import "../Image/Image.i"


#undef VPL_FFTW_ENABLED
#define VPL_VOLUME_DATA_ALLOCATION_IN_BLOCKS


%{
#include <VPL/Image/VolumeView.h>
#include <VPL/Image/VolumeBase.h>
#include <VPL/Image/Volume.h>
#include <VPL/Image/DensityVolume.h>

#include <VPL\ImageIO\DicomDirLoader.h>
#define SWIG_FILE_WITH_INIT
#include "Python.h"
#include <numpy/ndarrayobject.h>
#include <numpy/arrayobject.h>
#include <numpy/npy_common.h>
#include <numpy/numpy.h>
%}

%init
%{
    import_array();
%}
//%include <typemaps.i>

%import "VPL/Image/VolumeBase.h"
%include "VPL/Image/VolumeView.h"
%include "VPL/Image/Volume.h"


%extend vpl::img::CVolume{

PyObject* toNumpy()
{
    
    NPY_TYPES numpyType;
    if (typeid(T) == typeid(vpl::img::tPixel8))
        numpyType = NPY_UINT8;
    else if (typeid(T) == typeid(vpl::img::tPixel16))
        numpyType = NPY_UINT16;
    else if (typeid(T) == typeid(vpl::img::tPixel32))
        numpyType = NPY_UINT32;
    else if (typeid(T) == typeid(vpl::img::tFloatPixel))
        numpyType = NPY_FLOAT32;
    else if (typeid(T) == typeid(vpl::img::tDensityPixel))
        numpyType = NPY_INT16;
    else
    {
        PyErr_SetString(PyExc_ValueError, "This volume type is not supported.");
    }

    npy_intp dims[3] = {  self->getYSize(), self->getXSize(), self->getZSize() };
    PyObject* out;
    out = PyArray_SimpleNew(3, dims, numpyType);
    if (!out)
        return false;
    int xSize = self->getXSize();
    int ySize = self->getYSize();
    int zSize = self->getZSize();

    #pragma omp parallel for schedule(static) default(shared)
    for (int x = 0; x < xSize; ++x)
    {
        for (int y = 0; y < ySize; ++y)
        {
            for (int z = 0; z < zSize; ++z)
            {
                T* data = static_cast<T*>(PyArray_GETPTR3((PyArrayObject*)out,y,x,z));
                *data = self->at(x, y, z);
            }
        }
    }
    return out;
}

PyObject* toNumpy(PyObject* regionsDictionary )
{
    if (!PyDict_Check(regionsDictionary))
    {
        PyErr_SetString(PyExc_ValueError, "Arguments is not dictionary.");
        return false;
    }
    NPY_TYPES numpyType;
    if (typeid(T) == typeid(vpl::img::tPixel8))
        numpyType = NPY_UINT8;
    else if (typeid(T) == typeid(vpl::img::tPixel16))
        numpyType = NPY_UINT16;
    else if (typeid(T) == typeid(vpl::img::tPixel32))
        numpyType = NPY_UINT32;
    else if (typeid(T) == typeid(vpl::img::tFloatPixel))
        numpyType = NPY_FLOAT32;
    else if (typeid(T) == typeid(vpl::img::tDensityPixel))
        numpyType = NPY_INT16;
    else
    {
        PyErr_SetString(PyExc_ValueError, "This volume type is not supported.");
        return false;
    }

    npy_intp dims[3] = { self->getYSize(), self->getXSize(), self->getZSize() };
    PyObject* out;
    out = PyArray_SimpleNew(3, dims, numpyType);
    if (!out)
        return false;
    int xSize = self->getXSize();
    int ySize = self->getYSize();
    int zSize = self->getZSize();

#pragma omp parallel for schedule(static) default(shared)
    for (int x = 0; x < xSize; ++x)
    {
        for (int y = 0; y < ySize; ++y)
        {
            for (int z = 0; z < zSize; ++z)
            {
                T* data = static_cast<T*>(PyArray_GETPTR3((PyArrayObject*)out,y,x,z));
                T value = self->at(x, y, z);
                if(value & (value - 1))
                {
                    PyErr_SetString(PyExc_ValueError, "This volume contains more regions per voxel.");
                    return false;
                }
                if (value == 0)
                {
                    *data = 0;
                    continue;
                }

                unsigned i = 1;
                T pos = 1;
                // Iterate through bits of n till we find a set bit
                // i&n will be non-zero only when 'i' and 'n' have a set bit
                // at same position
                while (!(i & value))
                {
                    // Unset current bit and set the next bit in 'i'
                    i = i << 1;
                    // increment position
                    ++pos;
                }

                PyObject* key = PyUnicode_FromString(std::to_string(pos).c_str());
                *data = PyLong_AsLong(PyDict_GetItem(regionsDictionary, key));
            }
        }
    }
    return out;
}

bool fromNumpy(PyObject* in)
{
    NPY_TYPES numpyType;
    if (typeid(T) == typeid(vpl::img::tPixel8))
        numpyType = NPY_UINT8;
    else if (typeid(T) == typeid(vpl::img::tPixel16))
        numpyType = NPY_UINT16;
    else if (typeid(T) == typeid(vpl::img::tPixel32))
        numpyType = NPY_UINT32;
    else if (typeid(T) == typeid(vpl::img::tFloatPixel))
        numpyType = NPY_FLOAT32;
    else if (typeid(T) == typeid(vpl::img::tDensityPixel))
        numpyType = NPY_INT16;
    else
    {
        PyErr_SetString(PyExc_ValueError, "This volume type is not supported.");
    }

    int xSize = 0;
    int ySize = 0;
    int zSize = 0;


    // Check object type
    if (!is_array(in))
    {
        PyErr_SetString(PyExc_ValueError, "The given input is not known as a NumPy array or matrix.");
        return false;
    }

    // Check data type
    //TODO: Enabled? Must check type program or control user??
    /*else if (array_type(in) != numpyType)
    {
        PyErr_SetString(PyExc_ValueError, "Type mismatch between NumPy and VPL objects.");
        return false;
    }*/

    // Check dimensions
    else if (array_numdims(in) != 3)
    {
        PyErr_SetString(PyExc_ValueError, "Volume only support 3D array.");
        return false;
    }
    else
    {
        xSize = array_size(in, 1);
        ySize = array_size(in, 0);
        zSize = array_size(in, 2);

        self->resize(xSize, ySize, zSize);

    }
    // Extract data
    int isNewObject = 0;
    PyArrayObject* temp = obj_to_array_contiguous_allow_conversion(in, array_type(in), &isNewObject);

    if (temp == NULL)
    {
        PyErr_SetString(PyExc_ValueError, "Impossible to convert the input into a Python array object.");
        return false;
    }

    self->fill(0);

    #pragma omp parallel for schedule(static) default(shared)
    for (int x = 0; x < xSize; ++x)
    {
        for (int y = 0; y < ySize; ++y)
        {
            for (int z = 0; z  < zSize; ++z)
            {
                T* data = static_cast<T*>(PyArray_GETPTR3((PyArrayObject*)in, y, x, z));
                self->set(x, y, z, *data);
            }
        }
    }


    return true;
}

bool fromNumpy(PyObject* in, PyObject* regionsDictionary)
{
    if (!PyDict_Check(regionsDictionary))
    {
        PyErr_SetString(PyExc_ValueError, "Arguments is not dictionary.");
        return false;
    }

    NPY_TYPES numpyType;
    if (typeid(T) == typeid(vpl::img::tPixel8))
        numpyType = NPY_UINT8;
    else if (typeid(T) == typeid(vpl::img::tPixel16))
        numpyType = NPY_UINT16;
    else if (typeid(T) == typeid(vpl::img::tPixel32))
        numpyType = NPY_UINT32;
    else if (typeid(T) == typeid(vpl::img::tFloatPixel))
        numpyType = NPY_FLOAT32;
    else if (typeid(T) == typeid(vpl::img::tDensityPixel))
        numpyType = NPY_INT16;
    else
    {
        PyErr_SetString(PyExc_ValueError, "This volume type is not supported.");
    }

    int xSize = 0;
    int ySize = 0;
    int zSize = 0;


    // Check object type
    if (!is_array(in))
    {
        PyErr_SetString(PyExc_ValueError, "The given input is not known as a NumPy array or matrix.");
        return false;
    }

    // Check data type
    //TODO: Enabled? Must check type program or control user??
    /* else if (array_type(in) != numpyType)
    {
    PyErr_SetString(PyExc_ValueError, "Type mismatch between NumPy and VPL objects.");
    return false;
    }*/

    // Check dimensions
    else if (array_numdims(in) != 3)
    {
        PyErr_SetString(PyExc_ValueError, "Volume only support 3D array.");
        return false;
    }
    else
    {
        xSize = array_size(in, 1);
        ySize = array_size(in, 0);
        zSize = array_size(in, 2);

        self->resize(xSize, ySize, zSize);

    }
    // Extract data
    int isNewObject = 0;
    PyArrayObject* temp = obj_to_array_contiguous_allow_conversion(in, array_type(in), &isNewObject);

    if (temp == NULL)
    {
        PyErr_SetString(PyExc_ValueError, "Impossible to convert the input into a Python array object.");
        return false;
    }

    self->fill(0);

#pragma omp parallel for schedule(static) default(shared)
    for (int x = 0; x < xSize; ++x)
    {
        for (int y = 0; y < ySize; ++y)
        {
            for (int z = 0; z < zSize; ++z)
            {
                T* data = (static_cast<T*>(PyArray_GETPTR3(temp, y, x, z)));

                if (*data == 0)
                {
                    self->set(x, y, z, 0);
                    continue;
                }

                PyObject* key = PyUnicode_FromString(std::to_string(*data).c_str());
                int pos = PyLong_AsLong(PyDict_GetItem(regionsDictionary, key));


                T value = 1;
                while (pos > 1)
                {
                    value *= 2;
                    pos--;
                }


                self->set(x, y, z, value);
            }
        }
    }


    return true;
}
}

%ignore vpl::img::CVolume<vpl::img::tFloatPixel, VPL_VOLUME_DATA_ALLOCATOR>::toNumpy(PyObject*);
%ignore vpl::img::CVolume<vpl::img::tFloatPixel, VPL_VOLUME_DATA_ALLOCATOR>::fromNumpy(PyObject*, PyObject*);

%template(sp_densityVolume)vpl::base::CSharedPtr<vpl::img::CDensityVolume>;
%template(swig_base_Volume8)vpl::img::CVolumeBase< vpl::img::CVolume<vpl::img::tPixel8, VPL_VOLUME_DATA_ALLOCATOR> >;
%template(swig_base_Volume16)vpl::img::CVolumeBase< vpl::img::CVolume<vpl::img::tPixel16, VPL_VOLUME_DATA_ALLOCATOR> >;
%template(swig_base_Volume32)vpl::img::CVolumeBase< vpl::img::CVolume<vpl::img::tPixel32, VPL_VOLUME_DATA_ALLOCATOR> >;
%template(swig_base_FVolume)vpl::img::CVolumeBase< vpl::img::CVolume<vpl::img::tFloatPixel, VPL_VOLUME_DATA_ALLOCATOR> >;
%template(swig_base_DVolume)vpl::img::CVolumeBase< vpl::img::CVolume<vpl::img::tDensityPixel, VPL_VOLUME_DATA_ALLOCATOR> >;
//%template(swig_base_RGBAVolume)vpl::img::CVolumeBase< vpl::img::CVolume<vpl::img::tRGBAPixel, VPL_VOLUME_DATA_ALLOCATOR> >;
//%template(swig_base_ComplexVolume)vpl::img::CVolumeBase< vpl::img::CVolume<vpl::img::tComplexPixel, VPL_VOLUME_DATA_ALLOCATOR> >;



%template(CVolume8)vpl::img::CVolume<vpl::img::tPixel8, VPL_VOLUME_DATA_ALLOCATOR>;
%template(CVolume16)vpl::img::CVolume<vpl::img::tPixel16, VPL_VOLUME_DATA_ALLOCATOR>;
%template(CVolume32)vpl::img::CVolume<vpl::img::tPixel32, VPL_VOLUME_DATA_ALLOCATOR>;
%template(CFVolume)vpl::img::CVolume<vpl::img::tFloatPixel, VPL_VOLUME_DATA_ALLOCATOR>;
%template(CDVolume)vpl::img::CVolume<vpl::img::tDensityPixel, VPL_VOLUME_DATA_ALLOCATOR>;
//%template(CRGBAVolume)vpl::img::CVolume<vpl::img::tRGBAPixel, VPL_VOLUME_DATA_ALLOCATOR>;
//%template(CComplexVolume)vpl::img::CVolume<vpl::img::tComplexPixel, VPL_VOLUME_DATA_ALLOCATOR>;



%include "VPL/Image/DensityVolume.h"

%feature("ref")   vpl::base::CObject "$this->addReference();"
%feature("unref") vpl::base::CObject "$this->delReference();"
%feature("director");
