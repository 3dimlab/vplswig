//==============================================================================
// This file is part of
//
// VPLswig - SWIG-based VPL bindings for Python
// Changes are Copyright 2016 3Dim Laboratory s.r.o.
// All rights reserved.
//
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.
//==============================================================================

// Module description 
%module (package = "VPLSwig.Core") Progress




/* SWIG defines functuions and types*/
%include "stdint.i"
%include <std_string.i>
%include <std_except.i>

%include "typemaps.i"

%include "Common.i"

/*******************************************************************
// ************************ Defines base ***************************
// *****************************************************************/
%{
#include <VPL/System/SystemTypes.h>
#include <VPL/Base/Types.h>
#include <VPL/Module/ModuleExport.h>
%}



%include "VPL/System/SystemTypes.h"
%include "VPL/Base/Types.h"
%import "VPL/Module/ModuleExport.h"


/**************************************************************
// ************************ Defines ***************************
// ************************************************************/
%{
#include <VPL\Base\SharedPtr.h>

#include <VPL/Module/Progress.h>
%}


%include "VPL/Module/Progress.h"
