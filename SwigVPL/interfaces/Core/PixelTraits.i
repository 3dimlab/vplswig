//==============================================================================
// This file is part of
//
// VPLswig - SWIG-based VPL bindings for Python
// Changes are Copyright 2016 3Dim Laboratory s.r.o.
// All rights reserved.
//
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.
//==============================================================================
%{
#include <VPL/Image/PixelTraits.h>
#include <VPL/Base/TypeTraits.h>
%}

%include "VPL/Base/TypeTraits.h"
%include "VPL/Image/PixelTraits.h"

%template(swig_base_TPixel8)vpl::CTypeTraits< vpl::img::tPixel8 >;
%template(swig_base_TPixel16)vpl::CTypeTraits< vpl::img::tPixel16 >;
%template(swig_base_TPixel32)vpl::CTypeTraits< vpl::img::tPixel32 >;
%template(swig_base_TFloatPixel)vpl::CTypeTraits< vpl::img::tFloatPixel >;
%template(swig_base_TDensityPixel)vpl::CTypeTraits< vpl::img::tDensityPixel >;
%template(swig_base_TRGBPixel)vpl::CTypeTraits< vpl::img::tRGBPixel >;


%template(CTPixel8) vpl::img::CPixelTraits<vpl::img::tPixel8>;
%template(CTPixel16) vpl::img::CPixelTraits<vpl::img::tPixel16>;
%template(CTPixel32) vpl::img::CPixelTraits<vpl::img::tPixel32>;
%template(CTFloatPixel) vpl::img::CPixelTraits<vpl::img::tFloatPixel>;
%template(CTDensityPixel) vpl::img::CPixelTraits<vpl::img::tDensityPixel>;
%template(CTRGBAPixel) vpl::img::CPixelTraits<vpl::img::tRGBPixel>;
//%template(CTComplexPixel) vpl::img:CPixelTraits<vpl::img::tComplexPixel>;