//==============================================================================
// This file is part of
//
// VPLswig - SWIG-based VPL bindings for Python
// Changes are Copyright 2016 3Dim Laboratory s.r.o.
// All rights reserved.
//
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.
//==============================================================================

%import "../Volume/Volume.i"
%import "../Image/Image.i"


%{
#include <VPL/Image/Image.h>
#include <VPL\ImageIO\DicomSlice.h>
#include <VPL\ImageIO\DicomSliceLoader.h>
#include <VPL\ImageIO\DicomSliceLoaderVPL.h>
#include <VPL\ImageIO\DicomDirLoader.h>

#include <VPL/Module/Serialization.h>

#include <VPL/Image/Volume.h>
%}

%include "VPL/Module/Serialization.h"


%ignore ns::Wrapper::operator==;

%template(write_Image8)vpl::mod::write<vpl::img::CImage8>;
%template(read_Image8)vpl::mod::read<vpl::img::CImage8>;

%template(write_Image16)vpl::mod::write<vpl::img::CImage16>;
%template(read_Image16)vpl::mod::read<vpl::img::CImage16>;
%template(write_Image32)vpl::mod::write<vpl::img::CImage32>;
%template(read_Image32)vpl::mod::read<vpl::img::CImage32>;

%template(write_FImage)vpl::mod::write<vpl::img::CFImage>;
%template(read_FImage)vpl::mod::read<vpl::img::CFImage>;

%template(write_FImage)vpl::mod::write<vpl::img::CFImage>;
%template(read_FImage)vpl::mod::read<vpl::img::CFImage>;

%template(write_DImage)vpl::mod::write<vpl::img::CDImage>;
%template(read_DImage)vpl::mod::read<vpl::img::CDImage>;

%template(write_DImage)vpl::mod::write<vpl::img::CDImage>;
%template(read_DImage)vpl::mod::read<vpl::img::CDImage>;

%template(write_RGBAImage)vpl::mod::write<vpl::img::CRGBAImage>;
%template(read_RGBAImage)vpl::mod::read<vpl::img::CRGBAImage>;

%template(write_ComplexImage)vpl::mod::write<vpl::img::CComplexImage>;
%template(read_ComplexImage)vpl::mod::read<vpl::img::CComplexImage>;


//Volume
%template(write_Volume8)vpl::mod::write<vpl::img::CVolume8>;
%template(read_Volume8)vpl::mod::read<vpl::img::CVolume8>;

%template(write_Volume16)vpl::mod::write<vpl::img::CVolume16>;
%template(read_Volume16)vpl::mod::read<vpl::img::CVolume16>;
%template(write_Volume32)vpl::mod::write<vpl::img::CVolume32>;
%template(read_Volume32)vpl::mod::read<vpl::img::CVolume32>;

%template(write_FVolume)vpl::mod::write<vpl::img::CFVolume>;
%template(read_FVolume)vpl::mod::read<vpl::img::CFVolume>;

%template(write_FVolume)vpl::mod::write<vpl::img::CFVolume>;
%template(read_FVolume)vpl::mod::read<vpl::img::CFVolume>;

%template(write_DVolume)vpl::mod::write<vpl::img::CDVolume>;
%template(read_DVolume)vpl::mod::read<vpl::img::CDVolume>;

%template(write_DVolume)vpl::mod::write<vpl::img::CDVolume>;
%template(read_DVolume)vpl::mod::read<vpl::img::CDVolume>;