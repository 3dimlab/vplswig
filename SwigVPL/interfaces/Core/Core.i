//==============================================================================
// This file is part of
//
// VPLswig - SWIG-based VPL bindings for Python
// Changes are Copyright 2016 3Dim Laboratory s.r.o.
// All rights reserved.
//
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.
//==============================================================================

// Module description 
%module (directors = "1", package = "VPLSwig.Core") Core




/* SWIG defines functuions and types*/
%include "stdint.i"
%include <std_string.i>
%include <std_except.i>

%include "typemaps.i"

%include "Common.i"
%feature("director");

/*******************************************************************
// ************************ Defines base ***************************
// *****************************************************************/
%{
#include <VPL/System/SystemTypes.h>
#include <VPL/Base/Types.h>
#include <VPL/Base/BaseExport.h>
#include <VPL/Base/Object.h>
#include <VPL/Base/SharedPtr.h>
#include <VPL/Module/ModuleExport.h>
%}



%feature("ref")   vpl::base::CObject "$this->addReference();"
%feature("unref") vpl::base::CObject "$this->delReference();"

%include "VPL/System/SystemTypes.h"
%include "VPL/Base/Types.h"
%include "VPL/Base/BaseExport.h"
%include "VPL/Base/Object.h"
%include "VPL/Base/SharedPtr.h"
%import "VPL/Module/ModuleExport.h"


/**************************************************************
// ************************ Defines ***************************
// ************************************************************/
%{
#include <VPL/Image/ImageExport.h>
#include <VPL\ImageIO\ImageIOExport.h>
#include <VPL/Base/Range.h>

#include <VPL\Base\SharedPtr.h>
#include <VPL\Base\IteratorBase.h>
#include <VPL/Module/Channel.h>
#include <VPL/Module/Progress.h>
%}

%import "VPL/Image/ImageExport.h"
%import "VPL\ImageIO\ImageIOExport.h"



/****************************************************************
//************************** SWIG Parse ***************************
//*****************************************************************/

%import "VPL/Base/IteratorBase.h"
%import "VPL/Base/Exception.h"

%include "VPL/Base/Range.h"
%include "PixelTraits.i"

%include "Color.i"

%include "VPL/Module/Progress.h"
