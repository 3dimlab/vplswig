//==============================================================================
// This file is part of
//
// VPLswig - SWIG-based VPL bindings for Python
// Changes are Copyright 2016 3Dim Laboratory s.r.o.
// All rights reserved.
//
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.
//==============================================================================

// Module description 
%module (package = "VPLSwig.Core") Geometry
%include "Common.i"

%import "Core.i"

%{
#include <VPL/Image/Coordinates.h>
#include <VPL/Image/Size.h>
#include <VPL/Image/Point3.h>
#include <VPL/Image/Vector3.h>
%}

%{
#ifdef VPL_USE_64BIT_SIZE_TYPE
	typedef vpl::sys::tInt64 swig_tSize;
#else
	typedef vpl::sys::tInt32 swig_tSize;
#endif
%}

%include "VPL/Image/Coordinates.h"
%template(CCoordinates2_int)vpl::img::CCoordinates2<int>;
%template(CCoordinates2_tSize)vpl::img::CCoordinates2<swig_tSize>;
%template(CCoordinates2_float)vpl::img::CCoordinates2<float>;
%template(CCoordinates2_double)vpl::img::CCoordinates2<double>;

%template(CCoordinates3_int)vpl::img::CCoordinates3<int>;
%template(CCoordinates3_tSize)vpl::img::CCoordinates3<swig_tSize>;
%template(CCoordinates3_float)vpl::img::CCoordinates3<float>;
%template(CCoordinates3_double)vpl::img::CCoordinates3<double>;
%include "VPL/Image/Size.h"
%template(CSize2_int)vpl::img::CSize2<swig_tSize>;
%template(CSize2_float)vpl::img::CSize2<float>;
%template(CSize2_double)vpl::img::CSize2<double>;
                
%template(CSize3_int)vpl::img::CSize3<swig_tSize>;
%template(CSize3_float)vpl::img::CSize3<float>;
%template(CSize3_double)vpl::img::CSize3<double>;

%include "VPL/Image/Point3.h"
%template(CPoint2_int)vpl::img::CPoint2<swig_tSize>;
%template(CPoint2_float)vpl::img::CPoint2<float>;
%template(CPoint2_double)vpl::img::CPoint2<double>;
                 
%template(CPoint3_int)vpl::img::CPoint3<swig_tSize>;
%template(CPoint3_float)vpl::img::CPoint3<float>;
%template(CPoint3_double)vpl::img::CPoint3<double>;

%include "VPL/Image/Vector3.h"
%template(CVector2_int)vpl::img::CVector2<int>;
%template(CVector2_float)vpl::img::CVector2<float>;
%template(CVector2_double)vpl::img::CVector2<double>;
                  
%template(CVector3_int)vpl::img::CVector3<int>;
%template(CVector3_float)vpl::img::CVector3<float>;
%template(CVector3_double)vpl::img::CVector3<double>;
