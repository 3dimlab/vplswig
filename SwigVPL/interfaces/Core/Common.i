//==============================================================================
// This file is part of
//
// VPLswig - SWIG-based VPL bindings for Python
// Changes are Copyright 2016 3Dim Laboratory s.r.o.
// All rights reserved.
//
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.
//==============================================================================

#define WINDOWS

/***** Allow debug build even if no python_d.lib founded. ****/
%begin
%{
#ifdef _MSC_VER
#define SWIG_PYTHON_INTERPRETER_NO_DEBUG
#endif
%}


    // Generate docstring in output 
%feature("autodoc", "4");

// Disable some unused warnings
#pragma SWIG nowarn=362,312,325,350,302/*,325,312,401/*503*/
#pragma warning(suppress: 4244)


//System define
#ifdef WINDOWS

%{
#define NOMINMAX
#include <Windows.h>
%}
%include "Windows.i"

#define _WIN32
#endif

///* SWIG defines functuions and types*/
%include "stdint.i"
