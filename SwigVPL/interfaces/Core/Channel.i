//==============================================================================
// This file is part of
//
// VPLswig - SWIG-based VPL bindings for Python
// Changes are Copyright 2016 3Dim Laboratory s.r.o.
// All rights reserved.
//
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.
//==============================================================================

// Module description 
%module (package = "VPLSwig.Core") Channel
%include "Common.i"
%import "Core.i"
//%import "../Image/Image.i"


///* SWIG defines functuions and types*/
%include "stdint.i"
%include <std_string.i>
%include <std_except.i>

%include "typemaps.i"
%include "carrays.i"

%array_class(char, charArray);

%{
#include <VPL/Module/Serializable.h>
#include <VPL/Module/Channel.h>
%}

%ignore vpl::mod::CNullChannel;
%include "VPL/Module/Channel.h"

%include "Serialization.i"