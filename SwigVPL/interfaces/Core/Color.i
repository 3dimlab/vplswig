//==============================================================================
// This file is part of
//
// VPLswig - SWIG-based VPL bindings for Python
// Changes are Copyright 2016 3Dim Laboratory s.r.o.
// All rights reserved.
//
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.
//==============================================================================


/************************ Defines ***************************
 *************************************************************/
%{
#include <VPL/Image/RGBA.h>
#include <VPL/Image/PixelTypes.h>
#include <VPL/Image/Color.h>
%}

%include "VPL/Image/RGBA.h"
%include "VPL/Image/Color.h"
%include "VPL/Image/PixelTypes.h"