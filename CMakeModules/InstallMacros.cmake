  # Install macro
MACRO( LIBRARY_INSTALL )
    INSTALL( TARGETS ${LIBRARY_NAME}
            RUNTIME DESTINATION "${TRIDIM_PYTHONCPP_VER}/bin"
            LIBRARY DESTINATION "${TRIDIM_PYTHONCPP_VER}/lib"
             ARCHIVE DESTINATION "${TRIDIM_PYTHONCPP_VER}/lib" )
            
             
    FOREACH( HEADER ${HEADERS} )
      FILE( RELATIVE_PATH FILE "${CMAKE_INSTALL_PREFIX}/${TRIDIM_PYTHONCPP_VER}/include" ${HEADER} )
      GET_FILENAME_COMPONENT( DIR ${FILE} PATH )
      INSTALL( FILES ${HEADER} DESTINATION "${TRIDIM_PYTHONCPP_VER}/include/pycpp" )
    ENDFOREACH( HEADER )
ENDMACRO( LIBRARY_INSTALL )

MACRO( APP_INSTALL )
    INSTALL( TARGETS ${APP_NAME}
            RUNTIME DESTINATION "${TRIDIM_PYTHONCPP_VER}/bin"
            LIBRARY DESTINATION "${TRIDIM_PYTHONCPP_VER}/lib"
             ARCHIVE DESTINATION "${TRIDIM_PYTHONCPP_VER}/lib" )
ENDMACRO( APP_INSTALL )


MACRO(INSTALL_EXAMPLE_SOURCE moduleName)

    install( FILES ${CMAKE_SOURCE_DIR}/SwigVPL/Python/__init__.py
         DESTINATION ${CMAKE_INSTALL_PREFIX}/${TRIDIM_VPLSWIG_VER}/example/VPLSwig )
         
    install( FILES ${CMAKE_SOURCE_DIR}/SwigVPL/Python/__init__.py
         DESTINATION ${CMAKE_INSTALL_PREFIX}/${TRIDIM_VPLSWIG_VER}/example/VPLSwig/${package} )
         
    install( FILES ${CMAKE_SWIG_OUTDIR}/_${moduleName}.pyd
         DESTINATION ${CMAKE_INSTALL_PREFIX}/${TRIDIM_VPLSWIG_VER}/example/VPLSwig/${package} )
         
    install( FILES ${CMAKE_SWIG_OUTDIR}/${moduleName}.py
         DESTINATION ${CMAKE_INSTALL_PREFIX}/${TRIDIM_VPLSWIG_VER}/example/VPLSwig/${package} )

ENDMACRO(INSTALL_EXAMPLE_SOURCE)

MACRO(INSTALL_EXAMPLE)
    INSTALL(DIRECTORY ${PROJECT_SOURCE_DIR}/${TRIDIM_VPLSWIG_TEST}/python/data DESTINATION "${CMAKE_INSTALL_PREFIX}/${TRIDIM_VPLSWIG_VER}/example")
    FILE( GLOB DOC_TESTS ${PROJECT_SOURCE_DIR}/${TRIDIM_VPLSWIG_TEST}/python/*.py )

    FOREACH( EXAMPLE ${DOC_TESTS} )
        FILE( RELATIVE_PATH FILE "${CMAKE_INSTALL_PREFIX}/${TRIDIM_VPLSWIG_VER}/example" ${EXAMPLE} )
        GET_FILENAME_COMPONENT( DIR ${FILE} PATH )
        INSTALL( FILES ${EXAMPLE} DESTINATION "${TRIDIM_VPLSWIG_VER}/example" )
    ENDFOREACH( EXAMPLE )
ENDMACRO(INSTALL_EXAMPLE)

MACRO(INSTALL_DOC moduleName)
    install( FILES ${CMAKE_SOURCE_DIR}/SwigVPL/Python/__init__.py
         DESTINATION ${CMAKE_INSTALL_PREFIX}/${TRIDIM_VPLSWIG_DOC_VER}/source/VPLSwig )
         
    install( FILES ${CMAKE_SOURCE_DIR}/SwigVPL/Python/__init__.py
         DESTINATION ${CMAKE_INSTALL_PREFIX}/${TRIDIM_VPLSWIG_DOC_VER}/source/VPLSwig/${package} )
         
    install( FILES ${CMAKE_SWIG_OUTDIR}/_${moduleName}.pyd
         DESTINATION ${CMAKE_INSTALL_PREFIX}/${TRIDIM_VPLSWIG_DOC_VER}/source/VPLSwig/${package} )
         
    install( FILES ${CMAKE_SWIG_OUTDIR}/${moduleName}.py
         DESTINATION ${CMAKE_INSTALL_PREFIX}/${TRIDIM_VPLSWIG_DOC_VER}/source/VPLSwig/${package} )

ENDMACRO(INSTALL_DOC)

MACRO(INSTALL_LIBRARY moduleName)
    install( FILES ${CMAKE_SOURCE_DIR}/SwigVPL/Python/__init__.py
         DESTINATION ${CMAKE_INSTALL_PREFIX}/${TRIDIM_VPLSWIG_VER}/VPLSwig)
         
   install( FILES ${CMAKE_SOURCE_DIR}/SwigVPL/Python/__init__.py
         DESTINATION ${CMAKE_INSTALL_PREFIX}/${TRIDIM_VPLSWIG_VER}/VPLSwig/${package} )
         
    install( FILES ${CMAKE_SWIG_OUTDIR}/_${moduleName}.pyd
         DESTINATION ${CMAKE_INSTALL_PREFIX}/${TRIDIM_VPLSWIG_VER}/VPLSwig/${package} )
         
    install( FILES ${CMAKE_SWIG_OUTDIR}/${moduleName}.py
         DESTINATION ${CMAKE_INSTALL_PREFIX}/${TRIDIM_VPLSWIG_VER}/VPLSwig/${package} )
ENDMACRO(INSTALL_LIBRARY)

MACRO(INSTALL_ITEMS items package)

    foreach(item ${items})
        install(FILES ${VPL_3RDPARTY_DIR}/bin/${item} DESTINATION ${CMAKE_INSTALL_PREFIX}/${TRIDIM_VPLSWIG_VER}/VPLSwig/${package})
        if( BUILD_DOCUMENTATION )
                install(FILES ${VPL_3RDPARTY_DIR}/bin/${item} DESTINATION ${CMAKE_INSTALL_PREFIX}/${TRIDIM_VPLSWIG_DOC_VER}/source/VPLSwig/${package})
        endif()
        if( BUILD_VPLSWIG_EXAMPLES )
                install(FILES ${VPL_3RDPARTY_DIR}/bin/${item} DESTINATION ${CMAKE_INSTALL_PREFIX}/${TRIDIM_VPLSWIG_VER}/example/VPLSwig/${package})
        endif()
    endforeach()

ENDMACRO(INSTALL_ITEMS)


MACRO(INSTALL_ZLIB package)
    FILELIST(files "${VPL_3RDPARTY_DIR}/bin")
    
     #filter out non dll files
    foreach(file ${files})
    
        string(REGEX MATCH "^(.*zlib.dll|.*zlib.*[^d].dll)$" result ${file})

        if(NOT result STREQUAL "")
            list(APPEND dll_files ${file})
        endif()
    endforeach()
    
   
    INSTALL_ITEMS("${dll_files}" "${package}")
  
ENDMACRO(INSTALL_ZLIB)

MACRO(INSTALL_PNG package)
    FILELIST(files "${VPL_3RDPARTY_DIR}/bin")
    
     #filter out non dll files
    foreach(file ${files})
    
        string(REGEX MATCH "^(.*png.dll|.*png.*[^d].dll)$" result ${file})

        if(NOT result STREQUAL "")
            list(APPEND dll_files ${file})
        endif()
                
    endforeach()
    
  
    INSTALL_ITEMS("${dll_files}" "${package}")


ENDMACRO(INSTALL_PNG)
