#==============================================================================
# This file is part of
#
# VPLswig - SWIG-based VPL bindings for Python
# Changes are Copyright 2015 3Dim Laboratory s.r.o.
# All rights reserved.
#
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.
#==============================================================================


############################ Helper methods ##########################################
MACRO(VERSIONS_LIST result curdir)
  FILE(GLOB children RELATIVE ${curdir} ${curdir}/*)
  SET(dirlist "")
  FOREACH(child ${children})
    IF(IS_DIRECTORY ${curdir}/${child})
      LIST(APPEND dirlist ${child})
    ENDIF()
  ENDFOREACH()
  SET(${result} ${dirlist})
ENDMACRO()


MACRO(EXTRACT_LIB_FILES)
    SET(libstolinkwith)
    FOREACH(it ${ARGN})
        SET(libstolinkwith ${libstolinkwith} "optimized;${it}${VPL_LIBRARY_SUFFIX};debug;${it}${VPL_LIBRARY_SUFFIX}d;")
    ENDFOREACH(it)
ENDMACRO(EXTRACT_LIB_FILES)

MACRO(ADD_EXTERNAL_LIBRARY)
    FOREACH(it ${ARGN})
        set(external_library ${external_library} "${it}")
    ENDFOREACH(it)
ENDMACRO(ADD_EXTERNAL_LIBRARY)


macro( ADD_SOURCE_GROUPS _DIR_INCLUDE _DIR_SOURCE )
  source_group( "${_DIR_INCLUDE}" REGULAR_EXPRESSION ".*/${_DIR_INCLUDE}/[^/]*\\.(h|hxx|hpp)$" )
  source_group( "${_DIR_SOURCE}" REGULAR_EXPRESSION ".*/${_DIR_SOURCE}/[^/]*\\.(c|cpp)$" )

  foreach( arg ${ARGN} )
    source_group( "${_DIR_INCLUDE}/${arg}" REGULAR_EXPRESSION ".*/${_DIR_INCLUDE}/${arg}/[^/]*\\.(h|hxx|hpp)$" )
    source_group( "${_DIR_SOURCE}/${arg}" REGULAR_EXPRESSION ".*/${_DIR_SOURCE}/${arg}/[^/]*\\.(c|cpp)$" )
  endforeach( arg )
endmacro( ADD_SOURCE_GROUPS )


#lists all files in given current directory
macro(FILELIST result curdir)
  file(GLOB children RELATIVE ${curdir} ${curdir}/*)
  set(filelist "")
  foreach(child ${children})
    if(NOT IS_DIRECTORY ${curdir}/${child})
      list(APPEND filelist "${child}")
    endif()
  endforeach()

  set(${result} ${filelist})
endmacro()




############################### Library and execution ###################################
# Library name declaration
MACRO( LIBRARY _LIBRARY_NAME )
    SET( LIBRARY_NAME ${PYTHON_LIBRARY_PREFIX}${_LIBRARY_NAME}${PYTHON_LIBRARY_SUFFIX} )
    SET( PROJECT_NAME lib${_LIBRARY_NAME} )
    SET( SOURCES "" )
    SET( HEADERS "" )
ENDMACRO( LIBRARY )

MACRO( APP _APP_NAME )
	SET( APP_NAME ${_APP_NAME} )
    SET( PROJECT_NAME ${_APP_NAME} )
    SET( SOURCES "" )
    SET( HEADERS "" )
ENDMACRO( APP )

# Final library building macro
MACRO( LIBRARY_BUILD )
    ADD_LIBRARY( ${LIBRARY_NAME}
        ${SOURCES}
        ${HEADERS})
        
    SET_TARGET_PROPERTIES( ${LIBRARY_NAME} PROPERTIES
        PROJECT_LABEL ${PROJECT_NAME}
        DEBUG_POSTFIX d
        LINK_FLAGS "${PYTHON_LINK_FLAGS}" )
ENDMACRO( LIBRARY_BUILD )
  
# Final app building macro
MACRO( APP_BUILD )
    add_executable( ${APP_NAME}
        ${SOURCES}
        ${HEADERS})
		
    TARGET_LINK_LIBRARIES( ${APP_NAME}
	"${PYTHON_CPP}"
	${VPL_LIBRARIES}
	)
	if( BUILD_WITH_GDCM )
        TARGET_LINK_LIBRARIES( ${APP_NAME}
		${VPL_GDCM_LIBS}
	)

	include_directories(${VPL_GDCM_INCLUDE_DIR})
    endif( BUILD_WITH_GDCM )
	
    SET_TARGET_PROPERTIES( ${APP_NAME} PROPERTIES
        PROJECT_LABEL ${PROJECT_NAME}
        DEBUG_POSTFIX d
        LINK_FLAGS "${PYTHON_LINK_FLAGS}" )
ENDMACRO( APP_BUILD )

# Adding dependencies
MACRO( LIBRARY_DEP _NAME )
    TARGET_LINK_LIBRARIES( ${LIBRARY_NAME} ${_NAME}${PYTHON_LIBRARY_SUFFIX} )
ENDMACRO( LIBRARY_DEP )
  
   
# Macro for adding source file to library build
MACRO( PYTHON_LIBRARY_SOURCE )
    SET( SOURCES ${SOURCES} ${ARGV} )
ENDMACRO( PYTHON_LIBRARY_SOURCE )

# Macro for adding header file to library
MACRO( PYTHON_LIBRARY_HEADER )
    SET( HEADERS ${HEADERS} ${ARGV} )
ENDMACRO( PYTHON_LIBRARY_HEADER )

# Macro for adding all header files in a specified directory to library
MACRO( INCLUDE_DIR _DIR )
    FILE( GLOB_RECURSE HEADERS ${_DIR}/*.h ${_DIR}/*.hxx )
ENDMACRO( INCLUDE_DIR )
  
# Macro for adding all source files in a specified directory to library
MACRO( SRC_DIR _DIR )
    FILE( GLOB_RECURSE SOURCES ${_DIR}/*.cc ${_DIR}/*.cpp ${_DIR}/*.cc  )
ENDMACRO( SRC_DIR )


############################ Swig ##########################################
# Include main swig interface file, which will become swig project.
MACRO(ADD_MAIN_INTERFACE interfaceFile)
    SET(mainInterface ${interfaceFile})
  	set(CPPINTEROP 0)
    set(ENABLE_BUILTIN 1)
ENDMACRO(ADD_MAIN_INTERFACE)

# Include helper interface file
MACRO(ADD_INTERFACE interfaceFile)
    SET(anotherFiles ${anotherFiles} ${interfaceFile})
ENDMACRO(ADD_INTERFACE)

# New swig project
MACRO(ADD_SWIG_VPL_PYTHON_MODULE moduleName) 
    SET (MODULENAME "${moduleName}")  

	if(${CMAKE_GENERATOR} MATCHES "64")
        SET(ARCHITECTURE "x64")
	else(${CMAKE_GENERATOR} MATCHES "64")
		SET(ARCHITECTURE "x86")
    endif(${CMAKE_GENERATOR} MATCHES "64")
    
    SET(CMAKE_SWIG_OUTDIR "${CMAKE_BINARY_DIR}/python/${ARCHITECTURE}")
    SET(CMAKE_LIBRARY_OUTPUT_DIRECTORY_RELEASE "${CMAKE_BINARY_DIR}/python/${ARCHITECTURE}")
    SET(CMAKE_LIBRARY_OUTPUT_DIRECTORY_DEBUG "${CMAKE_BINARY_DIR}/python/${ARCHITECTURE}")
    
    include_directories( "${VPL_INCLUDE_DIR}/VPL/${moduleName}" )
    include_directories(${CMAKE_CURRENT_SOURCE_DIR})
    
    if( BUILD_WITH_GDCM )
        include_directories( ${VPL_GDCM_INCLUDE_DIR} )
        include_directories( ${VPL_ZLIB_INCLUDE_DIR} )
    endif( BUILD_WITH_GDCM )
    
    EXTRACT_LIB_FILES(${ARGN})

    if(BUILD_WITH_BUILTIN AND ENABLE_BUILTIN)
        SET_SOURCE_FILES_PROPERTIES(${mainInterface} PROPERTIES CPLUSPLUS ON SWIG_FLAGS "-builtin")
	else(BUILD_WITH_BUILTIN AND ENABLE_BUILTIN)
        SET_SOURCE_FILES_PROPERTIES(${mainInterface} PROPERTIES CPLUSPLUS ON SWIG_FLAGS "")
    endif(BUILD_WITH_BUILTIN AND ENABLE_BUILTIN)
	####################################
    # Detail info about swig exporting.
    # SET_SOURCE_FILES_PROPERTIES(${mainInterface} PROPERTIES CPLUSPLUS ON SWIG_FLAGS "-debug-tmsearch")
    ####################################

    ################ Documentation #################
    if(BUILD_DOCUMENTATION)
        INSTALL_DOC(${MODULENAME})
    endif()  
    ################ Tests #################    
    if(BUILD_VPLSWIG_EXAMPLES)
        INSTALL_EXAMPLE_SOURCE(${MODULENAME})
    endif()  
    ################ Library #######################
    INSTALL_LIBRARY(${MODULENAME})
             
    swig_add_library(${MODULENAME} LANGUAGE python SOURCES ${mainInterface})    
    SWIG_LINK_LIBRARIES( ${MODULENAME} ${libstolinkwith} ${external_library})
    SWIG_LINK_LIBRARIES ( ${MODULENAME} ${PYTHON_LIBRARIES} )
    SWIG_LINK_LIBRARIES(${MODULENAME} ${PYTHON_LIBRARY})

    if( BUILD_WITH_GDCM )
        SWIG_LINK_LIBRARIES( ${MODULENAME} ${VPL_GDCM_LIBS})
        SWIG_LINK_LIBRARIES( ${MODULENAME} ${VPL_ZLIB})
		
    endif( BUILD_WITH_GDCM)
	if( BUILD_WITH_XML )
        SWIG_LINK_LIBRARIES( ${MODULENAME} ${VPL_TinyXML_LIBS})
    endif( BUILD_WITH_XML )
    
    target_compile_options("${MODULENAME}"  PRIVATE -DSWIG_TYPE_TABLE=TRIDIM)
       
    SET(package "")  
    
    SET(mainInterface "")
        
    FOREACH(it ${anotherFiles})
        target_sources("${MODULENAME}"  PUBLIC "${it}")
    ENDFOREACH(it)
    set(anotherFiles "")
    
    FOREACH(it ${pythonFiles})
        target_sources("${MODULENAME}"  PUBLIC "${it}")
    ENDFOREACH(it)
     set(pythonFiles "")
     
ENDMACRO(ADD_SWIG_VPL_PYTHON_MODULE)

# Settings for exclude module as main module for loading with c++
# DEPRECATED: Probably in new version not used.
MACRO(SET_CPPINTEROP)
set(CPPINTEROP 1)
ENDMACRO(SET_CPPINTEROP)

# Enable swig exportation faster with builtin option.
MACRO(SET_NO_BUILTIN)
    set(ENABLE_BUILTIN 0)
ENDMACRO(SET_NO_BUILTIN)

# URI of package
MACRO(SET_PACKAGE packageURI)
    SET(package ${packageURI})
ENDMACRO(SET_PACKAGE)
  
# Define name DSWIG_TYPE_TABLE for searching by swig runtime in c++ app.
MACRO(SET_DSWIG_TYPE)
    target_compile_options("_${MODULENAME}"  PRIVATE -DSWIG_TYPE_TABLE=TRIDIM)
ENDMACRO(SET_DSWIG_TYPE)

# Include python file to swig project.
MACRO(ADD_PYTHON pythonFile)
    SET(pythonFiles ${pythonFiles} ${pythonFile})
ENDMACRO(ADD_PYTHON)









    