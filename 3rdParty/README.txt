#==============================================================================
# This file is part of
#
# VPLswig - SWIG-based VPL bindings for Python
# Changes are Copyright 2015 3Dim Laboratory s.r.o.
# All rights reserved.
#
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.
#==============================================================================

- Download the zip archive of 3rd party libraries for use with VPLSWIG 
  from https://bitbucket.org/3dimlab/vplswig/downloads/.

- Unpack content of this archive into the 'VPLSWIG/3rdParty' source directory
  or somewhere else...