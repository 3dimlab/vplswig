from VPLSwig.Volume import Volume
from VPLSwig.Volume import VolumeFilters
from VPLSwig.Image import Image
from VPLSwig.Core import Core


def volume_progress(volume, progress):
    """
        :type volume: Volume.CDensityVolume
        :type progress: Core.CProgress
    """
    progress.setProgressMax(50)
    anisotropic = VolumeFilters.CVolumeAnisotropicFilter_DVolume(0.5, 50)
    anisotropic.copy(progress)
    volume.fill(500)
    anisotropic(volume, volume)


volume = Volume.CDensityVolume(10, 10, 10)
pvolume = Volume.sp_densityVolume(volume)
spvolume = Volume.sp_densityVolume(volume)

progress = Core.CProgress()
progress.setProgressMax(50)

dcmLoader = Image.CDicomDirLoader()
dcmLoader.loadDicomDir("./data/dicom")


ret = dcmLoader.makeDensityVolume(spvolume.get())

volume_progress(spvolume.get(), progress)

spvolume.release()
pvolume.release()
