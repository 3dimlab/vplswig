import os
import numpy
from VPLSwig.Volume import Volume
from VPLSwig.Core import Channel

#
channelIn = Channel.CFileChannel(Channel.CH_IN, "./data/test.seg")
if channelIn.isConnected():
    volume = Volume.CVolume16()
    Channel.read_Volume16(volume, channelIn)
    channelIn.disconnect()

    print("================== Input ==================")
    print("XSize: " + str(volume.getXSize()))
    print("YSize: " + str(volume.getYSize()))
    print("ZSize: " + str(volume.getZSize()))

    print("================= Converting to numpy ===================")
    numpyVolume = volume.toNumpy()
    print("Numpy size (y.x.z): " + str(numpyVolume.shape))
    print("================= Converting from numpy =================")
    volume.fromNumpy(numpyVolume)
    print("XSize: " + str(volume.getXSize()))
    print("YSize: " + str(volume.getYSize()))
    print("ZSize: " + str(volume.getZSize()))

else:
    print("Channel is not opened")