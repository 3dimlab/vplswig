from VPLSwig.Core import Core

rgba1 = Core.CRGBA(5,10,150)
print(rgba1.getGreen())
rgba1.setGreen(35)
print(rgba1.getGreen())

rgba2 = Core.CRGBA(15,10,150,10)
print(rgba2 == rgba1)

rgba2.setRed(5)
rgba2.setGreen(35)
rgba2.setBlue(150)
rgba2.setAlpha(255)
print(rgba2 == rgba1)

