from VPLSwig.Image import Image
from VPLSwig.Image import ImageFilters
from VPLSwig.Core import Channel
from VPLSwig.Image import IO

channelIn = Channel.CFileChannel(Channel.CH_IN, "data/inputImage.png")
channelOut = Channel.CFileChannel(Channel.CH_OUT, "outImage.png")

image = Image.CImage16(100, 100, 10)
IO.loadGrayPNG(image, channelIn)

imageB = Image.CImage16(image.getSize())

filt = ImageFilters.CAvg7Filter_Image16()
print(filt(image, imageB))

IO.saveGrayPNG(imageB, channelOut)
channelOut.disconnect()