from VPLSwig.Image import Image
from VPLSwig.Image import ImageFilters
from VPLSwig.Core import Channel
from VPLSwig.Image import IO

channelIn = Channel.CFileChannel(Channel.CH_IN, "data/image.png")
channelOut = Channel.CFileChannel(Channel.CH_OUT, "outCanny.png")

image = Image.CImage16(100,100,10)
IO.loadGrayPNG(image,channelIn)
imageB = Image.CImage16(image.getSize())

filt = ImageFilters.CCanny_Image16(0.1,0.5,0.3)
print(filt(image,imageB))

IO.saveGrayPNG(imageB,channelOut)
channelOut.disconnect()