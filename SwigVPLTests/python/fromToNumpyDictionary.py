import os
import numpy
from VPLSwig.Volume import Volume
from VPLSwig.Core import Channel
from VPLSwig.Image import Image
import xml.etree.ElementTree as xml

# parsing config xml, contains dictionary for conversion. Used when regions are converted.
# bit position : value in numpy
configXML = xml.parse('./data/configToNumpy.xml').getroot()

regionsDictToNumpy = {}
# initialize dictionary
for child in configXML:
    regionsDictToNumpy[str(child.get('name'))] = int(child.find('value').text)
print("RegionsMapToNumpy")
print(regionsDictToNumpy)

# parsing config xml, contains dictionary for conversion. Used when regions are converted.
# bit position : value in numpy
configXML = xml.parse('./data/configFromNumpy.xml').getroot()

regionsDictFromNumpy = {}
# initialize dictionary
for child in configXML:
    regionsDictFromNumpy[str(child.find('value').text)] = int(child.get('name'))

print("RegionsDictFromNumpy")
print(regionsDictFromNumpy)

channelIn = Channel.CFileChannel(Channel.CH_IN, "./data/test.mseg")
if channelIn.isConnected():
    volume = Volume.CVolume32()
    Channel.read_Volume32(volume, channelIn)
    channelIn.disconnect()


    print("================== Input values ==================")
    print("[0,0,0]: {0}".format(volume.at(0, 0, 0)))
    print("[1,1,1]: {0}".format(volume.at(1, 1, 1)))
    print("[2,2,2]: {0}".format(volume.at(2, 2, 2)))
    print("[3,3,3]: {0}".format(volume.at(3, 3, 3)))
    print("[4,4,4]: {0}".format(volume.at(4, 4, 4)))
    print("[5,5,5]: {0}".format(volume.at(5, 5, 5)))
    print("[6,6,6]: {0}".format(volume.at(6, 6, 6)))
    print("[7,7,7]: {0}".format(volume.at(7, 7, 7)))
    print("[8,8,8]: {0}".format(volume.at(8, 8, 8)))
    print("[9,9,9]: {0}".format(volume.at(9, 9, 9)))




    print("================= Converting to numpy ===================")
    numpyVolume = volume.toNumpy(regionsDictToNumpy)
    print("[0,0,0]: {0}".format(numpyVolume[0, 0, 0]))
    print("[1,1,1]: {0}".format(numpyVolume[1, 1, 1]))
    print("[2,2,2]: {0}".format(numpyVolume[2, 2, 2]))
    print("[3,3,3]: {0}".format(numpyVolume[3, 3, 3]))
    print("[4,4,4]: {0}".format(numpyVolume[4, 4, 4]))
    print("[5,5,5]: {0}".format(numpyVolume[5, 5, 5]))
    print("[6,6,6]: {0}".format(numpyVolume[6, 6, 6]))
    print("[7,7,7]: {0}".format(numpyVolume[7, 7, 7]))
    print("[8,8,8]: {0}".format(numpyVolume[8, 8, 8]))
    print("[9,9,9]: {0}".format(numpyVolume[9, 9, 9]))


    print("================= Converting from numpy =================")
    volume.fromNumpy(numpyVolume, regionsDictFromNumpy)
    print("[0,0,0]: {0}".format(volume.at(0, 0, 0)))
    print("[1,1,1]: {0}".format(volume.at(1, 1, 1)))
    print("[2,2,2]: {0}".format(volume.at(2, 2, 2)))
    print("[3,3,3]: {0}".format(volume.at(3, 3, 3)))
    print("[4,4,4]: {0}".format(volume.at(4, 4, 4)))
    print("[5,5,5]: {0}".format(volume.at(5, 5, 5)))
    print("[6,6,6]: {0}".format(volume.at(6, 6, 6)))
    print("[7,7,7]: {0}".format(volume.at(7, 7, 7)))
    print("[8,8,8]: {0}".format(volume.at(8, 8, 8)))
    print("[9,9,9]: {0}".format(volume.at(9, 9, 9)))
else:
    print("Channel is not opened")