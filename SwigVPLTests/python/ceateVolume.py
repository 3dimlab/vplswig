from VPLSwig.Volume import Volume
from VPLSwig.Volume import VolumeFilters
from VPLSwig.Image import ImageFunctions
from VPLSwig.Image import Image
from VPLSwig.Core import Channel
from VPLSwig.Image import IO

volume = Volume.CDensityVolume(10,10,10)
pvolume = Volume.sp_densityVolume(volume)
spvolume = Volume.sp_densityVolume(volume)


dcmLoader = Image.CDicomDirLoader()
dcmLoader.loadDicomDir("./data/dicom")


ret = dcmLoader.makeDensityVolume(spvolume.get())

slc = Image.CDicomSlice(512,512)
spslice = Image.sp_dicomSlice(slc)
print(spvolume.getSliceXY(0,spslice.get()))

imageA = Image.CImage16(400,400)
imageA.convert_from_DImage(slc)

channelOut = Channel.CFileChannel(Channel.CH_OUT, "noFiltered.png")
IO.saveGrayPNG(imageA,channelOut)
channelOut.disconnect()
a = ImageFunctions.CHistogram_Image16()
img = Image.CImage16(100,100)
img.fill(100)
img.set(50,50,20000)
img.set(60,60,500)

a(img)


filtvolume = Volume.CDensityVolume(spvolume.getXSize(),spvolume.getYSize(),spvolume.getZSize())

filt = VolumeFilters.CVolumeAvg3Filter_DVolume()
filt(volume,filtvolume)

slcFilter = Image.CDicomSlice(512,512)
spsliceFilter = Image.sp_dicomSlice(slcFilter)
print(filtvolume.getSliceXY(0,spsliceFilter.get()))

imageFiltered = Image.CImage16(400,400)

imageFiltered.convert_from_DImage(slcFilter)

channelOut2 = Channel.CFileChannel(Channel.CH_OUT, "yesFiltered.png")
IO.saveGrayPNG(imageFiltered,channelOut2)
channelOut2.disconnect()


spslice.release()
spsliceFilter.release()
spvolume.release()
pvolume.release()