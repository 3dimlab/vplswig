from VPLSwig.Volume import Volume
from VPLSwig.Core import Channel


channelOut = Channel.CFileChannel(Channel.CH_OUT, "./data/test.mseg")
if channelOut.isConnected():
	volume = Volume.CVolume32(10, 10, 10)
	volume.fillEntire(0)
	volume.set(0, 0, 0, 0)
	volume.set(1, 1, 1, 1)
	volume.set(2, 2, 2, 2)
	volume.set(3, 3, 3, 4)
	volume.set(4, 4, 4, 8)
	volume.set(5, 5, 5, 16)
	volume.set(6, 6, 6, 32)
	volume.set(7, 7, 7, 64)
	volume.set(8, 8, 8, 128)
	volume.set(9, 9, 9, 256)

	Channel.write_Volume32(volume, channelOut)
	channelOut.disconnect()
else:
	print("Failed open channel")

