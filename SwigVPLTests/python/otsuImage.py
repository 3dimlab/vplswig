from VPLSwig.Image import Image
from VPLSwig.Image import ImageFunctions
from VPLSwig.Core import Channel
from VPLSwig.Image import IO

channelIn = Channel.CFileChannel(Channel.CH_IN, "data/otsu.png")
channelOut = Channel.CFileChannel(Channel.CH_OUT, "outOtsu.png")

image = Image.CImage16(100,100,10)
IO.loadGrayPNG(image,channelIn)

print(ImageFunctions.singleOtsuThresholding_Image16(image))

#method return pixels 0 or 1, for saving as PNG is required moved value 1 to higher.
image.replace(1, 65500)
				
IO.saveGrayPNG(image, channelOut)
channelOut.disconnect()