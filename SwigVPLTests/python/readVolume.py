from VPLSwig.Volume import Volume
from VPLSwig.Core import Channel

channelIn = Channel.CFileChannel(Channel.CH_IN, "./data/test.mseg")
if channelIn.isConnected():
	volume = Volume.CVolume32()
	Channel.read_Volume32(volume, channelIn)
	channelIn.disconnect()

	print("XSize: " + str(volume.getXSize()))
	print("YSize: " + str(volume.getYSize()))
	print("ZSize: " + str(volume.getZSize()))

