import numpy as np

def function1(n):
    print("Python value: ")
    print(n[0,14,4,2,0])
    n[0,14,4,2,0] = 255
    r = np.array([[1.5, 10.8], [3.3, 4.4]])
    print(r[0,1])
    return r
	
def importNumpy(path):
	array = np.load(path)
	return array
