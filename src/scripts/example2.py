from VPLSwig.Volume import Volume
from VPLSwig.Volume import VolumeFilters
from VPLSwig.Core import Core
from VPLSwig.Image import Image
from VPLSwig.Image import IO


def function1(path):
	"""
	:param path: string
	:param progress: Core.CProgress
	"""

	print(path)
	dcmLoader = Image.CDicomDirLoader()
	print(dcmLoader.loadDicomDir(path))
	retVolume = Volume.CDensityVolume()
	print(dcmLoader.makeDensityVolume(retVolume))
	return retVolume

 
#data = "C:/Users/melcer/Desktop/SyncResillio/Data/AssKull/ASS000/ASS000" 
#data = "C:/Users/melcer/Desktop/SyncResillio/Data/VPLTestData"
#data = "C:/Users/melcer/Desktop/SyncResillio/Data/ASTibia/AST001L/AST001L/anyfolder"
#function1(data)