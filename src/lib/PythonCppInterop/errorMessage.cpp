//==============================================================================
/* This file is part of
*
* VPLSWIG - Voxel Processing Library
* Copyright 2017 3Dim Laboratory s.r.o.
* All rights reserved.
*
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/
#include <pycpp/errorMessage.h>

namespace pycpp
{

void ErrorMessage::clear()
{
    head = type = body = traceback = "";
}

std::string ErrorMessage::operator()() const
{
    std::string message = head + "\n";
    message += "Error: " + type + "\n";
    message += "Text: " + body + "\n";
    message += "Trace: " + traceback + "\n";
    return message;
}
}
