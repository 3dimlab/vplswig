//==============================================================================
/* This file is part of
*
* VPLSWIG - Voxel Processing Library
* Copyright 2017 3Dim Laboratory s.r.o.
* All rights reserved.
*
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/
#include <pycpp/numpyType.h>
#include <numpy/ndarrayobject.h>
#include <numpy/arrayobject.h>
#include <numpy/npy_common.h>

#include <numpy/numpy.h>


namespace pycpp
{
NPY_TYPES types2Npy(const Types type)
{
	switch (type)
	{
	case INT8: return NPY_INT8;
	case UINT8: return NPY_UINT8;
	case INT16: return NPY_INT16;
	case UINT16: return NPY_UINT16;
	case INT32: return NPY_INT32;
	case UINT32: return NPY_UINT32;
	case FLOAT: return NPY_FLOAT;
	case DOUBLE: return NPY_DOUBLE;
	default: return NPY_NOTYPE;
	}
}
Types npy2Types(const NPY_TYPES npyType)
{
	switch (npyType)
	{
	case NPY_INT8: return INT8;
	case NPY_UINT8: return UINT8;
	case NPY_INT16: return INT16;
	case NPY_UINT16: return UINT16;
	case NPY_INT32: return INT32;
	case NPY_UINT32: return UINT32;
	case NPY_FLOAT: return FLOAT;
	case NPY_DOUBLE: return DOUBLE;
	default: return NOTYPE;
	}
}




NumpyType::NumpyType() : m_type(), m_obj(nullptr), m_dimensionCount(0)
{
}

NumpyType::~NumpyType()
{
    Py_CLEAR(m_obj);
}


bool NumpyType::create(PyObject* object)
{
	importArray();

	// Check object type
	if (!isArray(object))
	{
		std::cerr << "The given input is not known as a NumPy array" << std::endl;
		return false;
	}
	m_obj = object;

	m_dimensionCount = array_numdims(m_obj);
	for (int i = 0; i < m_dimensionCount; i++)
	{
		m_dimensionSizes.push_back(array_size(m_obj, i));
	}
	m_type = npy2Types(static_cast<NPY_TYPES>(array_type(object)));

	return true;
}

bool NumpyType::importArray()
{
	if (PyArray_API == nullptr)
	{
		import_array();
	}
	return true;
}

int NumpyType::getDimCount() const
{
	return m_dimensionCount;
}

int NumpyType::getDimSize(int dimIndex)
{
	return m_dimensionSizes.at(dimIndex);
}

bool NumpyType::isArray(PyObject* object)
{
	return is_array(object);
}

PyObject* NumpyType::pyObject() const
{
	return m_obj;
}

bool NumpyType::createArray()
{
	std::vector<npy_intp> vecDims;

	for (int i = 0; i < m_dimensionCount; i++)
	{
		const int n = m_dimensionSizes.at(i);
		vecDims.push_back(n);
	}
	npy_intp* npyDims = &vecDims[0];

	const NPY_TYPES npType = types2Npy(m_type);

	m_obj = PyArray_SimpleNew(m_dimensionSizes.size(), npyDims, npType);
	return m_obj != nullptr;
}


Types NumpyType::type() const
{
	return m_type;
}

std::string NumpyType::typeToStr() const
{
	switch (m_type)
	{
	case INT8: return "Int8";
	case UINT8: return "UInt8";
	case INT16: return "Int16";
	case UINT16: return "UInt16";
	case INT32: return "Int32";
	case UINT32: return "UInt32";
	case FLOAT: return "Float";
	case DOUBLE: return "Double";
	default: return "NOTYPE";
	}
}

void* NumpyType::getPtr(int dim1) const
{
	int isNewObject = 0;
	PyArrayObject* arrayObj = obj_to_array_contiguous_allow_conversion(m_obj, array_type(m_obj), &isNewObject);

	npy_intp ofset = 0;

	assert(dim1 < array_size(m_obj, 0));
	ofset += (dim1)* PyArray_STRIDES(arrayObj)[0];

	return static_cast<void*>(PyArray_BYTES(arrayObj) + ofset);
}
void* NumpyType::getPtr(int dim1, int dim2) const
{
	int isNewObject = 0;
	PyArrayObject* arrayObj = obj_to_array_contiguous_allow_conversion(m_obj, array_type(m_obj), &isNewObject);

	npy_intp ofset = 0;

	assert(dim1 < array_size(m_obj, 0));
	ofset += (dim1)* PyArray_STRIDES(arrayObj)[0];

	assert(dim2 < array_size(m_obj, 1));
	ofset += (dim2)* PyArray_STRIDES(arrayObj)[1];

	return static_cast<void*>(PyArray_BYTES(arrayObj) + ofset);
}

void* NumpyType::getPtr(const int dim1, const int dim2, const int dim3) const
{
	int isNewObject = 0;
	PyArrayObject* arrayObj = obj_to_array_contiguous_allow_conversion(m_obj, array_type(m_obj), &isNewObject);

	npy_intp ofset = 0;

	assert(dim1 < array_size(m_obj, 0));
	ofset += (dim1)* PyArray_STRIDES(arrayObj)[0];

	assert(dim2 < array_size(m_obj, 1));
	ofset += (dim2)* PyArray_STRIDES(arrayObj)[1];


	assert(dim3 < array_size(m_obj, 2));
	ofset += (dim3)* PyArray_STRIDES(arrayObj)[2];

	return static_cast<void*>(PyArray_BYTES(arrayObj) + ofset);
}


void* NumpyType::getPtr(const int* args) const
{
	int isNewObject = 0;
	PyArrayObject* arrayObj = obj_to_array_contiguous_allow_conversion(m_obj, array_type(m_obj), &isNewObject);

	npy_intp ofset = 0;
	for (int i = 0; i < m_dimensionCount; i++)
	{
		const int n = args[i];
		assert(n < array_size(m_obj, i));
		ofset += (n)* PyArray_STRIDES(arrayObj)[i];
	}
	return static_cast<void*>(PyArray_BYTES(arrayObj) + ofset);
}


}
