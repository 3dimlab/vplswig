//==============================================================================
/* This file is part of
*
* VPLSWIG - Voxel Processing Library
* Copyright 2017 3Dim Laboratory s.r.o.
* All rights reserved.
*
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/
#include <pycpp/swigType.h>

namespace pycpp
{
SwigType::SwigType(const std::string& className)
{
    m_className = className;
}
SwigType::SwigType(const std::string& className, const std::string& type1)
{
    m_className = className;
    m_types.push_back(type1);
}

SwigType::SwigType(const std::string& className, const std::string& type1, const std::string& type2)
{
    m_className = className;
    m_types.push_back(type1);
    m_types.push_back(type2);
}
SwigType::SwigType(const std::string& className, const std::string& type1, const std::string& type2, const std::string& type3)
{
    m_className = className;
    m_types.push_back(type1);
    m_types.push_back(type2);
    m_types.push_back(type3);

}

std::string SwigType::typeString()
{
    std::string ret = m_className;
    if (!m_types.empty())
    {
        ret += "<";
    }

    for (std::vector<std::string>::iterator it = m_types.begin(); it != m_types.end(); ++it)
    {
        ret += *it;
        if (std::next(it) != m_types.end())
        {
            ret += ", ";
        }
    }
    if (!m_types.empty())
    {
        ret += ">";
    }
    ret += " *";
    return ret;
}
}
