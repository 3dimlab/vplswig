//==============================================================================
/* This file is part of
*
* VPLSWIG - Voxel Processing Library
* Copyright 2017 3Dim Laboratory s.r.o.
* All rights reserved.
*
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/

#ifdef _DEBUG
#undef _DEBUG
#include <python.h>
#define _DEBUG
#else
#include <Python.h>
#endif
#ifdef _MSC_VER
#define PYTHON_INTERPRETER_NO_DEBUG
#endif

#include <pycpp/pyInitializer.h>

namespace pycpp
{
PyInitializer::PyInitializer()
= default;

void PyInitializer::init(const int argc, char** argv, const std::string& pythonInterpretPath)
{
    this->m_pythonInterpretPath = pythonInterpretPath;
    initialize();
    wchar_t* wargv[3];
    for (int i = 0; i < argc; i++)
    {
        size_t *t = static_cast<size_t*>(malloc(sizeof(size_t))); ;
        *t = strlen(argv[i]);
        wargv[i] = Py_DecodeLocale(argv[i], t);
        free(t);
    }
    Py_SetProgramName(wargv[0]);
    PySys_SetArgv(argc, wargv);
}

void PyInitializer::init(const std::string& pythonInterpretPath)
{
    this->m_pythonInterpretPath = pythonInterpretPath;
    initialize();
}

void PyInitializer::initialize() const
{
    if (!m_pythonInterpretPath.empty())
    {
        size_t size = m_pythonInterpretPath.length();
        size_t * psize = &size;
        Py_SetPythonHome(Py_DecodeLocale(m_pythonInterpretPath.c_str(), psize));

    }
    Py_Initialize();
}

void PyInitializer::destruct() const
{
    Py_Finalize();
}

PyInitializer::~PyInitializer()
{
    Py_Finalize();
}
}