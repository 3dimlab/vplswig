//==============================================================================
/* This file is part of
*
* VPLSWIG - Voxel Processing Library
* Copyright 2017 3Dim Laboratory s.r.o.
* All rights reserved.
*
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/
#include <iostream>
#include <sstream>

#ifdef _DEBUG
#undef _DEBUG
#include <python.h>
#define _DEBUG
#else
#include <Python.h>
#endif

#ifdef _MSC_VER
#define PYTHON_INTERPRETER_NO_DEBUG
#endif
#include <pycpp/definitions.h>

namespace pycpp
{

// functions to convert complex python data structures to their C++ 
// counterparts
static void makeListFromTuple(PyObject* tuple, tListStrings& out);
static void makeListFromList(PyObject* list, tListStrings& out);
static void makeMapFromDict(PyObject* dict, tMapStrings& out);
static std::string toString(PyObject* val);

PythonException::PythonException(const std::string& m)
{
    message = m;
}

PythonException::~PythonException() noexcept
= default;

const char*
PythonException::what() noexcept
{
    return message.c_str();
}

// local functions
void raiseFuncFailedException(const std::string& funcName)
{
    std::ostringstream oss;
    oss << "Call to function <" << funcName << "> failed.";
    throw PythonException(oss.str());
}

void makeListFromTuple(PyObject* tuple, tListStrings& out)
{
    if (tuple)
    {
        const int size = PyTuple_Size(tuple);
        for (int i = 0; i < size; i++)
        {
            PyObject* val = PyTuple_GetItem(tuple, i);
            out.push_back(toString(val));
        }
    }
}

void makeListFromList(PyObject* list, tListStrings& out)
{
    if (list)
    {
        const int size = PyList_Size(list);
        for (int i = 0; i < size; i++)
        {
            PyObject* val = PyList_GetItem(list, i);
            out.push_back(toString(val));
        }
    }
}

void makeMapFromDict(PyObject* dict, tMapStrings& out)
{
    if (dict)
    {
        PyObject* key = nullptr;
        PyObject* value = nullptr;

        Py_ssize_t pos = 0;
        while (PyDict_Next(dict, &pos, &key, &value))
        {
            out[toString(key)] = toString(value);
        }
    }
}

std::string toString(PyObject* val)
{
    if (val)
    {
        std::ostringstream oss;
        if (PyLong_Check(val))
            oss << PyLong_AsLong(val);
        else if (PyLong_Check(val))
            oss << PyLong_AsLong(val);
        else if (PyFloat_Check(val))
            oss << PyFloat_AsDouble(val);
        else if (PyUnicode_Check(val))
            oss << '\"' << PyUnicode_AsEncodedString(val, "utf-8", "Error ~") << '\"';
        else if (PyTuple_Check(val))
        {
            oss << '(';
            tListStrings ret;
            makeListFromTuple(val, ret);
            const size_t sz = ret.size();
            for (size_t i = 0; i < sz; i++)
            {
                oss << ret[i];
                if (i != (sz - 1))
                    oss << ',';
            }
            oss << ')';
        }
        else if (PyList_Check(val))
        {
            oss << '[';
            tListStrings ret;
            makeListFromList(val, ret);
            const size_t sz = ret.size();
            for (size_t i = 0; i < sz; i++)
            {
                oss << ret[i];
                if (i != (sz - 1))
                    oss << ',';
            }
            oss << ']';
        }
        else if (PyDict_Check(val))
        {
            oss << '{';
            tMapStrings ret;
            makeMapFromDict(val, ret);
            const size_t sz = ret.size();
            size_t i = 0;
            for (tMapStrings::const_iterator it = ret.begin(); it != ret.end(); ++it)
            {
                oss << it->first << ':' << it->second;
                if (i != (sz - 1))
                    oss << ',';
                i++;
            }
            oss << '}';
        }
        else
            return "";
        return oss.str();
    }
    return "";
}

swig_type_info* SwigTypePointer::GetSwigType(const std::string& type)
{
    swig_type_info* retSwigType = nullptr;
    try
    {
        retSwigType = SWIG_TypeQuery(type.c_str());
    }
    catch (...) {}
    return retSwigType;
}



}