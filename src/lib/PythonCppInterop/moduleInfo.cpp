//==============================================================================
/* This file is part of
*
* VPLSWIG - Voxel Processing Library
* Copyright 2017 3Dim Laboratory s.r.o.
* All rights reserved.
*
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/
#include <fstream>
#include <pycpp/moduleInfo.h>
#include <string>
#include <cctype>
namespace pycpp
{
ModuleInfo::ModuleInfo(const std::string& filePath, const std::string& infoTag)
{
    this->filePath = filePath;
    this->infoTag = infoTag;
    parseFile();
}

void ModuleInfo::parseFile()
{
    std::ifstream infile(filePath);

    std::string line;
    bool isParsingScriptInfo = true;
    std::string tempInfo;
    bool isInfoBlock = false;
    while (std::getline(infile, line))
    {
        if(isInfoBlock)
        {
            removeSpaceBefore(line);
            if(checkLineWithCommentTag(line, false))
            {
                isInfoBlock = false;
                if(isParsingScriptInfo)
                {
                    isParsingScriptInfo = false;
                    moduleDescription = tempInfo + line;

                }
            }
            tempInfo += line;

            continue;
        }
        if (isParsingScriptInfo)
        {
            if (checkLineWithCommentTag(line, true))
            {
                if (checkLineWithCommentTag(line,false))
                {
                    isInfoBlock = false;
                    moduleDescription = line;
                    isParsingScriptInfo = false;
                }
                else
                {
                    isInfoBlock = true;
                    removeSpaceBefore(line);
                    tempInfo = line;
                }
            }
        }
        else
        {
            if (checkLineWithCommentTag(line, true))
            {
                if (!checkLineWithCommentTag(line,false))
                {
                    isInfoBlock = true;
                }
                removeSpaceBefore(line);
                tempInfo = line;
                continue;
            }


            if (line.compare(0, 4, "def ") == 0)
            {
                FunctionInfo functionInfo;
                functionInfo.declaration = line.substr(4, line.find(')') - 3);
                functionInfo.description = tempInfo;
                functions.push_back(functionInfo);
            }
            tempInfo = "";
        }
    }
}
void ModuleInfo::removeSpaceBefore(std::string& line) const
{
    while(!line.empty())
    {
        if(line.at(0) == ' ' | line.at(0) == '\t')
        {
            line.erase(line.begin());
        }
        else
        {
            return;
        }

    }

}
bool ModuleInfo::checkLineWithCommentTag(std::string& line, bool isOpeningTag) const
{
    const std::size_t found = line.find(R"(""")");
    if (found != std::string::npos)
    {
        if(isOpeningTag)
        {
            line = line.substr(found+3);
        }
        else
        {
            line = line.substr(0, found);
        }
        return true;
    }
    return false;
}
}