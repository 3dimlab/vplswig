//==============================================================================
/* This file is part of
*
* VPLSWIG - Voxel Processing Library
* Copyright 2017 3Dim Laboratory s.r.o.
* All rights reserved.
*
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/
#include <iostream>
#include <sstream>

#define STR_EXPAND(tok) #tok
#define DEFINE2STRING(tok) STR_EXPAND(tok)

#ifdef _DEBUG
#undef _DEBUG
#include <python.h>
#define _DEBUG
#else
#include <Python.h>
#endif

#ifdef _MSC_VER
#define PYTHON_INTERPRETER_NO_DEBUG
#endif

#include <pycpp/pyRuntime.h>
#include <pycpp/definitions.h>
namespace pycpp
{
;

//==============================================================================
/*
* Functions to convert complex python data structures to their C++ equivalents.
*/

static void makeListFromPyTuple(PyObject* tuple, tListStrings& out);
static void makeListFromPyList(PyObject* list, tListStrings& out);
static void makeMapFromPyDict(PyObject* dict, tMapStrings& out);
static std::string makeStringFromPyObject(PyObject* val);



void PyRuntime::init(const int argc, char** argv, const std::string& pythonInterpretPath)
{
    m_pyInitializer.init(argc, argv, pythonInterpretPath);
}

void PyRuntime::init(const std::string& pythonInterpretPath)
{
    m_pyInitializer.init(pythonInterpretPath);
}

PyRuntime::~PyRuntime()
{
    std::map<std::string, PyObject*>::iterator it = m_modules.begin();
	const std::map<std::string, PyObject*>::iterator end = m_modules.end();
    while (it != end)
    {
        Py_DECREF(it->second);
        ++it;
    }
}

void PyRuntime::destruct()
{
    std::map<std::string, PyObject*>::iterator it = m_modules.begin();
    while (it != m_modules.end())
    {
        Py_DECREF(it->second);
        ++it;
    }
    m_modules.clear();
    m_pyInitializer.destruct();
}

bool PyRuntime::runString(const std::string& code)
{
	const int ret = PyRun_SimpleString(code.c_str());
	if (ret)
	{
		m_error.head = "There was a error in the script.";
		saveError();
		return false;
	}
	return true;
}

bool PyRuntime::runFile(const std::string& fileName)
{
    std::string f = fileName + ".py";

    Py_BuildValue("s", f.c_str());
    FILE *file = _Py_fopen(f.c_str(), "r+");
    if (file != nullptr) {
	    const int ret = PyRun_SimpleFile(file, f.c_str());
        if (ret)
        {
			m_error.head = "There was a error in the script.";
			saveError();
			return false;
        }
    }
	return true;
}

void PyRuntime::addArgument(PyObject* argument)
{
    if (argument != nullptr)
    {
        m_arguments.push_back(argument);
        Py_XINCREF(argument);
    }
}
void PyRuntime::addArgument(const std::string& argument)
{
    PyObject* pyArgument = PyUnicode_FromString(argument.c_str());
    m_arguments.push_back(pyArgument);
}

void PyRuntime::addArgument(const long argument)
{
    PyObject* pyArgument = PyLong_FromLong(argument);
    m_arguments.push_back(pyArgument);
}
void PyRuntime::addArgument(const int argument)
{
	PyObject* pyArgument = PyLong_FromLong(argument);
	m_arguments.push_back(pyArgument);
}
void PyRuntime::addArgument(const double argument)
{
    PyObject* pyArgument = PyFloat_FromDouble(argument);
    m_arguments.push_back(pyArgument);
}


bool PyRuntime::loadModules(tListStrings modules)
{
	for (std::vector<std::string>::const_iterator it = modules.begin(); it != modules.end(); ++it)
    {
        if(!load(*it))
        {
            return false;
        }
    }
    return true;
}



bool PyRuntime::load(const std::string& moduleName)
{
	PyObject *pName = PyUnicode_DecodeFSDefault(moduleName.c_str());
    if (pName == nullptr)
    {
	    saveError();
	    return false;
    }
	PyObject *pModule = PyImport_Import(pName);
	Py_DECREF(pName);

	if (pModule != nullptr) {
        m_modules[moduleName] = pModule;
		return true;;
	}
	saveError();
	return false;
}

bool PyRuntime::call(const std::string& moduleName, const std::string& funcName)
{
	PyObject* retValue = nullptr;
	if (!makeCall(moduleName, funcName, &retValue))
	{
		m_error.head = "Call method failed";
		return false;
	}

	if (retValue)
	{
		Py_DECREF(retValue);
	}
	return true;
}

void PyRuntime::addModulePath(const std::string& path) const
{
	PyObject* sysPath = PySys_GetObject(static_cast<char*>("path"));
	PyObject* modulePath = PyString_FromString(path.c_str());
	PyList_Append(sysPath, modulePath);
	Py_DECREF(modulePath);
}

bool PyRuntime::call(const std::string& moduleName, const std::string& funcName, long& retData)
{
	PyObject* retValue = nullptr;
	if (!makeCall(moduleName, funcName,&retValue))
	{
        m_error.head = "Call method failed";
		return false;
	}

    if (retValue)
    {
        if (!PyInt_Check(retValue))
        {
            Py_DECREF(retValue);
			m_error.head = "Call method failed";
			m_error.body = "Not an integer value in return.";
			return false;
        }
        retData = PyInt_AsLong(retValue);
        Py_DECREF(retValue);
    }
	return true;
}

bool PyRuntime::call(const std::string& moduleName, const std::string& funcName, double& retData)
{
	PyObject* retValue = nullptr;
	if (!makeCall(moduleName, funcName, &retValue))
	{
		m_error.head = "Call method failed";
		return false;
	}
    if (retValue)
    {
        if (!PyFloat_Check(retValue))
        {
            Py_DECREF(retValue);
			m_error.head = "Call method failed";
			m_error.body = "Not a real value in return.";
			return false;
        }
        retData = PyFloat_AsDouble(retValue);
        Py_DECREF(retValue);
    }
	return true;
}

ErrorMessage PyRuntime::getError()
{
	ErrorMessage retMessage = m_error;
	m_error.clear();
	return retMessage;
}

void PyRuntime::saveError()
{
	PyObject *type = nullptr;
	PyObject *value = nullptr;
	PyObject *traceback = nullptr;
	if (PyErr_Occurred())
	{
		PyObject *pyErrorString, *pyErrorUnicode;

		PyErr_Fetch(&type, &value, &traceback);
		PyErr_NormalizeException(&type, &value, &traceback);

		if ((type != nullptr))
		{
			pyErrorString = PyObject_Str(type);
			pyErrorUnicode = PyUnicode_FromObject(pyErrorString);

			std::wstring ws(PyUnicode_AS_UNICODE(pyErrorUnicode));
			m_error.type = std::string(ws.begin(), ws.end());
		}

		if (value != nullptr)
		{
			pyErrorString = PyObject_Str(value);
			pyErrorUnicode = PyUnicode_FromObject(pyErrorString);

			std::wstring ws(PyUnicode_AS_UNICODE(pyErrorUnicode));
			m_error.body = std::string(ws.begin(), ws.end());
		}

		if (traceback != nullptr)
		{
			pyErrorString = PyObject_Str(traceback);
			pyErrorUnicode = PyUnicode_FromObject(pyErrorString);

			std::wstring ws(PyUnicode_AS_UNICODE(pyErrorUnicode));
			m_error.traceback = std::string(ws.begin(), ws.end());
		}


		Py_XDECREF(type);
		Py_XDECREF(value);
		Py_XDECREF(traceback);
	}
}

bool PyRuntime::call(const std::string& moduleName, const std::string& funcName, std::string& retData)
{
	PyObject* retValue = nullptr;
	if (!makeCall(moduleName, funcName, &retValue))
	{
		m_error.head = "Call method failed";
		return false;
	}

    if (retValue)
    {
        if (!PyUnicode_Check(retValue))
        {
            Py_DECREF(retValue);
			m_error.head = "Call method failed";
			m_error.body = "Not a string value in return.";
			return false;
        }
        PyObject * ascii_mystring = PyUnicode_AsASCIIString(retValue);
        retData = PyBytes_AsString(ascii_mystring);

        Py_DECREF(retValue);
    }
	return true;
}

bool PyRuntime::call(const std::string& moduleName, const std::string& funcName, PyObject** retData)
{
	
	if (!makeCall(moduleName, funcName, retData))
	{
		m_error.head = "Call method failed";
		return false;
	}
	
	return true;
}

bool PyRuntime::call(const std::string& moduleName, const std::string& funcName, tListStrings& retData)
{
	PyObject* retValue = nullptr;
	if (!makeCall(moduleName, funcName, &retValue))
	{
		m_error.head = "Call method failed";
		return false;
	}
    if (retValue)
    {
        if (PyTuple_Check(retValue))
            makeListFromPyTuple(retValue, retData);
        else if (PyList_Check(retValue))
            makeListFromPyList(retValue, retData);
        else
        {
			m_error.head = "Call method failed";
			m_error.body = "Not a tuple or list in return.";
            Py_DECREF(retValue);
			return false;
        }
        Py_DECREF(retValue);
    }
	return true;
}
bool PyRuntime::call(const std::string& moduleName,const std::string& funcName, tMapStrings& retData)
{
	PyObject* retValue = nullptr;
	if (!makeCall(moduleName, funcName,&retValue))
	{
		m_error.head = "Call method failed";
		return false;
	}
    if (retValue)
    {
        if (!PyDict_Check(retValue))
        {
            Py_DECREF(retValue);
			m_error.head = "Call method failed";
			m_error.body = "Not a dictionary object in return.";
        }
        makeMapFromPyDict(retValue, retData);
        Py_DECREF(retValue);
    }
	return true;
}

PyRuntime::PyRuntime()
= default;

// private functions
bool PyRuntime::getFunctionObject(const std::string& moduleName, const std::string& funcName, PyObject** retFunction)
{
	std::ostringstream oss;

	if (m_modules.find(moduleName) == m_modules.end())
	{
		oss << "No module<" << moduleName << "> loaded.";
		m_error.body = oss.str();

		return false;
	}

    if (m_modules[moduleName] != nullptr)
    {
        PyObject* pModule = PyImport_ReloadModule(m_modules[moduleName]);
        if (pModule == nullptr) {
            saveError();
            return false;
        }
        m_modules[moduleName] = pModule;

    }
	
	
	*retFunction = PyObject_GetAttrString(m_modules[moduleName], const_cast<char*>(funcName.c_str()));
	if (!retFunction || !PyCallable_Check(*retFunction))
    {
        oss << '<' << funcName << "> is not a function or is not callable.";
		m_error.body = oss.str();
		return false;
    }
    return true;
}


PyObject* PyRuntime::createArgsTuple()
{
	const size_t size = m_arguments.size();
	if (!size)
	{
		return nullptr;
	}
	PyObject* tuple = PyTuple_New(size);
    size_t i = 0;
	for (std::vector<PyObject*>::const_iterator it = m_arguments.begin(); it != m_arguments.end(); ++it)
    {
		PyTuple_SetItem(tuple, i++, *it);
	}
	m_arguments.clear();
    return tuple;
}

bool PyRuntime::makeCall(const std::string& moduleName, const std::string& funcName, PyObject** retObject)
{
    PyObject* func = nullptr;
    const std::string function = funcName.substr(0, funcName.find('('));
    if (!getFunctionObject(moduleName, function, &func))
    {
        m_arguments.clear();
        return false;
    }

    PyObject* argsTuple = createArgsTuple();
    Py_XINCREF(argsTuple);


    *retObject = PyObject_CallObject(func, argsTuple);
    const Py_ssize_t count = PyTuple_Size(argsTuple);
    for(Py_ssize_t i = 0; i < count; i++)
    {
        Py_XDECREF(PyTuple_GetItem(argsTuple, i));
    }
    if (*retObject == nullptr)
    {
        saveError();
        return false;
    }
    if (*retObject == Py_None)
    {
        *retObject = nullptr;
    }
    Py_XDECREF(argsTuple);
    Py_DECREF(func);
    return true;
}


void makeListFromPyTuple(PyObject* tuple, tListStrings& out)
{
    if (tuple)
    {
	    const int size = PyTuple_Size(tuple);
        for (int i = 0; i < size; i++)
        {
            PyObject* val = PyTuple_GetItem(tuple, i);
			out.push_back(makeStringFromPyObject(val));
        }
    }
}

void makeListFromPyList(PyObject* list, tListStrings& out)
{
    if (list)
    {
	    const int size = PyList_Size(list);
        for (int i = 0; i < size; i++)
        {
            PyObject* val = PyList_GetItem(list, i);
			out.push_back(makeStringFromPyObject(val));
        }
    }
}

void makeMapFromPyDict(PyObject* dict, tMapStrings& out)
{
    if (dict)
    {
        PyObject* key = nullptr;
        PyObject* value = nullptr;

        Py_ssize_t pos = 0;
        while (PyDict_Next(dict, &pos, &key, &value))
        {
			out[makeStringFromPyObject(key)] = makeStringFromPyObject(value);
        }
    }
}

std::string makeStringFromPyObject(PyObject* val)
{
    if (val)
    {
        std::ostringstream oss;
        if (PyLong_Check(val))
            oss << PyLong_AsLong(val);
        else if (PyLong_Check(val))
            oss << PyLong_AsLong(val);
        else if (PyFloat_Check(val))
            oss << PyFloat_AsDouble(val);
        else if (PyUnicode_Check(val))
            oss << '\"' << PyUnicode_AsEncodedString(val, "utf-8", "Error ~") << '\"';
        else if (PyTuple_Check(val))
        {
            oss << '(';
            tListStrings ret;
            makeListFromPyTuple(val, ret);
            const size_t sz = ret.size();
            for (size_t i = 0; i < sz; i++)
            {
                oss << ret[i];
                if (i != (sz - 1))
                    oss << ',';
            }
            oss << ')';
        }
        else if (PyList_Check(val))
        {
            oss << '[';
            tListStrings ret;
            makeListFromPyList(val, ret);
	        const size_t sz = ret.size();
            for (size_t i = 0; i < sz; i++)
            {
                oss << ret[i];
                if (i != (sz - 1))
                    oss << ',';
            }
            oss << ']';
        }
        else if (PyDict_Check(val))
        {
            oss << '{';
            tMapStrings ret;
            makeMapFromPyDict(val, ret);
	        const size_t sz = ret.size();
            size_t i = 0;
	        for (tMapStrings::const_iterator it = ret.begin(); it != ret.end(); ++it)
            {
                oss << it->first << ':' << it->second;
                if (i != (sz - 1))
                    oss << ',';
                i++;
            }
            oss << '}';
        }
        else
            return "";
        return oss.str();
    }
    else
        return "";
}

}