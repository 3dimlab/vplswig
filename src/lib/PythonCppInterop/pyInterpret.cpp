//==============================================================================
/* This file is part of
*
* VPLSWIG - Voxel Processing Library
* Copyright 2017 3Dim Laboratory s.r.o.
* All rights reserved.
*
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/

#include <pycpp/pyInterpret.h>

namespace pycpp
{
PyInterpret::PyInterpret()
{
    m_isInitialized = false;
}

PyInterpret::~PyInterpret()
= default;

bool PyInterpret::open(const std::string& pythonPath)
{
    if (!m_isInitialized)
    {
        try
        {
            init(pythonPath);
            m_isInitialized = true;
        }
        catch (...)
        {
            return false;
        }
    }
    return m_isInitialized;
}

bool PyInterpret::isOpen() const
{
    return m_isInitialized;
}

bool PyInterpret::loadModule(const std::string& directoryPath, const std::string& name)
{
    if (m_isInitialized)
    {
        addModulePath(directoryPath);
        return load(name);
    }
    return false;
}

ErrorMessage PyInterpret::errorMessage()
{
    if (m_isInitialized)
    {
        return getError();
    }
    return ErrorMessage();
}

std::string PyInterpret::errorString()
{
    std::string ret;
    if (m_isInitialized)
    {
        const ErrorMessage error = getError();
        ret = error.head + ":: " + error.body;
    }
    return ret;
}

void PyInterpret::close()
{
    if (m_isInitialized)
    {
        destruct();
        m_isInitialized = false;
    }
}
}