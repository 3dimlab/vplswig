//==============================================================================
/* This file is part of
*
* VPLSWIG - Voxel Processing Library
* Copyright 2017 3Dim Laboratory s.r.o.
* All rights reserved.
*
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/
#ifdef _DEBUG
#undef _DEBUG
#include <python.h>
#define _DEBUG
#else
#include <python.h>
#endif

#include <pycpp/pyInterpret.h>
#include <iostream>
#include "pycpp/swigType.h"

#include <VPL/Image/DensityVolume.h>
#include <VPL/ImageIO/DicomDirLoader.h>
#include "pycpp/pyInterpret.h"
//#define WORK_NOTEBOOK_PATH
#define CUSTOM_INTERPRET

#include "pycpp/numpyType.h"
#include "pycpp/systemTypes.h"
#include <pycpp/moduleInfo.h>

using pycpp::PyInterpret;

namespace settings
{
std::string vplSwigVersion = "180913";

#ifdef CUSTOM_INTERPRET
std::string interpretPath = "X:/msvc14/x64/Python 3.7.0/interpret";
#elif
std::string interpretPath = "";
#endif
#ifdef WORK_NOTEBOOK_PATH
std::string dataDir = "C:/Users/melcer/Desktop/SyncResillio/Data/";
std::string vplPythonDir = "C:/Users/melcer/3Dim/app/VPLPython/";
#else
std::string dataDir = "D:/3Dim/Resillio/MySyncData/Data/";
std::string vplPythonDir = "D:/app/VPLPython/";
#endif

std::string scriptName = "example";

std::string vplSwigPath = vplPythonDir + "VPLSwig " + vplSwigVersion;
std::string scriptPath = vplPythonDir + "PythonCppInterop "+ vplSwigVersion +"/bin/scripts";

std::string data = dataDir + "AssKull/ASS000/ASS000";
//std::string data = dataDir + "VPLTestData";
//std::string data = dataDir + "ASTibia/AST001L/AST001L/anyfolder";
std::string msegPath = dataDir + "AssKullBad/ASS000/ASS000.mseg";

std::string numpyFile = dataDir + "3Dim_ASSkull-sample_converted/ASS000/orig.npy";
    
} // namespace settings


//#define WITH_INTERPRET


void function1(PyInterpret& interpret)
{
    /*vpl::img::CDensityVolume volume(100, 150, 200);
    SwigType type("vpl::img::CDensityVolume");

    vpl::img::CDicomSlicePtr slice(new vpl::img::CDicomSlice(100, 100, 100));
    SwigType sliceType("vpl::img::CDicomSlice");
    sliceType.class2PyObject<vpl::img::CDicomSlice *>(&*slice);
    //interpret.addArgument(sliceType.class2PyObject<vpl::img::CDicomSlice *>(&*slice));


    interpret.addArgument(settings::data);*/

	pycpp::NumpyType numpy;

	numpy.create<pycpp::tPixel32>(5, 10, 15, 5, 3, 1);
	numpy.setValue(123456, 0, 14, 4, 2, 0);
	interpret.addArgument(numpy.pyObject()); 
	
    PyObject* ret;
    if (interpret.call(settings::scriptName, "function1", &ret))
    {
        if (ret == nullptr)
        {
            std::cerr << "Function not return data" << std::endl;
            return;
        }


		pycpp::tPixel32 value;
    	numpy.getValue<pycpp::tPixel32>(&value, 0, 14, 4, 2, 0);
		std::cout << "Value: " << value << std::endl;


		pycpp::NumpyType numpy2;
		numpy2.create(ret);

		
		
		double dvalue;
		switch (numpy2.type())
		{

		case pycpp::UINT32: 
			numpy2.getValue<pycpp::tPixel32>(&value,0, 1);
			std::cout << "Value: " << value << std::endl;
			break;
		case pycpp::DOUBLE:
			numpy2.getValue<double>(&dvalue, 0, 1);
			std::cout << "Value: " << dvalue << std::endl;
			break;
		default:
			break;
		}

        /*std::cout << "before: " << volume.getXSize() << " " << volume.getYSize() << " " << volume.getZSize() << std::endl;


        const vpl::img::CDicomSlicePtr slice(sliceType.pyObject2Class<vpl::img::CDicomSlice *>(ret));*/

        //const vpl::img::CDensityVolumePtr spFiltered(type.pyObject2Class<vpl::img::CDensityVolume *>(ret));
        //std::cout << "after: " << spFiltered->getXSize() << " " << spFiltered->getYSize() << " " << spFiltered->getZSize() << std::endl;
    }
    else
    {
        std::cerr << "Failed run function - " << interpret.errorString() << std::endl;
    }
}

void loadDicom()
{
    std::cout << "Load dicom" << std::endl;
    vpl::img::CDicomDirLoader loader;
    std::cout << "Loader: " << loader.loadDicomDir(settings::data) << std::endl;

    vpl::img::CDensityVolume volume;
    std::cout << "make: " <<  loader.makeDensityVolume(volume) << std::endl;

}

void importNumpy(PyInterpret& interpret)
{
	interpret.addArgument(settings::numpyFile);
	PyObject* ret;
	if (interpret.call(settings::scriptName, "importNumpy", &ret))
	{
		pycpp::NumpyType numpy;
		numpy.create(ret);
		int xSize = numpy.getDimSize(0);
		int ySize = numpy.getDimSize(1);
		int zSize = numpy.getDimSize(2);

		vpl::img::CDensityVolume newVolume(xSize, ySize, zSize);
		time_t begin, end;
		time(&begin);
#pragma omp parallel for schedule(static) default(shared)

		for (int z = 0; z < zSize; z++)
		{
			for (int y = 0; y < ySize; y++)
			{
				for (int x = 0; x < xSize; x++)
				{
					vpl::img::tDensityPixel value = numpy.getValue<vpl::img::tDensityPixel>(x, y, z);
					newVolume.set(x, y, z, value);
				}
			}
		}
		time(&end);
		const double difference = difftime(end, begin);
		printf("time taken for function() %.2lf seconds.\n", difference);
	}
	else
	{
		std::cerr << interpret.errorString() << std::endl;
	}
}

void deserialize()
{
    /*std::cout << "Reading mseg" << std::endl;
    vpl::mod::CFileChannel channel(vpl::mod::CH_IN, settings::msegPath);
    if(!channel.isConnected())
    {
        std::cerr << "Failed found mseg" << settings::msegPath << std::endl;
        return;
    }
    vpl::img::CVolume32 volume;
    vpl::mod::read<vpl::img::CVolume32>(volume, channel);
    std::cout << volume.at(0, 0, 0) << std::endl;
    channel.disconnect();
    int xSize = volume.getXSize();
    int ySize = volume.getYSize();
    int zSize = volume.getZSize();


    for (int x = 0; x < xSize; ++x)
    {
        for (int y = 0; y < ySize; ++y)
        {
            for (int z = 0; z < zSize; ++z)
            {
              
                vpl::img::tPixel32 value = volume.at(x, y, z);
                if (value & (value - 1))
                {
                    std::cerr << "More values per pixels"  << std::endl;
                    return;
                }
                if(value == 0)
                {
                    int data = 0;
                    continue;
                }
                unsigned i = 1;
                int pos = 1;
                // Iterate through bits of n till we find a set bit
                // i&n will be non-zero only when 'i' and 'n' have a set bit
                // at same position
                while (!(i & value))
                {
                    // Unset current bit and set the next bit in 'i'
                    i = i << 1;
                    // increment position
                    ++pos;
                }

               
                int data = pos;
            }
        }
    }*/
}

void numpyTest()
{
	pycpp::NumpyType numpy;

	numpy.create<pycpp::tPixel32>(5, 10, 15, 5, 3, 1);
	numpy.setValue(123456, 0, 14, 4, 2, 0);
	pycpp::tPixel32 value;
	numpy.getValue<pycpp::tPixel32>(&value,0, 14, 4, 2, 0);
	std::cout << "Value: " << value << std::endl;
}


int main(int argc, char** argv)
{
    try
    {
        pycpp::ModuleInfo moduleInfo("C:/Users/melcer/3Dim/Development/devel/plugins/PythonScripting/scripts/pythonScripting.py");
        //loadDicom();
    	//deserialize();


    	/*PyInterpret interpret;
      
        if (interpret.open(settings::interpretPath))
        {
            std::cout << "Initialized interpret." << std::endl;

			//numpyTest();

           /* if (!interpret.loadModule(settings::vplSwigPath, "VPLSwig.Core.Core"))
            {
                std::cerr << "Failed load module VPLSwig.Core.Core" << std::endl;
                system("pause");
                return -1;
            }
            if (!interpret.loadModule(settings::vplSwigPath, "VPLSwig.Volume.Volume"))
            {
                std::cerr << "Failed load module VPLSwig.Volume.Volume" << std::endl;
                system("pause");
                return -1;
            }*/

            /*if (!interpret.loadModule(settings::vplSwigPath, "VPLSwig.Image.Image"))
            {
                std::cerr << "Failed load module VPLSwig.Image.Image" << std::endl;
                system("pause");
                return -1;
            }*/
           /* if (!interpret.loadModule(settings::scriptPath, settings::scriptName))
            {
                std::cerr << "Failed load module example" << std::endl;
                const pycpp::ErrorMessage error = interpret.errorMessage();
                std::cerr << error.head << error.body << std::endl;
                system("pause");
                return -1;
            }

            //function1(interpret);
			importNumpy(interpret);

        }
        else
        {
            std::cerr << "Failed initialize interpret." << std::endl;
        }*/

    }
    catch(std::exception& e)
    {
        std::cerr << "exception caught: " << e.what() << std::endl;
    }
    system("pause");
    return 0;
}