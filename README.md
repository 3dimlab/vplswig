    **********************************************************************
    * VPLswig - SWIG-based VPL bindings for Python
    * Copyright 2015-2017 3Dim Laboratory s.r.o.
    * All rights reserved.
    * 
    * Use of this file is governed by a BSD-style license that can be
    * found in the LICENSE file.
    * 
    **********************************************************************

Table of Contents

1. What is VPLswig?
2. Brief Guide
3. Installation
4. Bug Reporting


1 What is VPLswig?
==================

VPL (Voxel Processing Library) is an open source collection of 2D/3D image 
processing tools originally aimed at medical images. It contains number of 
routines for volumetric data processing like 3D filtering, edge detection, 
segmentation, etc.

With VPLswig one can benefit from the power of VPL from a higher-level
language like Python.  

---

2 Brief Guide
=============

This repository contains SWIG related code and tools and allows you to generate
Python API for basic VPL functionality. 

2.1 Python API
-------------

Here is the list of VPL features and classes for which the Python API is generated:

- CChannel
- CRGBA, CCOlor, CRange, C_Pixel
- CCoordinates, CPoint, CSize, CVector
- loadColorPNG, loadGrayPNG, saveColorPNG, saveGrayPNG
- CImage, CDicomDirLoader, CDicomSlice
- CDicomSliceLoader, CSlice, CSerializable, dicomSlice
- CVolume, CVolumeAnisotropicFilter, CVolumeGaussFilter
- CAvg_Filter, CCanny, CGaussFilter
- CZeroCrossDetector, CFullHistogram, CHistogram
- lot of function like getProduct

2.2 Python API Examples
-----------------------

```
#!python
from VPLSwig.Image import Image
from VPLSwig.Image import ImageFilters
from VPLSwig.Core import Channel
from VPLSwig.Image import IO

channelIn = Channel.CFileChannel(Channel.CH_IN, "data/inImage.png")
channelOut = Channel.CFileChannel(Channel.CH_OUT, "data/OUTtestImage.png")

image = Image.CImage16(100,100,10)
IO.loadGrayPNG(image,channelIn)

imageB = Image.CImage16(image.getSize())

filt = ImageFilters.CAvg7Filter_Image16()
print(filt(image,imageB))

IO.saveGrayPNG(imageB,channelOut)
channelOut.disconnect()
```


---

3 Installation
==============
Please see instructions on [Wiki](https://bitbucket.org/3dimlab/vplswig/wiki).

---

4 Bug Reporting
===============

We don't promise that this software is completely bug free. If you encounter
any issue, please let us now.

- Report the bug using 
  [Bitbucket's issue tracker](https://bitbucket.org/3dimlab/vplswig/issues),
- mail your bug reports to spanel(at)3dim-laboratory.cz,

or

- fork the repository, fix it, and send us a pull request.
