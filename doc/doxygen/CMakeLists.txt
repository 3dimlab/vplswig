#==============================================================================
# This file is part of
#
# VPLswig - SWIG-based VPLswig bindings for Python
# Changes are Copyright 2015 3Dim Laboratory s.r.o.
# All rights reserved.
#
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.
#==============================================================================

FIND_PACKAGE( Doxygen )

IF( DOXYGEN_FOUND )

  # Copy and modify Doxygen configuration file 
  CONFIGURE_FILE( ${CMAKE_CURRENT_SOURCE_DIR}/CMakeLibraries.in
                  ${CMAKE_CURRENT_BINARY_DIR}/CMakeLibraries.dox
                  @ONLY IMMEDIATE )

  # Custom target for generation of doxygen documentation
  IF( NOT MSVC )
    ADD_CUSTOM_TARGET( libraries_doc ${DOXYGEN} CMakeLibraries.dox )
  ELSE( NOT MSVC )
    ADD_CUSTOM_TARGET( LIBRARIES_DOC ${DOXYGEN} CMakeLibraries.dox )
  ENDIF( NOT MSVC )


  IF( NOT MSVC )
    ADD_CUSTOM_TARGET( doc )
    ADD_DEPENDENCIES( doc libraries_doc )
  ELSE( NOT MSVC )
    ADD_CUSTOM_TARGET( ALL_DOC )
    ADD_DEPENDENCIES( ALL_DOC LIBRARIES_DOC )
  ENDIF( NOT MSVC )

ELSE( DOXYGEN_FOUND )

  MESSAGE( WARNING "Doxygen not found" )

ENDIF( DOXYGEN_FOUND )
