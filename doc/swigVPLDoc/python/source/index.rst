.. VPLSwig documentation master file, created by
   sphinx-quickstart on Sat Mar 11 20:49:02 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to VPLSwig's documentation!
===================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   generated/modules

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
