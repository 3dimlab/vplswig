VPLSwig package
===============

Subpackages
-----------

.. toctree::

    VPLSwig.Core
    VPLSwig.Image
    VPLSwig.Volume

Module contents
---------------

.. automodule:: VPLSwig
    :members:
    :undoc-members:
    :show-inheritance:
