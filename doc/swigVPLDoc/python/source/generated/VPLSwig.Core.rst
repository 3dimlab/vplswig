VPLSwig.Core package
====================

Submodules
----------

.. toctree::

   VPLSwig.Core.Channel
   VPLSwig.Core.Core
   VPLSwig.Core.Geometry

Module contents
---------------

.. automodule:: VPLSwig.Core
    :members:
    :undoc-members:
    :show-inheritance:
