//==============================================================================
/* This file is part of
*
* VPLSWIG - Voxel Processing Library
* Copyright 2017 3Dim Laboratory s.r.o.
* All rights reserved.
*
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/

#pragma once
#include <string>

#include "pyRuntime.h"
#include "errorMessage.h"

namespace pycpp
{
//! \brief Controller for initialize and comunicate with python interpret.
class PyInterpret : public PyRuntime
{
public:
    PyInterpret();
    virtual ~PyInterpret();
    //! \brief Initialize interpret.
    //! \param pythonPath Path to python interpret. 
    //! When Path is empty, then is used python from PATH in system.
    //! \return Succes - true, Fail - false
    bool open(const std::string& pythonPath = "");

    //! \brief Check if interpret is already open.
    //! \return boolean
    bool isOpen() const;
    
    //! \brief Destruct open interpret.
    void close();
                                              
    //! \brief Importing python module into interpret.
    //! \param directoryPath Directory of module.
    //! \param name Name of module
    //! \return Success - true, Fail - false
    bool loadModule(const std::string& directoryPath, const std::string& name);
    
    //! \brief Collect error about last operation from interpret.
    //! \return Error Message, which contains body, header ...
    ErrorMessage errorMessage();

    std::string errorString();
};
} // namespace pycpp
