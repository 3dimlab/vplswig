//==============================================================================
/* This file is part of
*
* VPLSWIG - Voxel Processing Library
* Copyright 2017 3Dim Laboratory s.r.o.
* All rights reserved.
*
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/

#pragma once
#include <string>
#include <map>
#include <vector>
#include <exception>
#include "swigRuntime.h"

#define STR_EXPAND(tok) #tok
#define DEFINE2STRING(tok) STR_EXPAND(tok)


namespace pycpp
{
//! Typedefs of some common objects

typedef std::map<std::string, std::string> tMapStrings;
typedef std::vector<std::string> tListStrings;


void raiseFuncFailedException(const std::string& funcName);


   /**
   * Exception thrown by Python api calls.
   */
class PythonException : public std::exception
{

public:

    PythonException(const std::string& m);
    virtual ~PythonException() noexcept;

    virtual const char* what() noexcept;

private:

    std::string message;

}; // class CPythonException

class SwigTypePointer
{
public:

    static swig_type_info* GetSwigType(const std::string& type);
private:    
    swig_type_info* m_swigType = nullptr;

};
} // namespace pycpp
