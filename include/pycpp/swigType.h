//==============================================================================
/* This file is part of
*
* VPLSWIG - Voxel Processing Library
* Copyright 2017 3Dim Laboratory s.r.o.
* All rights reserved.
*
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/

#pragma once
#include <string>
#include <vector>
#include "definitions.h"

namespace pycpp
{

struct SwigTypeError : public std::exception 
{ 
    SwigTypeError() noexcept = default;
    virtual ~SwigTypeError() noexcept = default;
    const char *what() const noexcept override { return "Failed to retrieve swig type."; } \
};
struct SwigConversionError : public std::exception
{
    SwigConversionError() noexcept = default;
    virtual ~SwigConversionError() noexcept = default;
    const char *what() const noexcept override { return "Swig failed convert to required type."; } \
};

//! \brief Wrapper on type which are exported from c++ by swig.
//! Search type in python modules. 
//! Example: SwigType("vpl::img::CDensityVolume")
class SwigType
{
public:

    //! \brief For not template classes.
    //! \param className Name of class. For example: vpl::img::CDensityVolume
    explicit SwigType(const std::string& className);

    //! \brief For template classes with 1 type.
    //! \param className Name of class. For example: vpl::img::CDensityVolume
    //! \param type1 First template type
    SwigType(const std::string& className, const std::string& type1);

    //! \brief For template classes with 1 type.
    //! \param className Name of class. For example: vpl::img::CDensityVolume
    //! \param type1 First template type
    //! \param type2 Second template type
    SwigType(const std::string& className, const std::string& type1, const std::string& type2);

    //! \brief For template classes with 1 type.
    //! \param className Name of class. For example: vpl::img::CDensityVolume
    //! \param type1 First template type
    //! \param type2 Second template type
    //! \param type3 Third template type
    SwigType(const std::string& className, const std::string& type1, const std::string& type2,
              const std::string& type3);

    

    //! \brief Convert c++ class to python PyObject
    //! \tparam T Type of c++ class
    //! \param object c++ object for convertion to PyObject
    //! \return Converted python object
    template <class T>
    PyObject* class2PyObject(T object);;

    //! \brief Convert python PyObject to c++ class
    //! \tparam T Type of c++ class
    //! \param object Python object for conversion to c++
    //! \return Converted c++ object
    template <class T>
    T pyObject2Class(PyObject* object);;

    
private:
    //! Class name
    std::string m_className;
    //! Vector of templated types
    std::vector<std::string> m_types{};

    //! Create final type string, which will be searched in swigRuntime.
    std::string typeString();

};

template <class T>
PyObject* SwigType::class2PyObject(T object)
{
    SwigTypePointer swigTypePointer;
    swig_type_info* swigType = swigTypePointer.GetSwigType(typeString());
    if (swigType != nullptr)
    {
        return SWIG_NewPointerObj(static_cast<void*>(object), swigType, 0 | 0);
    }
    throw SwigTypeError();
}

template <class T>
T SwigType::pyObject2Class(PyObject* object)
{
    void* argp1 = nullptr;
    SwigTypePointer swigTypePointer;
    swig_type_info* swigType = swigTypePointer.GetSwigType(typeString());
    if (swigType != nullptr)
    {
        const int res = SWIG_ConvertPtr(object, &argp1, swigType, 0);

        if (SWIG_IsOK(res))
        {
            return reinterpret_cast<T>(argp1);
        }
        throw SwigConversionError();
    }
    throw SwigTypeError();
}
}
