//==============================================================================
/* This file is part of
*
* VPLSWIG - Voxel Processing Library
* Copyright 2017 3Dim Laboratory s.r.o.
* All rights reserved.
*
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/

#pragma once
#include <string>
#include <map>
#include <vector>
#include "definitions.h"
#include "errorMessage.h"
#include <Python.h>
#include "pyInitializer.h"

namespace pycpp
{
//! \brief  Provide operation for low-level python calls.
class PyRuntime
{
public:
	//! When destruct is closed interpret.
	~PyRuntime();

    //! \brief Adding path directory to sysPath for importing modules.
    //! \param path Directory path, where is module.
    void addModulePath(const std::string& path) const;

    //! \brief Importing one module to opened interpret.
    //! \param moduleName Name of module for loading.
    //! \return Succes - true, Fail - false
    bool load(const std::string& moduleName);

    //! \brief Importing many modules in list to opened interpret.
    //! \param modules List of module for loading.
    //! \return Succes - true, Fail - false
    bool loadModules(tListStrings modules);




    //////////////////////////////////////////////////////////////
	//! \brief Adding argument for calling function in module.
    //! \param argument Any python object
    //! Example:
    //!     vpl::img::CDicomSlice slice;
    //!     SwigType sliceType("vpl::img::CDicomSlice");
    //!     interpret.addArgument(sliceType.class2PyObject<vpl::img::CDicomSlice *>(&slice));
    void addArgument(PyObject* argument);

	//! \brief Adding argument for calling function in module.
    //! \param argument 
    void addArgument(const std::string& argument);

	//! \brief Adding argument for calling function in module.
    //! \param argument integer
    void addArgument(long argument);

	//! \brief Adding argument for calling function in module.
	//! \param argument integer
	void addArgument(int argument);

	//! \brief Adding argument for calling function in module.
    //! \param argument double
    void addArgument(double argument);


    //! \brief Execute code from string in python interpret.
    //! \param code python script for execution.
    //! \return Succes - true, Fail - false
    bool runString(const std::string& code);

 
    //! \brief Execute code from file in python interpret.
	//! \param fileName Name of filename, directory must be added before by addModulePath().
	//! \return Succes - true, Fail - false
	bool runFile(const std::string& fileName);




 
    //! \brief Execute void function in loaded module.
	//! \param moduleName Name of module.
	//! \param funcName Name of function for execution.
	//! \return Succes - true, Fail - false
	bool call(const std::string& moduleName, const std::string& funcName);

   
    //! \brief Execute function in loaded module, with return type integer.
    //! \param moduleName Name of module.
    //! \param funcName Name of function for execution.
    //! \param retData Data from return in script.
    //! \return Succes - true, Fail - false
    bool call(const std::string& moduleName, const std::string& funcName,long& retData);


	//! \brief Execute function in loaded module, with return type PyObject.
	//! \param moduleName Name of module.
	//! \param funcName  Name of function for execution.
	//! \param retData Data from return in script.
	//! \return Succes - true, Fail - false
	bool call(const std::string& moduleName, const std::string& funcName,PyObject** retData);


	//! \brief Execute function in loaded module, with return type double.
	//! \param moduleName Name of module.
	//! \param funcName Name of function for execution.
	//! \param retData Data from return in script.
	//! \return Succes - true, Fail - false
	bool call(const std::string& moduleName, const std::string& funcName,double& retData);


    //! \brief Execute function in loaded module, with return type string.
    //! \param moduleName Name of module.
    //! \param funcName Name of function for execution.
    //! \param retData Data from return in script.
    //! \return Succes - true, Fail - false
	bool call(const std::string& moduleName, const std::string& funcName,std::string& retData);



    //! \brief Execute function in loaded module, with return std::vector<std::string>.
    //! \param moduleName Name of module.
    //! \param funcName Name of function for execution.
    //! \param retData Data from return in script.
    //! \return Succes - true, Fail - false
	bool call(const std::string& moduleName, const std::string& funcName,tListStrings& retData);


    //! \brief Execute function in loaded module, with return std::map<std::string, std::string>.
    //! \param moduleName Name of module.
    //! \param funcName Name of function for execution.
    //! \param retData Data from return in script.
    //! \return Succes - true, Fail - false
	bool call(const std::string& moduleName, const std::string& funcName,tMapStrings& retData);

protected:
    bool m_isInitialized;
    //! Base constructor.
    PyRuntime();

    //! Initialize python interpret with arguments.
    void init(int argc, char** argv, const std::string& pythonInterpretPath = "");

    //! Initialize python.
    void init(const std::string& pythonInterpretPath = "");
    //! Close interpret.
    void destruct();

    //! \brief Error about last operation from interpret.
    //! \return Error Message, which contains body, header ...
    ErrorMessage getError();

    //! \brief Generate final error message from many parts.
    void saveError();

private:


    //! \brief Retrieves a function handle from the python module.
	//! \param moduleName Name of module.
	//! \param funcName Name of function for execution.
	//! \param retFunction PyObject representing function.
	//! \return Succes - true, Fail - false
	bool getFunctionObject(const std::string& moduleName, const std::string& funcName, PyObject** retFunction);


    //! \brief Converts an tArgumentMap to a tuple of PyObjects, 
    //! so that they can be passed as arguments to call of python function.
	//! \return Python tuple.
	PyObject* createArgsTuple();

    //! \brief Execute function with any type, conversion to return type is not here.
    //! Arguments for execution convert to required python object. 
	//! \param moduleName Name of module.
	//! \param funcName Name of function for execution.
	//! \param retObject Data from return in script.
	//! \return Succes - true, Fail - false
	bool makeCall(const std::string& moduleName, const std::string& funcName,PyObject** retObject);


    //! \brief Initializer of python interpret.
    PyInitializer m_pyInitializer;

    //! \brief Loaded modules.
    std::map <std::string, PyObject*> m_modules{};

    //! \brief Arguments for calling one function.
    //! When function is execute arguments are cleared.
	std::vector<PyObject*> m_arguments{};

    //! Error collector
	ErrorMessage m_error;

}; // class PyRuntime

} // namespace pycpp

