//==============================================================================
/* This file is part of
*
* VPLSWIG - Voxel Processing Library
* Copyright 2017 3Dim Laboratory s.r.o.
* All rights reserved.
*
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/

#pragma once
#include <string>

#pragma once
namespace pycpp
{

//! Container for python arror message in any states.
//! From this parts is created final message.
class ErrorMessage
{
public:

	void clear();

	std::string head;
	std::string type;
	std::string body;
	std::string traceback;

	//! Generate final error message from many parts. 
	//! \return Concatenated error message.
	std::string operator()() const;
};

} // namespace pycpp

