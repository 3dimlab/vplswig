//==============================================================================
/* This file is part of
*
* VPLSWIG - Voxel Processing Library
* Copyright 2017 3Dim Laboratory s.r.o.
* All rights reserved.
*
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/

#pragma once
#undef slots
// Avoid having to link with the debug version of python in debug mode (e.g., python36_d.lib)
#ifdef _DEBUG
#undef _DEBUG
#include <python.h>
#define _DEBUG
#else
#include <python.h>
#endif
#define slots

#include "systemTypes.h"
#include <cstdarg>
#include <vector>
#include "Python.h"

#include <typeinfo>
#include <iostream>
#include <Python.h>

namespace pycpp
{

enum Types {
	BOOL = 0,
	INT8,UINT8,
	INT16, UINT16,
	INT32, UINT32,
	FLOAT, DOUBLE,
	STRING,
	NOTYPE
};


struct NumpyOutOfIndexException : public std::exception
{
	const char * what() const noexcept override
	{
		return "Out of index.";
	}
};

class NumpyType
{
public:
    //! Constructor
    NumpyType();

    //! Destructor, free pointer on pyObject
    ~NumpyType();

	//! Create new numpy format, with variable count of dimensions.
	template <typename T>
	bool create(int dimensionCount, ...);

	//! Create c++ type of numpy from python array.
	bool create(PyObject* object);

	//! Count of dimensions in numpy array.
	int getDimCount() const;

	//! Size of one dimension.
	int getDimSize(int dimIndex);

	//! Getter for numpy type, which is define in enum.
	Types type() const;

	template<typename T>
	static Types evaluateType();
	std::string typeToStr() const;

	//! Set value in N dimension numpy array. Throw exception when arguments is out of index.
	template<typename T>
	void setValue(T value, ...);
	//! Return value from N dimension numpy array. Throw exception when arguments is out of index.
	template<typename T>
	void getValue(T* value, ...);

	//! Return value from 1D numpy array. Throw exception when arguments is out of index.
	template <class T>
	void setValue(T value, int dim1);

	//! Set value in 1D numpy array. Throw exception when arguments is out of index.
	template <class T>
	T getValue(int dim1);

	//! Set value in 2D numpy array. Throw exception when arguments is out of index.
	template <class T>
	void setValue(T value, int dim1, int dim2);

	//! Return value from 2D numpy array. Throw exception when arguments is out of index.
	template <class T>
	T getValue(int dim1, int dim2);

	//! Set value in 3D numpy array. Throw exception when arguments is out of index.
	template <class T>
	void setValue(T value, int dim1, int dim2, int dim3);

	//! Return value from 3D numpy array. Throw exception when arguments is out of index.
	template <class T>
	T getValue(int dim1, int dim2, int dim3);

	//! Used when numpy object as interpret argument call.
	//! Example: interpret.addArgument(numpy.pyObject());
	PyObject* pyObject() const;


private:
	//! Initialize numpy
	static bool importArray();
	//! Checks if object is numpy array
	static bool isArray(PyObject* object);
	//! Create numpy array.
	bool createArray();

	//! Getter for pointer on data in numpy array.
	void* getPtr(const int* args) const;
	void* getPtr(int dim1) const;
	void* getPtr(int dim1, int dim2) const;
	void* getPtr(int dim1, int dim2, int dim3) const;

	Types m_type;
	int m_dimensionCount;
	std::vector<int> m_dimensionSizes;

	//! Reference on python object.
	PyObject * m_obj;
};


#include "numpyType.hxx"

}
