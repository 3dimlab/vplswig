//==============================================================================
/* This file is part of
*
* VPLSWIG - Voxel Processing Library
* Copyright 2017 3Dim Laboratory s.r.o.
* All rights reserved.
*
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/

#pragma once
#include <string>
#include <vector>

namespace pycpp
{
//! Initialize new python interpret.
class PyInitializer
{
    //! \brief Base constructor
    PyInitializer();

    ~PyInitializer();

    //! \brief Initialize interpret with argv.
    //! \param argc argv count
    //! \param argv argv of program
    //! \param pythonInterpretPath Path to python interpret. 
    //! When Path is empty, then is used python from PATH in system.
    void init(int argc, char** argv, const std::string& pythonInterpretPath = "");

    //! \brief Initialize interpret
    //! \param pythonInterpretPath Path to python interpret. 
    //! When Path is empty, then is used python from PATH in system.
    void init(const std::string& pythonInterpretPath = "");

    //! \brief Close initialized interpret
    void destruct() const;

    //! \brief Initialization core
    void initialize() const;

    friend class PyRuntime;
    std::string m_pythonInterpretPath;
};

} // namespace pycpp
