//==============================================================================
/* This file is part of
*
* VPLSWIG - Voxel Processing Library
* Copyright 2017 3Dim Laboratory s.r.o.
* All rights reserved.
*
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/


#pragma once
#include "systemTypes.h"
#include <cstdarg>
#include <vector>
#include <typeinfo>
#include <iostream>
#include "numpyType.h"

template <typename T>
bool NumpyType::create(int dimensionCount, ...)
{
	m_dimensionCount = dimensionCount;

	importArray();

	m_type = evaluateType<T>();

	va_list valst;

	va_start(valst, dimensionCount);

	for (int i = 0; i < m_dimensionCount; i++)
	{
		const int n = va_arg(valst, int);
		m_dimensionSizes.push_back(n);
	}

	va_end(valst);

	return createArray();
}

template<typename T>
Types NumpyType::evaluateType()
{
	const std::type_info& requiredType = typeid(T);

	if (requiredType.hash_code() == typeid(tPixel8).hash_code())
		return UINT8;
	if (requiredType.hash_code() == typeid(tPixel16).hash_code())
		return UINT16;
	if (requiredType.hash_code() == typeid(tPixel32).hash_code())
		return UINT32;
    if (requiredType.hash_code() == typeid(tInt8).hash_code())
        return INT8;
	if (requiredType.hash_code() == typeid(tDensityPixel).hash_code())
		return INT16;
    if (requiredType.hash_code() == typeid(tInt32).hash_code())
		return INT32;
	if (requiredType.hash_code() == typeid(tFloatPixel).hash_code())
		return FLOAT;
    if (requiredType.hash_code() == typeid(double).hash_code())
        return DOUBLE;
	return NOTYPE;
}

template <typename T>
void NumpyType::setValue(T value, ...)
{
	va_list valst;
	int* args = static_cast<int*>(malloc(sizeof(int) * m_dimensionCount));
	va_start(valst, value);
	for (int i = 0; i < m_dimensionCount; i++)
	{
		const int n = va_arg(valst, int);
		if (n >= m_dimensionSizes.at(i))
		{
			free(args);
			throw NumpyOutOfIndexException();
		}
		args[i] = n;
	}
	va_end(valst);

	T* data = static_cast<T *>(getPtr(args));
	*data = value;
	free(args);

}

template <typename T>
void NumpyType::getValue(T* value, ...)
{
	va_list valst;
	int* args = static_cast<int*>(malloc(sizeof(int) * m_dimensionCount));

	va_start(valst, value);
	for (int i = 0; i < m_dimensionCount; i++)
	{
		const int n = va_arg(valst, int);
		if (n >= m_dimensionSizes.at(i))
		{
			free(args);
			throw NumpyOutOfIndexException();
		}
		args[i] = n;
	}
	va_end(valst);
	*value = *static_cast<T *>(getPtr(args));
	free(args);
}

template <typename T>
void NumpyType::setValue(T value, const int dim1)
{
	if (dim1 >= m_dimensionSizes.at(0))
	{
		throw NumpyOutOfIndexException();
	}
	T* data = static_cast<T *>(getPtr(dim1));
	*data = value;
}

template <typename T>
T NumpyType::getValue(const int dim1)
{
	if (dim1 >= m_dimensionSizes.at(0))
	{
		throw NumpyOutOfIndexException();
	}
	return *static_cast<T *>(getPtr(dim1));
}
template <typename T>
void NumpyType::setValue(T value, const int dim1, const int dim2)
{
	if (dim1 >= m_dimensionSizes.at(0) || dim2 >= m_dimensionSizes.at(1)) 
	{
		throw NumpyOutOfIndexException();
	}
	T* data = static_cast<T *>(getPtr(dim1, dim2));
	*data = value;
}

template <typename T>
T NumpyType::getValue(const int dim1, const int dim2)
{
	if (dim1 >= m_dimensionSizes.at(0) || dim2 >= m_dimensionSizes.at(1))
	{
		throw NumpyOutOfIndexException();
	}
	return *static_cast<T *>(getPtr(dim1,dim2));
}
template <typename T>
void NumpyType::setValue(T value, const int dim1, const int dim2, const int dim3)
{
	if (dim1 >= m_dimensionSizes.at(0) || dim2 >= m_dimensionSizes.at(1) || dim3 >= m_dimensionSizes.at(2))
	{
		throw NumpyOutOfIndexException();
	}
	T* data = static_cast<T *>(getPtr(dim1, dim2, dim3));
	*data = value;
}

template <typename T>
T NumpyType::getValue(const int dim1, const int dim2, const int dim3)
{
	if(dim1 >= m_dimensionSizes.at(0) || dim2 >= m_dimensionSizes.at(1) || dim3 >= m_dimensionSizes.at(2))
	{
		throw NumpyOutOfIndexException();
	}
	return *static_cast<T *>(getPtr(dim1,dim2, dim3));
}







