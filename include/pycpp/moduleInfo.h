//==============================================================================
/* This file is part of
*
* VPLSWIG - Voxel Processing Library
* Copyright 2017 3Dim Laboratory s.r.o.
* All rights reserved.
*
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/
#pragma once
#include <string>
#include <vector>

namespace pycpp
{
namespace defaults
{
const std::string infoMetaTag = "INFO:";
} // namespace defaults

//! Represented one function in script file.
struct FunctionInfo
{
    //! Short Description of method, which is comment with defined meta tag before declarations.
    std::string description;

    //! Contains function declaration in script e.g.  f(a,b).
    std::string declaration;
};


//! Object contains all functions in script file, also comment description of this functions.
//! When object is created, input script file is automaticly parsed. 
class ModuleInfo
{
public:
    //! \brief Constructor parsed input file.
    //! \param filePath Path to python script file, which should be parsed.
    //! \param infoTag Meta tag which recognise description of functions in comments.
    explicit ModuleInfo(const std::string& filePath, const std::string& infoTag = defaults::infoMetaTag);

    std::string moduleDescription;
    std::vector<FunctionInfo> functions;
private:
    //! Reading and parsing of input file.
    void parseFile();
    void removeSpaceBefore(std::string& line) const;
    bool checkLineWithCommentTag(std::string& line, bool isOpeningTag) const;
    std::string filePath;
    std::string infoTag;
};
} // namespace pycpp
